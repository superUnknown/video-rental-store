CREATE SCHEMA users;
ALTER SCHEMA users OWNER TO app_role;
GRANT ALL ON SCHEMA users TO app_role;

CREATE SCHEMA rentals;
ALTER SCHEMA rentals OWNER TO app_role;
GRANT ALL ON SCHEMA rentals TO app_role;

CREATE SCHEMA payments;
ALTER SCHEMA payments OWNER TO app_role;
GRANT ALL ON SCHEMA payments TO app_role;