#!/usr/bin/env bash
echo "Loading database scripts..."
for f in /docker-entrypoint-initdb.d/scripts/*.sql; do
  [ -f "$f" ] && psql --username postgres --dbname video_rental_store < "$f"
done
