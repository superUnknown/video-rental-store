package pl.budka.videorentalstore.api.users.commons.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@RequiredArgsConstructor
public enum UsersErrorCode {
    ACCOUNT_NOT_FOUND(HttpStatus.NOT_FOUND);

    private final HttpStatus status;

}