package pl.budka.videorentalstore.api.users.accounts.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    @NotNull
    private UUID id;

    @NotBlank
    private String firstname;

    @NotBlank
    private String surname;

    @NotBlank
    private String email;

}