package pl.budka.videorentalstore.api.users.accounts.http;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.budka.videorentalstore.api.users.accounts.model.AccountDto;
import pl.budka.videorentalstore.core.model.ApiResponse;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@FeignClient(AccountsApi.NAME)
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface AccountsApi {

    String NAME = "accounts-api";

    String ACCOUNT_ID_VARIABLE = "accountId";

    String GET_ACCOUNT_PATH = "/accounts/{" + ACCOUNT_ID_VARIABLE + "}";

    @GetMapping(GET_ACCOUNT_PATH)
    ApiResponse<AccountDto> getAccount(@NotNull @PathVariable(ACCOUNT_ID_VARIABLE) UUID accountId);

}