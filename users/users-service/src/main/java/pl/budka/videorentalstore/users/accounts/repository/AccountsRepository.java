package pl.budka.videorentalstore.users.accounts.repository;

import org.springframework.data.repository.Repository;
import pl.budka.videorentalstore.users.accounts.model.Account;

import java.util.Optional;
import java.util.UUID;

public interface AccountsRepository extends Repository<Account, UUID> {

    Optional<Account> findOne(UUID accountId);

}