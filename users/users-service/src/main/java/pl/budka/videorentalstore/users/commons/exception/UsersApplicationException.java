package pl.budka.videorentalstore.users.commons.exception;

import pl.budka.videorentalstore.api.users.commons.model.UsersErrorCode;
import pl.budka.videorentalstore.core.exceptions.ApplicationException;

public abstract class UsersApplicationException extends ApplicationException {

    protected UsersApplicationException(String message, UsersErrorCode code) {
        this(message, code, null);
    }

    protected UsersApplicationException(String message, UsersErrorCode code, Exception cause) {
        super(message, code.name(), code.getStatus().value(), cause);
    }

}