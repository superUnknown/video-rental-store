package pl.budka.videorentalstore.users.accounts.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.api.users.accounts.model.AccountDto;
import pl.budka.videorentalstore.users.accounts.exception.AccountNotFoundException;
import pl.budka.videorentalstore.users.accounts.repository.AccountsRepository;
import pl.budka.videorentalstore.users.utils.Mapper;

import java.util.UUID;

@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class AccountsServiceBean implements AccountsService {

    private final AccountsRepository accountsRepository;

    @Override
    @Transactional(readOnly = true)
    public AccountDto getAccount(UUID accountId) throws AccountNotFoundException {
        log.trace("Getting account [accountId: {}].", accountId);
        AccountDto result = Mapper.map(accountsRepository.findOne(accountId).orElseThrow(() -> AccountNotFoundException.forId(accountId)), AccountDto.class);
        log.trace("Retrieved account [accountId: {}]: {}.", accountId, result);
        return result;
    }

}