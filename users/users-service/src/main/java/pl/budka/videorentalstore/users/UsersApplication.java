package pl.budka.videorentalstore.users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import pl.budka.videorentalstore.core.exceptions.HttpExceptionHandler;

@SpringBootApplication
@Import({HttpExceptionHandler.class})
public class UsersApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsersApplication.class, args);
    }

}