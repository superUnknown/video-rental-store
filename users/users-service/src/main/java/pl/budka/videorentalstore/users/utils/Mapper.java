package pl.budka.videorentalstore.users.utils;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class Mapper {

    private static final ModelMapper INSTANCE;

    static {
        INSTANCE = new ModelMapper();
        INSTANCE.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    public static <D> D map(Object src, Class<D> clazz) {
        return INSTANCE.map(src, clazz);
    }

    public static <D> D map(Object src, D dest) {
        INSTANCE.map(src, dest);
        return dest;
    }

}