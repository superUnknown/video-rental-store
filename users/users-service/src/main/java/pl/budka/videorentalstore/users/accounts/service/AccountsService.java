package pl.budka.videorentalstore.users.accounts.service;

import pl.budka.videorentalstore.api.users.accounts.model.AccountDto;
import pl.budka.videorentalstore.users.accounts.exception.AccountNotFoundException;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface AccountsService {

    AccountDto getAccount(@NotNull UUID accountId) throws AccountNotFoundException;

}