package pl.budka.videorentalstore.users.accounts.http;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.budka.videorentalstore.api.users.accounts.http.AccountsApi;
import pl.budka.videorentalstore.api.users.accounts.model.AccountDto;
import pl.budka.videorentalstore.core.model.ApiResponse;
import pl.budka.videorentalstore.users.accounts.service.AccountsService;

import javax.validation.constraints.NotNull;
import java.util.UUID;

import static pl.budka.videorentalstore.core.utils.HttpApiHandler.execute;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AccountsController implements AccountsApi {

    private final AccountsService accountsService;

    @Override
    public ApiResponse<AccountDto> getAccount(@NotNull @PathVariable(ACCOUNT_ID_VARIABLE) UUID accountId) {
        log.info("Getting account [accountId: {}].", accountId);
        ApiResponse<AccountDto> result = execute(() -> accountsService.getAccount(accountId));
        log.info("Retrieved account [accountId: {}]: {}.", accountId, result);
        return result;
    }

}