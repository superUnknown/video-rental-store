package pl.budka.videorentalstore.users.accounts.exception;

import pl.budka.videorentalstore.api.users.commons.model.UsersErrorCode;
import pl.budka.videorentalstore.users.commons.exception.UsersApplicationException;

import java.util.UUID;

public class AccountNotFoundException extends UsersApplicationException {

    private AccountNotFoundException(String message) {
        super(message, UsersErrorCode.ACCOUNT_NOT_FOUND);
    }

    public static AccountNotFoundException forId(UUID accountId) {
        return new AccountNotFoundException(String.format("Account with id '%s' not found.", accountId));
    }

}