CREATE TABLE account (
    id UUID PRIMARY KEY,
    firstname TEXT NOT NULL,
    surname TEXT NOT NULL,
    email TEXT UNIQUE NOT NULL
);