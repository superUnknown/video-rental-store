package pl.budka.videorentalstore.core.utils;

import feign.FeignException;
import pl.budka.videorentalstore.core.exceptions.ApplicationException;
import pl.budka.videorentalstore.core.exceptions.ApplicationRuntimeException;
import pl.budka.videorentalstore.core.model.ApiResponse;

import java.util.function.Supplier;

public class HttpApiHandler {

    public static <T> ApiResponse<T> execute(ApplicationExceptionThrowingSupplier<T> supplier) {
        try {
            return ApiResponse.ok(supplier.get());
        } catch (ApplicationException e) {
            throw ApplicationRuntimeException.of(e);
        }
    }

    public static ApiResponse<Void> execute(ApplicationExceptionThrowingConsumer consumer) {
        try {
            consumer.accept();
        } catch (ApplicationException e) {
            throw ApplicationRuntimeException.of(e);
        }
        return ApiResponse.ok();
    }

    public static <T> T call(Supplier<ApiResponse<T>> supplier) {
        ApiResponse<T> response;
        try {
            response = supplier.get();
        } catch (FeignException e) {
            throw ApplicationRuntimeException.of(e);
        }
        if (!response.isOk()) {
            throw ApplicationRuntimeException.of(response);
        }
        return response.getResult();
    }

    @FunctionalInterface
    public interface ApplicationExceptionThrowingSupplier<T> {

        T get() throws ApplicationException;

    }

    @FunctionalInterface
    public interface ApplicationExceptionThrowingConsumer {

        void accept() throws ApplicationException;

    }

}