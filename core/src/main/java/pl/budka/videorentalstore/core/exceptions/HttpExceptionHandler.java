package pl.budka.videorentalstore.core.exceptions;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.budka.videorentalstore.core.model.ApiResponse;

@Slf4j
@ControllerAdvice
public class HttpExceptionHandler {

    @ResponseBody
    @ExceptionHandler(ApplicationRuntimeException.class)
    public ResponseEntity<ApiResponse> exceptionHandler(ApplicationRuntimeException e) {
        if (e.isServerError()) {
            log.error(e.getMessage(), e);
        } else {
            log.warn(e.getMessage());
        }
        return toResponseEntity(ApiResponse.error(e.getStatus(), e.getError()));
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {
            MissingServletRequestParameterException.class,
            HttpMediaTypeNotSupportedException.class,
            HttpMessageNotReadableException.class,
            IllegalArgumentException.class,
            InvalidFormatException.class
    })
    public ResponseEntity<ApiResponse> badRequestHandler(Exception e) {
        log.warn(e.getMessage());
        return toResponseEntity(ApiResponse.error(HttpStatus.BAD_REQUEST.value(), ApiResponse.Error.builder()
                .code(HttpStatus.BAD_REQUEST.name())
                .message(e.getMessage())
                .build()
        ));
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiResponse> exceptionHandler(Exception e) {
        log.error(e.getMessage(), e);
        return toResponseEntity(ApiResponse.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), ApiResponse.Error.builder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.name())
                .message(e.getMessage())
                .build()
        ));
    }

    private static ResponseEntity<ApiResponse> toResponseEntity(ApiResponse response) {
        return new ResponseEntity<>(response, HttpStatus.valueOf(response.getStatus()));
    }

}