package pl.budka.videorentalstore.core.utils;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

public class Time {

    private static final ZoneId timezone = ZoneOffset.UTC;

    private static Clock clock = Clock.system(timezone);

    public static Instant now() {
        return Instant.now(clock);
    }

    public static Instant atEndOfDay(Instant source) {
        return source.truncatedTo(ChronoUnit.DAYS).plus(Duration.ofDays(1)).minusNanos(1);
    }

    private static void setClock(Instant now) {
//        only for testing (no need to synchronize)
        clock = Clock.fixed(now, timezone);
    }

}