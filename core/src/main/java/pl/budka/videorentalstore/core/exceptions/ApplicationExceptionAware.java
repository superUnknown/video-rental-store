package pl.budka.videorentalstore.core.exceptions;

import pl.budka.videorentalstore.core.model.ApiResponse;

public interface ApplicationExceptionAware {

    ApiResponse.Error getError();

    int getStatus();

    default boolean isServerError() {
        return getStatus() >= 500;
    }

}