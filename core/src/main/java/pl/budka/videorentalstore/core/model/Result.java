package pl.budka.videorentalstore.core.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {

    @Valid
    private T value;

    public static <T> Result<T> of(T value) {
        return Result.<T>builder().value(value).build();
    }

}