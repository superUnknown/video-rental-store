package pl.budka.videorentalstore.core.streams;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Slf4j
@RequiredArgsConstructor
public final class StreamEventPublisherSupport {

    private final ApplicationEventPublisher eventPublisher;

    public <T> void publish(MessageChannel channel, T event) {
        log.trace("Publishing stream event [channel: {}, event: {}].", channel, event);
        channel.send(MessageBuilder.withPayload(event).build());
        log.trace("Published stream event [channel: {}, event: {}].", channel, event);
    }

    public <T> void enqueue(MessageChannel channel, T event) {
        log.trace("Enqueuing stream event [channel: {}, event: {}].", channel, event);
        eventPublisher.publishEvent(PublishStreamEvent.builder()
                .channel(channel)
                .event(event)
                .build()
        );
        log.trace("Enqueued stream event [channel: {}, event: {}].", channel, event);
    }

    @TransactionalEventListener
    <T> void onPublishStreamEvent(PublishStreamEvent<T> event) {
        publish(event.getChannel(), event.getEvent());
    }


    @Value
    @Builder
    private static class PublishStreamEvent<T> {

        @NotNull
        private MessageChannel channel;

        @Valid
        @NotNull
        private T event;

    }

}