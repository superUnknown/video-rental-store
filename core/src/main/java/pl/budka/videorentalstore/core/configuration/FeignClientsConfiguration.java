package pl.budka.videorentalstore.core.configuration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.budka.videorentalstore.core.exceptions.ApplicationRuntimeException;
import pl.budka.videorentalstore.core.model.ApiResponse;

import java.io.IOException;

@Configuration
public class FeignClientsConfiguration {

    @Bean
    public ErrorDecoder errorDecoder(ObjectMapper mapper) {
        return new ErrorDecoder.Default() {
            @Override
            public Exception decode(String methodKey, Response response) {
                try {
                    ApiResponse apiResponse = mapper.readValue(response.body().asInputStream(), new TypeReference<ApiResponse>() {});
                    return ApplicationRuntimeException.of(apiResponse);
                } catch (IOException e) {
                    return super.decode(methodKey, response);
                }
            }

        };
    }

}