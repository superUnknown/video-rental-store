package pl.budka.videorentalstore.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(NON_NULL)
public class ApiResponse<T> {

    private static final int OK = 200;

    @Min(OK)
    private int status;

    @Valid
    private T result;

    @Valid
    private Error error;

    @JsonIgnore
    public boolean isOk() {
        return this.status < 400;
    }

    public static ApiResponse<Void> ok() {
        return ApiResponse.<Void>builder().status(OK).build();
    }

    public static <T> ApiResponse<T> ok(T result) {
        return ApiResponse.<T>builder().status(OK).result(result).build();
    }

    public static ApiResponse error(Integer status, Error error) {
        return ApiResponse.builder().status(status).error(error).build();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Error {

        @NotBlank
        private String code;

        @NotBlank
        private String message;

    }

}