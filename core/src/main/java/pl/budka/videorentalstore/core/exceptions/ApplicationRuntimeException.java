package pl.budka.videorentalstore.core.exceptions;

import feign.FeignException;
import lombok.Getter;
import pl.budka.videorentalstore.core.model.ApiResponse;

@Getter
public class ApplicationRuntimeException extends RuntimeException implements ApplicationExceptionAware {

    private static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";

    private final ApiResponse.Error error;

    private final int status;

    private ApplicationRuntimeException(String message, int status, String code, Exception cause) {
        this(status, ApiResponse.Error.builder().code(code).message(message).build(), cause);
    }

    private ApplicationRuntimeException(int status, ApiResponse.Error error, Exception cause) {
        super(error.getMessage(), cause);
        this.error = error;
        this.status = status == 0 ? 500 : status;
    }

    public static ApplicationRuntimeException of(ApplicationException cause) {
        return new ApplicationRuntimeException(cause.getStatus(), cause.getError(), cause);
    }

    public static ApplicationRuntimeException of(ApiResponse response) {
        return new ApplicationRuntimeException(response.getStatus(), response.getError(), null);
    }

    public static ApplicationRuntimeException of(FeignException cause) {
//        TODO better error handling (timeout etc...)
        return new ApplicationRuntimeException(cause.getMessage(), cause.status(), INTERNAL_SERVER_ERROR, cause);
    }

}