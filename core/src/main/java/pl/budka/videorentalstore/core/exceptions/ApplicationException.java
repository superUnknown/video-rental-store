package pl.budka.videorentalstore.core.exceptions;

import lombok.Getter;
import pl.budka.videorentalstore.core.model.ApiResponse;

@Getter
public abstract class ApplicationException extends Exception implements ApplicationExceptionAware {

    private final ApiResponse.Error error;

    private final int status;

    protected ApplicationException(String message, String code, int status) {
        this(message, code, status, null);
    }

    protected ApplicationException(String message, String code, int status, Exception cause) {
        super(message, cause);
        this.error = ApiResponse.Error.builder().code(code).message(message).build();
        this.status = status == 0 ? 500 : status;
    }

}