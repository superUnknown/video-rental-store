# video-rental-store

# Architecture
To create that system, I choosed my favorite architecture - microservices. Individual components are responsible mainly for single purposes. Services are communicating each other with http REST-API and Kafka streams.

## users-service
Component manages user’s accounts. I didn’t focused on that part too much - a created only API for getting account by it’s identifier and supplied 2 accounts for demo purposes.

###  Definitions
* **Account**: represents user account in system; I assumed that each account is active and permitted for movie rentals.

### Features (include features only for end-client purposes) 
#### Getting user’a account by id:
```
GET:/accounts/{accountId}

Example response:
{
  "status": 200,
  "result": {
    "id": "8cbb8f6e-b443-4417-ba7d-ea92a136431c",
    "firstname": "Olivia",
    "surname": "Jones",
    "email": "jones@dev.com"
  }
}
```

## renals-service
Component is responsible for managing rental, movies, price calculations and delegating payment requests for rental to dedicated service.

###  Definitions
* **Movie**: represents movie, that can be rented by user. I assumed that each movie in the database has it’s unique identifier (e.g. two copies of the movie „Black Panther” are represented as two individual entities in the movie store. I didn’t focused on managing movies inventory and I supplied 3 movies for demo purposes (each for every movie type).
* **Rental**: represents single user’s rental for one or many movies for given period of time declared up front. I restricted parallel rentals per user to one at the same time for simplicity.
* **BonusPoints**: tracks customers “bonus” points for rentals; bonus points are granted when user returns rented movies.

### Features (include features only for end-client purposes) 
#### Creating (starting) rental:
```
POST:/rentals {
	accountId: UUID,
	endingUntil: Instant,
	movieIds: Set<UUID>
}

Example response:
{
  "status": 200,
  "result": {
    "paymentLink": "http://127.0.0.1:8083/transactions/d863c5ee-b010-4696-8d85-eb0ef20601b5/payment-notifications",
    "rental": {
      "id": "170b739c-da7c-4d4a-8d6a-73647c822b25",
      "accountId": "8cbb8f6e-b443-4417-ba7d-ea92a136431c",
      "chargeId": "87331ffe-d28a-4503-9182-a9aae9422676",
      "status": "REQUESTED",
      "started": "2019-06-19T17:35:29.373Z",
      "endingUntil": "2019-06-20T23:59:59.999999999Z",
      "completed": null,
      "movieIds": [
        "fdb43407-4a0e-4938-bae7-f6faa7efc70f"
      ]
    }
  }
}
```
#### Getting rental detailed status (combines rental and associated charge states):
```
GET:/rentals/{rentalId}/detailed-status

Example response:
{
  "status": 200,
  "result": {
    "value": "ACTIVE__PAID"
  }
}
```
#### Finishing given account’s active rental (returning movies):
```
PUT:/rentals/finish {
	accountId: UUID
}

Example response when movies are returned at time:
{
  "status": 200,
  "result": {
    "rental": {
      "id": "170b739c-da7c-4d4a-8d6a-73647c822b25",
      "accountId": "8cbb8f6e-b443-4417-ba7d-ea92a136431c",
      "chargeId": "87331ffe-d28a-4503-9182-a9aae9422676",
      "status": "COMPLETED",
      "started": "2019-06-19T17:35:29.373Z",
      "endingUntil": "2019-06-20T23:59:59.999999Z",
      "completed": "2019-06-19T18:41:27.447Z",
      "movieIds": [
        "fdb43407-4a0e-4938-bae7-f6faa7efc70f"
      ]
    },
    "status": "SUCCEED"
  }
}

Example response when movies are returned late:
{
  "status": 200,
  "result": {
	  "paymentLink": "http://127.0.0.1:8083/transactions/d863c5ee-b010-4696-8d85-eb0ef20601b5/payment-notifications",
    "rental": {
      "id": "170b739c-da7c-4d4a-8d6a-73647c822b25",
      "accountId": "8cbb8f6e-b443-4417-ba7d-ea92a136431c",
      "chargeId": "87331ffe-d28a-4503-9182-a9aae9422676",
      "status": "COMPLETED",
      "started": "2019-06-19T17:35:29.373Z",
      "endingUntil": "2019-06-20T23:59:59.999999Z",
      "completed": "2019-06-19T18:41:27.447Z",
      "movieIds": [
        "fdb43407-4a0e-4938-bae7-f6faa7efc70f"
      ]
    },
    "status": "ADDITIONAL_PAYMENT_REQUIRED"
  }
}
```
#### Estimating rental cost of selected movies:
```
GET:/rentals/cost-estimation?endingUntil={yyyy-MM-dd'T'HH:mm:ss.SSSZ}&movieId={UUID}&movieId={UUID}&movieId=...

Example response:
{
  "status": 200,
  "result": {
    "totalPrice": 70,
    "currency": "SEK",
    "movies": [
      {
        "id": "fdb43407-4a0e-4938-bae7-f6faa7efc70f",
        "name": "Black Panther",
        "price": 40
      },
      {
        "id": "fab539f6-85c4-4436-976b-503a02fdf3e3",
        "name": "The Dark Knight",
        "price": 30
      }
    ]
  }
}
```

## payments-service
Component is responsible for requesting payments for rentals and handling payment process state notifications. I’m mocking external payment gateway that operates like one that I have experience with - PayU. Returned payment link, that GUI should redirect user to, to complete payment on the external payment service’s web page, points directly to internal payment notification handler that is used to track payment process on the external payment gateway. 

###  Definitions
* **Charge**: represents charge for single movies rental
* **Transaction**: individual payments delegated to external payment gateway that are associated with single real payment transactions. Charge can include many that kind of transactions - it tracks every user’s payment attempt (failed and succeed)

### Features (include features only for end-client purposes) 
#### Receiving transaction payment notifications:
```
GET:/transactions/{transactionId}/payment-notifications # optional 'failed=true' query param symulates failed payment attempt

Example response (status 200 just indicates that notification has been received successfully - it doesn't tell that payment succeed):
{
  "status": 200
}
```

# Environment
All system can be started from provided `docker-compose.yml` file located in the root project directory. It contains all system components (users-service, rentals-service, payments-service, postgres, kafka). 

## How to run
```
1. After cloning repository, enter into project root directory.

2. Build project (maven will build required dockers):
mvn clean && mvn clean install

3. Start system:
docker-compose up -d

4. Show all components logs (all services are configured with TRACE logging level):
docker-compose logs -f
```

## Default configuration
### Default users-service configuration
```
video-rental-store/users/users-service/src/main/resources/bootstrap.properties
```
### Default rentals-service configuration
```
video-rental-store/rentals/rentals-service/src/main/resources/bootstrap.properties
```
### Default payments-service configuration
```
video-rental-store/payments/payments-service/src/main/resources/bootstrap.properties
```


# Demo database
Database is filled with some users and movies for demo purposes.

### Accounts
1. John Smith (smith@dev.com); accountId: `802811F7-770B-4A57-B62B-E89A42AF08B2`
2. Olivia Jones (jones@dev.com); accountId: `8CBB8F6E-B443-4417-BA7D-EA92A136431C`

### Movies
1. Black Panther (NEW_RELEASE); movieId: `FDB43407-4A0E-4938-BAE7-F6FAA7EFC70F`
2. The Dark Knight (REGULAR); movieId: `FAB539F6-85C4-4436-976B-503A02FDF3E3`
3. The Shawshank Redemption (OLD); movieId: `8977C542-2B0E-418C-A58E-D0C6205D815D`

# Rental process
All provided examples assume, that system is started with default environment configuration (port mappings, components addresses).

## Rejected rental process due to payment failed
* Creating rental or all 3 movies (here we are locking given movies that they can’t be rented for another user):

```
curl -XPOST 'http://127.0.0.1:8082/rentals' -H 'content-type: application/json' --data '{
	"accountId": "8CBB8F6E-B443-4417-BA7D-EA92A136431C",
	"endingUntil": "2019-06-24T13:00:00.000Z",
	"movieIds": [
		"FDB43407-4A0E-4938-BAE7-F6FAA7EFC70F",
		"FAB539F6-85C4-4436-976B-503A02FDF3E3",
		"8977C542-2B0E-418C-A58E-D0C6205D815D"
	]
}'
```

* Returned response will contain property `paymentLink`; add `failed=true` query param to simulate payment error - it points to payment notification handler:

```
curl http://127.0.0.1:8083/transactions/{transactionId}/payment-notifications?failed=true
```

* After calling `paymentLink`, GUI should monitor rental state until it change from `REQUESTED` to `REJECTED`; after that change:
	* transaction will be `FAILED`,
	* parent charge of transaction will be `FAILED`,
	* requested rental will be `REJECTED`.

```
curl http://127.0.0.1:8082/rentals/{rentalId}/detailed-status
```

* That state is not repeatable; once rental is `REJECTED`, client should request another rental;  when rental is `REJECTED`, all given movies are back to available).

## Successful rental process when payment succeed
* Creating rental or all 3 movies (here we are locking given movies that they can’t be rented for another user):

```
curl -XPOST 'http://127.0.0.1:8082/rentals' -H 'content-type: application/json' --data '{
	"accountId": "8CBB8F6E-B443-4417-BA7D-EA92A136431C",
	"endingUntil": "2019-06-24T13:00:00.000Z",
	"movieIds": [
		"FDB43407-4A0E-4938-BAE7-F6FAA7EFC70F",
		"FAB539F6-85C4-4436-976B-503A02FDF3E3",
		"8977C542-2B0E-418C-A58E-D0C6205D815D"
	]
}'
```

* Returned response will contain property `paymentLink` - it points to payment notification handler:

```
curl http://127.0.0.1:8083/transactions/{transactionId}/payment-notifications
```

* After calling `paymentLink`, GUI should monitor rental state until it change from `REQUESTED` to `ACTIVE__PAID`; after that change:
	* transaction will be `COMPLETED`,
	* parent charge of transaction will be `COMPLETED`,
	* requested rental will be `ACTIVE`.

```
curl http://127.0.0.1:8082/rentals/{rentalId}/detailed-status
```

* Now, client has active rental. User can return rented movies anytime he want.

## Movies return process (finishing rental) when returning at time
* Active rental can be finished at any time, but there are no refunds for unused days.
* Requesting rental finish for given account:

```
curl -XPUT 'http://127.0.0.1:8082/rentals/finish' -H 'content-type: application/json' --data '{
    "accountId": "8CBB8F6E-B443-4417-BA7D-EA92A136431C"
}' | jq
```

* When returning at time, operation will return `result.status=SUCCEED`; bonus points could not be visible immediately (they are granted asynchronously).
* Now, client has `COMPLETED` rental and bonus point are granted. User can now request another rental.

## Movies return process (finishing rental) when returning late and additional penalty payment failed
* To simulate returning late, we can modify rental period directly in the database (e.g. changing rental period to 2 days in the past):

```
# Ender into postgres docker container
docker exec -it video-rental-store_postgres_1 bash

# Connect to the database
psql -h 127.0.0.1 -p 5432 -U storeuser video_rental_store

# Modify rental period (e.g. substract 3 days from 'started' and 'ending_until' columns)
update rentals.rental set started = '2019-06-14 18:07:44.261', ending_until = '2019-06-16 23:59:59.999999'
``` 

* Requesting rental finish for given account:

```
curl -XPUT 'http://127.0.0.1:8082/rentals/finish' -H 'content-type: application/json' --data '{
    "accountId": "8CBB8F6E-B443-4417-BA7D-EA92A136431C"
}' | jq
```
When returning late, operation will return `result.status=ADDITIONAL_PAYMENT_REQUIRED` and additional `result.paymentLink` property.

* Add `failed=true` query param to simulate payment error - it points to payment notification handler:

```
curl http://127.0.0.1:8083/transactions/{transactionId}/payment-notifications?failed=true
```

* After calling `paymentLink`, GUI should monitor rental state until it change from `ACTIVE__PENDING_CORRECTION_PAYMENT` to `ACTIVE__FAILED_CORRECTION_PAYMENT`; after that change:
	* transaction will be `FAILED`,
	* parent charge of transaction will be `FAILED`,
	* rental will remain `ACTIVE`.

```
curl http://127.0.0.1:8082/rentals/{rentalId}/detailed-status
```

* After payment failed, process can be repeated with exactly the same operation as in point 1:

```
curl -XPUT 'http://127.0.0.1:8082/rentals/finish' -H 'content-type: application/json' --data '{
    "accountId": "8CBB8F6E-B443-4417-BA7D-EA92A136431C"
}' | jq
```
Now we can simulate successful payment by not adding `failed=true` query param to complete rental.

## Movies return process (finishing rental) when returning late and additional penalty payment succeed
* To simulate returning late, we can modify rental period directly in the database (e.g. changing rental period to 2 days in the past):

```
# Ender into postgres docker container
docker exec -it video-rental-store_postgres_1 bash

# Connect to the database
psql -h 127.0.0.1 -p 5432 -U storeuser video_rental_store

# Modify rental period (e.g. substract 3 days from 'started' and 'ending_until' columns)
update rentals.rental set started = '2019-06-14 18:07:44.261', ending_until = '2019-06-16 23:59:59.999999'
``` 

* Requesting rental finish for given account:
```
curl -XPUT 'http://127.0.0.1:8082/rentals/finish' -H 'content-type: application/json' --data '{
    "accountId": "8CBB8F6E-B443-4417-BA7D-EA92A136431C"
}' | jq
```
When returning late, operation will return `result.status=ADDITIONAL_PAYMENT_REQUIRED` and additional `result.paymentLink` property.

* Call `result.paymentLink`, which points to payment notification handler:

```
curl http://127.0.0.1:8083/transactions/{transactionId}/payment-notifications
```

* After calling `paymentLink`, GUI should monitor rental state until it change from `ACTIVE__PENDING_CORRECTION_PAYMENT` to `COMPLETED`; after that change:
	* transaction will be `COMPLETED`,
	* parent charge of transaction will be `COMPLETED`,
	* rental will be `COMPLETED`.

```
curl http://127.0.0.1:8082/rentals/{rentalId}/detailed-status
```

* Now, client has `COMPLETED` rental and bonus point are granted. User can now request another rental.

# TODO
Things that should be done, but I didn’t covered them due to lack of time:

1. REST-API for internal system purposes should be restricted for end-client (e.g. requesting charge payment for rental and charge modification); I would done this with API Gateway pattern.
2. API for getting rental details, payments details, regenerate `paymentLink` - generally, I did’t focused on creating API for data retrieval.
3. CRUD’s for movie inventory.
4. users-service - I didn’t focused on that part
5. Failover for requesting payment for rental: when starting rental, payment is requested and there could be e.g. timeout at the connection between `rentals-service => payments-service`. When that kind or error occurs, starting rental is rejected, but charge for this rental is created (it leads to orphaned charge in the database). That kind of situation occurs often during first start payment request, when JVM is not warmed up yet (if it happens, just please ignore that and try again :) ) It could occur during finishing rental late too.
6. Tests - at the beginning I was testing every single use-case, but later I just was living tests definitions with TODO flag. The are no unit tests for movie price calculator and for getting rental detailed status too. I didn’t created any acceptance tests which runs on real components.
7. Synchronization - there is no access synchronization to movies, rentals, charges and transactions (e.g. API end-client could call creating rental operation twice which could end up with two created rentals for the same client for the same period of time). In my opinion, for that kind of architecture, simple synchronization wouldn’t be the best idea and I would use redis and e.g. `RedissonLock` for distributed synchronization between multiple components/instances.
8. After rental is completed, its charge should enter into some final state that does’t allow to modify it; now completed charge is more like „succeed for now” and it can be exceeded by additional transaction in case of penalty payment requirement. There should be additional state telling that movies are returned and charge is unmodifiable.  