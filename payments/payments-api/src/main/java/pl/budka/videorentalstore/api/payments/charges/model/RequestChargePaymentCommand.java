package pl.budka.videorentalstore.api.payments.charges.model;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestChargePaymentCommand {

    @NotNull
    private CurrencyCode currency;

    @Valid
    @NotNull
    private ChargeDto.Customer customer;

    @Valid
    @NotEmpty
    private List<ChargeDto.PricePosition> pricePositions;

}