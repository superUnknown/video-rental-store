package pl.budka.videorentalstore.api.payments.charges.http;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto;
import pl.budka.videorentalstore.api.payments.charges.model.ModifyChargeCommand;
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand;
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentResultDto;
import pl.budka.videorentalstore.core.model.ApiResponse;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@FeignClient(ChargesApi.NAME)
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface ChargesApi {

    String NAME = "charges-api";

    String CHARGE_ID_VARIABLE = "chargeId";

    String REQUEST_CHARGE_PAYMENT_PATH = "/charges";

    String GET_CHARGE_PATH = "/charges/{" + CHARGE_ID_VARIABLE + "}";

    String MODIFY_CHARGE_PATH = "/charges/{" + CHARGE_ID_VARIABLE + "}";

    @GetMapping(GET_CHARGE_PATH)
    ApiResponse<ChargeDto> getCharge(@NotNull @PathVariable(CHARGE_ID_VARIABLE) UUID chargeId);

    @PostMapping(path = REQUEST_CHARGE_PAYMENT_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ApiResponse<RequestChargePaymentResultDto> requestChargePayment(@Valid @NotNull @RequestBody RequestChargePaymentCommand command);

    @PutMapping(path = MODIFY_CHARGE_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ApiResponse<RequestChargePaymentResultDto> modifyCharge(@NotNull @PathVariable(CHARGE_ID_VARIABLE) UUID chargeId, @Valid @NotNull @RequestBody ModifyChargeCommand command);

}