package pl.budka.videorentalstore.api.payments.charges.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.budka.videorentalstore.core.utils.Time;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChargeStatusChangedEvent {

    public static final String QUEUE = "charge-status-changed";

    @NotNull
    private final Instant time = Time.now();

    @NotNull
    private UUID chargeId;

    @NotNull
    private ChargeStatus targetStatus;

}