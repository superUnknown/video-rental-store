package pl.budka.videorentalstore.api.payments.transactions.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.budka.videorentalstore.core.utils.Time;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionStatusChangedEvent {

    public static final String QUEUE = "transaction-status-changed";

    @NotNull
    private final Instant time = Time.now();

    @NotNull
    private UUID transactionId;

    @NotNull
    private TransactionStatus targetStatus;

}