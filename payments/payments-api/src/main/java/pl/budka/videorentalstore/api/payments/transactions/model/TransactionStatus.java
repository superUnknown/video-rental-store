package pl.budka.videorentalstore.api.payments.transactions.model;

public enum TransactionStatus {
    CREATED,
    COMPLETED,
    FAILED;

    public boolean isFinished() {
        return this != CREATED;
    }

}