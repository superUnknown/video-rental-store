package pl.budka.videorentalstore.api.payments.transactions.http;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.budka.videorentalstore.core.model.ApiResponse;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@FeignClient(TransactionsApi.NAME)
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface TransactionsApi {

    String NAME = "transactions-api";

    String TRANSACTION_ID_VARIABLE = "transactionId";

    String FAILED_VARIABLE = "failed";

    String RECEIVE_TRANSACTION_PAYMENT_NOTIFICATION_PATH = "/transactions/{" + TRANSACTION_ID_VARIABLE + "}/payment-notifications";

    @GetMapping(RECEIVE_TRANSACTION_PAYMENT_NOTIFICATION_PATH)
    ApiResponse<Void> receiveTransactionPaymentNotification(@NotNull @PathVariable(TRANSACTION_ID_VARIABLE) UUID transactionId, @RequestParam(name = FAILED_VARIABLE, required = false) boolean failed);

}