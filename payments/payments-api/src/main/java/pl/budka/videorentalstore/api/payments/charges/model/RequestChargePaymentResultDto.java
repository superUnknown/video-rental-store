package pl.budka.videorentalstore.api.payments.charges.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestChargePaymentResultDto {

    @NotBlank
    private String paymentLink;

    @Valid
    @NotNull
    private ChargeDto charge;

}