package pl.budka.videorentalstore.api.payments.transactions.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionDto {

    @NotNull
    private UUID id;

    @NotNull
    private UUID chargeId;

    @NotNull
    private TransactionStatus status;

    @NotNull
    private Instant created;

    private Instant completed;

    @NotNull
    @DecimalMin("0.01")
    private BigDecimal amount;

}