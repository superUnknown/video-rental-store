package pl.budka.videorentalstore.api.payments.commons.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@RequiredArgsConstructor
public enum PaymentsErrorCode {
    CHARGE_NOT_FOUND(HttpStatus.NOT_FOUND),
    CHARGE_ALREADY_FINISHED(HttpStatus.CONFLICT),
    CHARGE_PAYMENT_IN_PROGRESS(HttpStatus.METHOD_NOT_ALLOWED),
    CHARGE_REFUND_NOT_SUPPORTED(HttpStatus.METHOD_NOT_ALLOWED),
    TRANSACTION_NOT_FOUND(HttpStatus.NOT_FOUND),
    TRANSACTION_ALREADY_FINISHED(HttpStatus.CONFLICT);

    private final HttpStatus status;

}