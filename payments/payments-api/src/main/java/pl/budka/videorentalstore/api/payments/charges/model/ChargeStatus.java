package pl.budka.videorentalstore.api.payments.charges.model;

public enum ChargeStatus {
    PENDING,
    COMPLETED,
    FAILED
}