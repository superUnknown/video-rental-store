package pl.budka.videorentalstore.api.payments.transactions.streams;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageChannel;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatusChangedEvent;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface TransactionStatusChangedEventListener {

    @StreamListener(TransactionStatusChangedEvent.QUEUE)
    void onEvent(@Valid @NotNull TransactionStatusChangedEvent event);

    interface Channel {

        @Input(TransactionStatusChangedEvent.QUEUE)
        MessageChannel input();

    }

}