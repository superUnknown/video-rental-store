package pl.budka.videorentalstore.api.payments.charges.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ModifyChargeCommand {

    @Valid
    @NotEmpty
    private List<ChargeDto.PricePosition> pricePositions;

}