package pl.budka.videorentalstore.api.payments.charges.model;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionDto;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChargeDto {

    @NotNull
    private UUID id;

    @NotNull
    private ChargeStatus status;

    @NotNull
    private CurrencyCode currency;

    @NotNull
    private Instant created;

    @Valid
    @NotNull
    private Customer customer;

    @Valid
    @NotEmpty
    private List<PricePosition> pricePositions;

    @Valid
    @NotEmpty
    private List<TransactionDto> transactions;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Customer {

        @NotBlank
        private String firstname;

        @NotBlank
        private String surname;

        @NotBlank
        private String email;

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PricePosition {

        @NotBlank
        private String name;

        @Min(1)
        @NotNull
        private Integer quantity;

        @NotNull
        @DecimalMin("0.01")
        private BigDecimal unitPrice;

    }

}