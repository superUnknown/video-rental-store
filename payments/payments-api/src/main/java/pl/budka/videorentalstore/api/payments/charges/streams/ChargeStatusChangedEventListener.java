package pl.budka.videorentalstore.api.payments.charges.streams;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageChannel;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatusChangedEvent;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface ChargeStatusChangedEventListener {

    @StreamListener(ChargeStatusChangedEvent.QUEUE)
    void onEvent(@Valid @NotNull ChargeStatusChangedEvent event);

    interface Channel {

        @Input(ChargeStatusChangedEvent.QUEUE)
        MessageChannel input();

    }

}