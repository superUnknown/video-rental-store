package pl.budka.videorentalstore.payments.utils;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionDto;
import pl.budka.videorentalstore.payments.transactions.model.Transaction;

public class Mapper {

    private static final ModelMapper INSTANCE;

    private static final PropertyMap<Transaction, TransactionDto> TRANSACTION_TO_DTO_MAPPING = new PropertyMap<Transaction, TransactionDto>() {
        @Override
        protected void configure() {
            map().setChargeId(source.getCharge().getId());
        }
    };

    static {
        INSTANCE = new ModelMapper();
        INSTANCE.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        INSTANCE.addMappings(TRANSACTION_TO_DTO_MAPPING);
    }

    public static <D> D map(Object src, Class<D> clazz) {
        return INSTANCE.map(src, clazz);
    }

    public static <D> D map(Object src, D dest) {
        INSTANCE.map(src, dest);
        return dest;
    }

}