package pl.budka.videorentalstore.payments.commons.exception;

import pl.budka.videorentalstore.api.payments.commons.model.PaymentsErrorCode;
import pl.budka.videorentalstore.core.exceptions.ApplicationException;

public abstract class PaymentsApplicationException extends ApplicationException {

    protected PaymentsApplicationException(String message, PaymentsErrorCode code) {
        this(message, code, null);
    }

    protected PaymentsApplicationException(String message, PaymentsErrorCode code, Exception cause) {
        super(message, code.name(), code.getStatus().value(), cause);
    }

}