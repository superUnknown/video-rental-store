package pl.budka.videorentalstore.payments.charges.exception;

import pl.budka.videorentalstore.api.payments.commons.model.PaymentsErrorCode;
import pl.budka.videorentalstore.payments.commons.exception.PaymentsApplicationException;

public class ChargeRefundNotSupportedException extends PaymentsApplicationException {

    public ChargeRefundNotSupportedException() {
        super("Charge refund is not supported", PaymentsErrorCode.CHARGE_REFUND_NOT_SUPPORTED);
    }

}