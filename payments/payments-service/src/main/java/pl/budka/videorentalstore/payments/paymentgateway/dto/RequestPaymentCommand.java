package pl.budka.videorentalstore.payments.paymentgateway.dto;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestPaymentCommand {

    @NotNull
    private UUID transactionId;

    @NotNull
    private CurrencyCode currency;

    @Valid
    @NotNull
    private ChargeDto.Customer customer;

    @Valid
    @NotEmpty
    private List<PricePosition> pricePositions;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PricePosition {

        @NotBlank
        private String name;

        @Min(1)
        @NotNull
        private Integer quantity;

        @NotNull
        private BigDecimal unitPrice;

    }

}