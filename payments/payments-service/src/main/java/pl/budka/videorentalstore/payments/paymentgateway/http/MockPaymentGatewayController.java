package pl.budka.videorentalstore.payments.paymentgateway.http;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentCommand;
import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentResultDto;

@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class MockPaymentGatewayController implements PaymentGatewayApi {

    @Override
    public RequestPaymentResultDto requestPayment(RequestPaymentCommand command) {
        log.info(">>> SIMULATING CONNECTION WITH SOME PAYMENT GATEWAY [command: {}].", command);
        RequestPaymentResultDto result = RequestPaymentResultDto.builder()
                .paymentLink(String.format("http://127.0.0.1:8083/transactions/%s/payment-notifications", command.getTransactionId()))
                .build();
        log.info("<<< SIMULATING CONNECTION WITH SOME PAYMENT GATEWAY [command: {}]: {}.", command, result);
        return result;
    }

}