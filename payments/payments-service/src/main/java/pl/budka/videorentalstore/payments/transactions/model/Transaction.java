package pl.budka.videorentalstore.payments.transactions.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatus;
import pl.budka.videorentalstore.core.utils.Time;
import pl.budka.videorentalstore.payments.charges.model.Charge;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Transaction {

    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    @NotNull
    @Builder.Default
    @Enumerated(EnumType.STRING)
    private TransactionStatus status = TransactionStatus.CREATED;

    @NotNull
    @Builder.Default
    private Instant created = Time.now();

    private Instant completed;

    @NotNull
    @DecimalMin("0.01")
    private BigDecimal amount;

    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "charge_id", nullable = false)
    private Charge charge;

    @Transient
    public boolean isFinished() {
        return status.isFinished();
    }

    @Transient
    public boolean isCompleted() {
        return status == TransactionStatus.COMPLETED;
    }

}