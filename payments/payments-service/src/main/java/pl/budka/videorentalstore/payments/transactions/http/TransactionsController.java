package pl.budka.videorentalstore.payments.transactions.http;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.budka.videorentalstore.api.payments.transactions.http.TransactionsApi;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatus;
import pl.budka.videorentalstore.core.model.ApiResponse;
import pl.budka.videorentalstore.payments.transactions.dto.ReceiveTransactionPaymentNotificationCommand;
import pl.budka.videorentalstore.payments.transactions.service.TransactionsService;

import javax.validation.constraints.NotNull;
import java.util.UUID;

import static pl.budka.videorentalstore.core.utils.HttpApiHandler.execute;

@Slf4j
@RestController
@RequiredArgsConstructor
public class TransactionsController implements TransactionsApi {

    private final TransactionsService transactionsService;

    @Override
    public ApiResponse<Void> receiveTransactionPaymentNotification(@NotNull @PathVariable(TRANSACTION_ID_VARIABLE) UUID transactionId, @RequestParam(name = FAILED_VARIABLE, required = false) boolean failed) {
        log.info("Receiving transaction payment notification [transactionId: {}, failed: {}]", transactionId, failed);
        ApiResponse<Void> result = execute(() -> transactionsService.receiveTransactionPaymentNotification(ReceiveTransactionPaymentNotificationCommand.builder()
                .paymentStatus(failed ? TransactionStatus.FAILED : TransactionStatus.COMPLETED)
                .transactionId(transactionId)
                .build()
        ));
        log.info("Received transaction payment notification [transactionId: {}, failed: {}]", transactionId, failed);
        return result;
    }

}