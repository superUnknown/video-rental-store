package pl.budka.videorentalstore.payments.charges.model;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatus;
import pl.budka.videorentalstore.core.utils.Time;
import pl.budka.videorentalstore.payments.transactions.model.Transaction;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Charge {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Builder.Default
    @Enumerated(EnumType.STRING)
    private ChargeStatus status = ChargeStatus.PENDING;

    @NotNull
    @Enumerated(EnumType.STRING)
    private CurrencyCode currency;

    @NotNull
    @Builder.Default
    private Instant created = Time.now();

    @NotNull
    @JoinColumn(name = "customer_id", nullable = false)
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Customer customer;

    @NotEmpty
    @Builder.Default
    @JoinColumn(name = "charge_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PricePosition> pricePositions = new ArrayList<>();

    @NotEmpty
    @Builder.Default
    @OneToMany(mappedBy = "charge", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Transaction> transactions = new ArrayList<>();

    @Transient
    public BigDecimal getTotalAmount() {
        return pricePositions.stream()
                .map(it -> it.getUnitPrice().multiply(BigDecimal.valueOf(it.getQuantity())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Transient
    public BigDecimal getAlreadyPaidAmount() {
        return transactions.stream().filter(Transaction::isCompleted).map(Transaction::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Transient
    public BigDecimal getLeftToPay() {
        return getTotalAmount().subtract(getAlreadyPaidAmount());
    }

    @Transient
    public boolean isPending() {
        return status == ChargeStatus.PENDING;
    }

    @Transient
    public boolean isFinished() {
        return status != ChargeStatus.PENDING;
    }

    @Transient
    public boolean isFullyPaid() {
        return getLeftToPay().signum() <= 0;
    }

    @Transient
    public void setPricePositions(List<PricePosition> pricePositions) {
        this.pricePositions.clear();
        this.pricePositions.addAll(pricePositions);

    }

    @Transient
    public void addTransaction(Transaction transaction) {
        transaction.setCharge(this);
        transactions.add(transaction);
    }

    @Getter
    @Setter
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Table(name = "customer")
    public static class Customer {

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID id;

        @NotBlank
        private String firstname;

        @NotBlank
        private String surname;

        @NotBlank
        private String email;

    }

    @Getter
    @Setter
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Table(name = "price_position")
    public static class PricePosition {

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID id;

        @NotBlank
        private String name;

        @Min(1)
        @NotNull
        private Integer quantity;

        @NotNull
        @DecimalMin("0.01")
        private BigDecimal unitPrice;

    }

}