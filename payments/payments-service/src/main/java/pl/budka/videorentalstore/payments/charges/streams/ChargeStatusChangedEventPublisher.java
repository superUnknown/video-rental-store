package pl.budka.videorentalstore.payments.charges.streams;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatusChangedEvent;

public interface ChargeStatusChangedEventPublisher {

    void publish(ChargeStatusChangedEvent event);

    interface Channel {

        @Output(ChargeStatusChangedEvent.QUEUE)
        MessageChannel output();

    }

}
