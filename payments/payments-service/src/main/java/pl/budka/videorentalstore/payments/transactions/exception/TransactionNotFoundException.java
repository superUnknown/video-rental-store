package pl.budka.videorentalstore.payments.transactions.exception;

import pl.budka.videorentalstore.api.payments.commons.model.PaymentsErrorCode;
import pl.budka.videorentalstore.payments.commons.exception.PaymentsApplicationException;

import java.util.UUID;

public class TransactionNotFoundException extends PaymentsApplicationException {

    private TransactionNotFoundException(String message) {
        super(message, PaymentsErrorCode.TRANSACTION_NOT_FOUND);
    }

    public static TransactionNotFoundException forId(UUID transactionId) {
        return new TransactionNotFoundException(String.format("Transaction with id '%s' doesn't exist.", transactionId));
    }

}