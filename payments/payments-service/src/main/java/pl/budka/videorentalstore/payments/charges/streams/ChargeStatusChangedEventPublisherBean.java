package pl.budka.videorentalstore.payments.charges.streams;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatusChangedEvent;
import pl.budka.videorentalstore.core.streams.StreamEventPublisherSupport;

@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@EnableBinding(ChargeStatusChangedEventPublisher.Channel.class)
public class ChargeStatusChangedEventPublisherBean implements ChargeStatusChangedEventPublisher {

    private final StreamEventPublisherSupport publisher;

    private final Channel channel;

    @Override
    public void publish(ChargeStatusChangedEvent event) {
        publisher.enqueue(channel.output(), event);
    }

}