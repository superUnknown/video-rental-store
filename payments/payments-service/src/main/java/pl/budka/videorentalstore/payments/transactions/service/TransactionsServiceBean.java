package pl.budka.videorentalstore.payments.transactions.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionDto;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatus;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatusChangedEvent;
import pl.budka.videorentalstore.core.utils.Time;
import pl.budka.videorentalstore.payments.charges.exception.ChargeNotFoundException;
import pl.budka.videorentalstore.payments.charges.model.Charge;
import pl.budka.videorentalstore.payments.charges.repository.ChargesRepository;
import pl.budka.videorentalstore.payments.transactions.dto.CreateTransactionCommand;
import pl.budka.videorentalstore.payments.transactions.dto.ReceiveTransactionPaymentNotificationCommand;
import pl.budka.videorentalstore.payments.transactions.exception.TransactionAlreadyFinishedException;
import pl.budka.videorentalstore.payments.transactions.exception.TransactionNotFoundException;
import pl.budka.videorentalstore.payments.transactions.model.Transaction;
import pl.budka.videorentalstore.payments.transactions.repository.TransactionsRepository;
import pl.budka.videorentalstore.payments.transactions.streams.TransactionStatusChangedEventPublisher;
import pl.budka.videorentalstore.payments.utils.Mapper;

import java.util.UUID;

@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class TransactionsServiceBean implements TransactionsService {

    private final TransactionStatusChangedEventPublisher transactionStatusChangedEventPublisher;

    private final TransactionsRepository transactionsRepository;

    private final ChargesRepository chargesRepository;

    @Override
    @Transactional(readOnly = true)
    public TransactionDto getTransaction(UUID transactionId) throws TransactionNotFoundException {
        log.trace("Getting transaction [transactionId: {}].", transactionId);
        TransactionDto result = Mapper.map(transactionsRepository.findOne(transactionId)
                .orElseThrow(() -> TransactionNotFoundException.forId(transactionId)), TransactionDto.class
        );
        log.trace("Retrieved transaction [transactionId: {}]: {}.", transactionId, result);
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public TransactionDto createTransaction(CreateTransactionCommand command) throws ChargeNotFoundException {
        log.debug("Creating transaction [command: {}].", command);
        Charge charge = chargesRepository.findOne(command.getChargeId()).orElseThrow(() -> ChargeNotFoundException.forId(command.getChargeId()));
        Transaction transaction = Mapper.map(command, Transaction.builder().build());
        charge.addTransaction(transaction);
        chargesRepository.save(charge);
        TransactionDto result = Mapper.map(transaction, TransactionDto.class);
        log.debug("Created transaction [command: {}]: {}.", command, result);
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void receiveTransactionPaymentNotification(ReceiveTransactionPaymentNotificationCommand command) throws TransactionNotFoundException, TransactionAlreadyFinishedException {
        log.debug("Receiving transaction payment notification [command: {}].", command);
        Transaction transaction = transactionsRepository.findOne(command.getTransactionId())
                .orElseThrow(() -> TransactionNotFoundException.forId(command.getTransactionId()));
        if (transaction.isFinished()) {
            throw TransactionAlreadyFinishedException.withId(transaction.getId());
        }
        changeStatus(transaction, command.getPaymentStatus());
        transactionsRepository.save(transaction.toBuilder()
                .completed(Time.now())
                .build()
        );
        log.debug("Received transaction payment notification [command: {}].", command);
    }

    private void changeStatus(Transaction transaction, TransactionStatus targetStatus) {
        log.trace("Changing transaction status [transactionId: {}, status: {}, targetStatus: {}].", transaction.getId(), transaction.getStatus(), targetStatus);
        transaction.setStatus(targetStatus);
        transactionStatusChangedEventPublisher.publish(TransactionStatusChangedEvent.builder()
                .targetStatus(transaction.getStatus())
                .transactionId(transaction.getId())
                .build()
        );
    }

}