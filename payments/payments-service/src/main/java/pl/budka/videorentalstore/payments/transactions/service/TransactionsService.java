package pl.budka.videorentalstore.payments.transactions.service;

import pl.budka.videorentalstore.api.payments.transactions.model.TransactionDto;
import pl.budka.videorentalstore.payments.charges.exception.ChargeNotFoundException;
import pl.budka.videorentalstore.payments.transactions.dto.CreateTransactionCommand;
import pl.budka.videorentalstore.payments.transactions.dto.ReceiveTransactionPaymentNotificationCommand;
import pl.budka.videorentalstore.payments.transactions.exception.TransactionAlreadyFinishedException;
import pl.budka.videorentalstore.payments.transactions.exception.TransactionNotFoundException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface TransactionsService {

    TransactionDto getTransaction(@NotNull UUID transactionId) throws TransactionNotFoundException;

    TransactionDto createTransaction(@Valid @NotNull CreateTransactionCommand command) throws ChargeNotFoundException;

    void receiveTransactionPaymentNotification(@Valid @NotNull ReceiveTransactionPaymentNotificationCommand command) throws TransactionNotFoundException, TransactionAlreadyFinishedException;

}