package pl.budka.videorentalstore.payments.transactions.streams;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatusChangedEvent;

public interface TransactionStatusChangedEventPublisher {

    void publish(TransactionStatusChangedEvent event);

    interface Channel {

        @Output(TransactionStatusChangedEvent.QUEUE + "-output")
        MessageChannel output();

    }

}
