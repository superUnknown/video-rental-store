package pl.budka.videorentalstore.payments.transactions.streams;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatusChangedEvent;
import pl.budka.videorentalstore.api.payments.transactions.streams.TransactionStatusChangedEventListener;
import pl.budka.videorentalstore.payments.charges.exception.ChargeAlreadyFinishedException;
import pl.budka.videorentalstore.payments.charges.exception.ChargeNotFoundException;
import pl.budka.videorentalstore.payments.charges.service.ChargesService;
import pl.budka.videorentalstore.payments.transactions.exception.TransactionNotFoundException;
import pl.budka.videorentalstore.payments.transactions.service.TransactionsService;

import java.util.UUID;

@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@EnableBinding(TransactionStatusChangedEventListener.Channel.class)
public class TransactionStatusChangedEventListenerBean implements TransactionStatusChangedEventListener {

    private final TransactionsService transactionsService;

    private final ChargesService chargesService;

    @Override
    public void onEvent(TransactionStatusChangedEvent event) {
        log.info("onTransactionStatusChangedEvent [event: {}]", event);
        if (!event.getTargetStatus().isFinished()) {
            log.debug("Ignoring transaction status change event [event: {}].", event);
            return;
        }
        finishAssociatedCharge(event.getTransactionId());
    }

    private void finishAssociatedCharge(UUID transactionId) {
        try {
            chargesService.finishCharge(transactionsService.getTransaction(transactionId).getChargeId());
        } catch (TransactionNotFoundException | ChargeAlreadyFinishedException e) {
            log.warn("Failed to finish charge ({}).", e.getMessage());
        } catch (ChargeNotFoundException e) {
            throw new RuntimeException("Unexpected error occurred when finishing charge.", e);
        }
    }

}