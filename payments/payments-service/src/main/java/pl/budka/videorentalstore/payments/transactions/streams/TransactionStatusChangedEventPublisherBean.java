package pl.budka.videorentalstore.payments.transactions.streams;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatusChangedEvent;
import pl.budka.videorentalstore.core.streams.StreamEventPublisherSupport;

@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@EnableBinding(TransactionStatusChangedEventPublisher.Channel.class)
public class TransactionStatusChangedEventPublisherBean implements TransactionStatusChangedEventPublisher {

    private final StreamEventPublisherSupport publisher;

    private final Channel channel;

    @Override
    public void publish(TransactionStatusChangedEvent event) {
        publisher.enqueue(channel.output(), event);
    }

}