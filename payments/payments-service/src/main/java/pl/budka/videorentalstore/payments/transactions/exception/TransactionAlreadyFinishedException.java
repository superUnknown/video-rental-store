package pl.budka.videorentalstore.payments.transactions.exception;

import pl.budka.videorentalstore.api.payments.commons.model.PaymentsErrorCode;
import pl.budka.videorentalstore.payments.commons.exception.PaymentsApplicationException;

import java.util.UUID;

public class TransactionAlreadyFinishedException extends PaymentsApplicationException {

    private TransactionAlreadyFinishedException(String message) {
        super(message, PaymentsErrorCode.TRANSACTION_ALREADY_FINISHED);
    }

    public static TransactionAlreadyFinishedException withId(UUID transactionId) {
        return new TransactionAlreadyFinishedException(String.format("Transaction with id '%s' is already finished.", transactionId));
    }

}