package pl.budka.videorentalstore.payments.charges.exception;

import pl.budka.videorentalstore.api.payments.commons.model.PaymentsErrorCode;
import pl.budka.videorentalstore.payments.commons.exception.PaymentsApplicationException;

import java.util.UUID;

public class ChargeAlreadyFinishedException extends PaymentsApplicationException {

    private ChargeAlreadyFinishedException(String message) {
        super(message, PaymentsErrorCode.CHARGE_ALREADY_FINISHED);
    }

    public static ChargeAlreadyFinishedException withId(UUID chargeId) {
        return new ChargeAlreadyFinishedException(String.format("Charge with id '%s' is already finished.", chargeId));
    }

}