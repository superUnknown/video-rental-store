package pl.budka.videorentalstore.payments.transactions.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatus;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReceiveTransactionPaymentNotificationCommand {

    @NotNull
    private UUID transactionId;

    @NotNull
    private TransactionStatus paymentStatus;

}