package pl.budka.videorentalstore.payments.charges.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatus;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatusChangedEvent;
import pl.budka.videorentalstore.api.payments.charges.model.ModifyChargeCommand;
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand;
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentResultDto;
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionDto;
import pl.budka.videorentalstore.payments.charges.exception.ChargeAlreadyFinishedException;
import pl.budka.videorentalstore.payments.charges.exception.ChargeNotFoundException;
import pl.budka.videorentalstore.payments.charges.exception.ChargePaymentInProgressException;
import pl.budka.videorentalstore.payments.charges.exception.ChargeRefundNotSupportedException;
import pl.budka.videorentalstore.payments.charges.model.Charge;
import pl.budka.videorentalstore.payments.charges.repository.ChargesRepository;
import pl.budka.videorentalstore.payments.charges.streams.ChargeStatusChangedEventPublisher;
import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentCommand;
import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentResultDto;
import pl.budka.videorentalstore.payments.paymentgateway.http.PaymentGatewayApi;
import pl.budka.videorentalstore.payments.transactions.dto.CreateTransactionCommand;
import pl.budka.videorentalstore.payments.transactions.service.TransactionsService;
import pl.budka.videorentalstore.payments.utils.Mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class ChargesServiceBean implements ChargesService {

    private final ChargeStatusChangedEventPublisher chargeStatusChangedEventPublisher;

    private final TransactionsService transactionsService;

    private final PaymentGatewayApi paymentGatewayApi;

    private final ChargesRepository chargesRepository;

    @Override
    @Transactional(readOnly = true)
    public ChargeDto getCharge(UUID chargeId) throws ChargeNotFoundException {
        log.trace("Getting charge [chargeId: {}].", chargeId);
        ChargeDto result = Mapper.map(chargesRepository.findOne(chargeId)
                .orElseThrow(() -> ChargeNotFoundException.forId(chargeId)), ChargeDto.class
        );
        log.trace("Retrieved charge [chargeId: {}]: {}.", chargeId, result);
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public RequestChargePaymentResultDto requestChargePayment(RequestChargePaymentCommand command) {
        log.debug("Requesting charge payment [command: {}].", command);
        Charge charge = chargesRepository.save(Mapper.map(command, Charge.builder().build()));
        RequestChargePaymentResultDto result = requestChargePayment(charge);
        log.debug("Requested charge payment [command: {}]: {}.", command, result);
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void finishCharge(UUID chargeId) throws ChargeNotFoundException, ChargeAlreadyFinishedException {
        log.debug("Finishing charge [chargeId: {}].", chargeId);
        Charge charge = chargesRepository.findOne(chargeId).orElseThrow(() -> ChargeNotFoundException.forId(chargeId));
        if (charge.isFinished()) {
            throw ChargeAlreadyFinishedException.withId(chargeId);
        }
        changeStatus(charge, charge.isFullyPaid() ? ChargeStatus.COMPLETED : ChargeStatus.FAILED);
        chargesRepository.save(charge);
        log.debug("Finished charge [chargeId: {}, appliedStatus: {}].", chargeId, charge.getStatus());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public RequestChargePaymentResultDto modifyCharge(UUID chargeId, ModifyChargeCommand command) throws ChargeNotFoundException, ChargePaymentInProgressException, ChargeRefundNotSupportedException {
        log.debug("Modifying charge [chargeId: {}, command: {}].", chargeId, command);
        Charge charge = chargesRepository.findOne(chargeId).orElseThrow(() -> ChargeNotFoundException.forId(chargeId));
        if (charge.isPending()) {
            throw ChargePaymentInProgressException.withId(charge.getId());
        }
        if (!isExtendingChargeAmount(charge, command)) {
            throw new ChargeRefundNotSupportedException();
        }
        charge.setPricePositions(command.getPricePositions().stream()
                .map(it -> Mapper.map(it, Charge.PricePosition.class))
                .collect(Collectors.toList())
        );
        changeStatus(charge, ChargeStatus.PENDING);
        chargesRepository.save(charge);
        RequestChargePaymentResultDto result = requestChargePayment(charge);
        log.debug("Modified charge [chargeId: {}, command: {}]: {}.", chargeId, command, result);
        return result;
    }

    private static boolean isExtendingChargeAmount(Charge charge, ModifyChargeCommand command) {
        return charge.getTotalAmount().compareTo(command.getPricePositions().stream()
                .map(it -> it.getUnitPrice().multiply(BigDecimal.valueOf(it.getQuantity())))
                .reduce(BigDecimal.ZERO, BigDecimal::add)
        ) < 1;
    }

    private RequestChargePaymentResultDto requestChargePayment(Charge charge) {
        TransactionDto transaction = createTransaction(CreateTransactionCommand.builder()
                .amount(charge.getLeftToPay())
                .chargeId(charge.getId())
                .build()
        );
        RequestPaymentResultDto payment = paymentGatewayApi.requestPayment(RequestPaymentCommand.builder()
                .customer(Mapper.map(charge.getCustomer(), ChargeDto.Customer.class))
                .pricePositions(assemblePricePositions(charge))
                .transactionId(transaction.getId())
                .currency(charge.getCurrency())
                .build()
        );
        return RequestChargePaymentResultDto.builder()
                .charge(Mapper.map(chargesRepository.findOne(charge.getId()).get(), ChargeDto.class))
                .paymentLink(payment.getPaymentLink())
                .build();
    }

    private static List<RequestPaymentCommand.PricePosition> assemblePricePositions(Charge charge) {
        List<RequestPaymentCommand.PricePosition> pricePositions = charge.getPricePositions().stream()
                .map(it -> Mapper.map(it, RequestPaymentCommand.PricePosition.class))
                .collect(Collectors.toList());
        BigDecimal alreadyPaid = charge.getAlreadyPaidAmount();
        if (alreadyPaid.signum() > 0) {
            pricePositions.add(RequestPaymentCommand.PricePosition.builder()
                    .name("ALREADY PAID OR SOME OTHER NAME")
                    .unitPrice(alreadyPaid.negate())
                    .quantity(1)
                    .build()
            );
        }
        return pricePositions;
    }

    private TransactionDto createTransaction(CreateTransactionCommand command) {
        try {
            return transactionsService.createTransaction(command);
        } catch (ChargeNotFoundException e) {
            throw new RuntimeException("Unexpected error occurred when creating transaction.", e);
        }
    }

    private void changeStatus(Charge charge, ChargeStatus targetStatus) {
        log.trace("Changing charge status [chargeId: {}, status: {}, targetStatus: {}].", charge.getId(), charge.getStatus(), targetStatus);
        charge.setStatus(targetStatus);
        chargeStatusChangedEventPublisher.publish(ChargeStatusChangedEvent.builder()
                .targetStatus(charge.getStatus())
                .chargeId(charge.getId())
                .build()
        );
    }

}