package pl.budka.videorentalstore.payments.charges.exception;

import pl.budka.videorentalstore.api.payments.commons.model.PaymentsErrorCode;
import pl.budka.videorentalstore.payments.commons.exception.PaymentsApplicationException;

import java.util.UUID;

public class ChargePaymentInProgressException extends PaymentsApplicationException {

    private ChargePaymentInProgressException(String message) {
        super(message, PaymentsErrorCode.CHARGE_PAYMENT_IN_PROGRESS);
    }

    public static ChargePaymentInProgressException withId(UUID chargeId) {
        return new ChargePaymentInProgressException(String.format("Payment of charge with id '%s' is in progress.", chargeId));
    }

}