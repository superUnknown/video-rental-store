package pl.budka.videorentalstore.payments.paymentgateway.http;

import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentCommand;
import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentResultDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface PaymentGatewayApi {

    RequestPaymentResultDto requestPayment(@Valid @NotNull RequestPaymentCommand command);

}