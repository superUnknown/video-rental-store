package pl.budka.videorentalstore.payments.transactions.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateTransactionCommand {

    @NotNull
    private UUID chargeId;

    @NotNull
    @DecimalMin("0.01")
    private BigDecimal amount;

}