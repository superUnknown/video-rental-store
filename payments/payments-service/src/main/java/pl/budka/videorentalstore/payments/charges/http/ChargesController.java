package pl.budka.videorentalstore.payments.charges.http;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.budka.videorentalstore.api.payments.charges.http.ChargesApi;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto;
import pl.budka.videorentalstore.api.payments.charges.model.ModifyChargeCommand;
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand;
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentResultDto;
import pl.budka.videorentalstore.core.model.ApiResponse;
import pl.budka.videorentalstore.payments.charges.service.ChargesService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

import static pl.budka.videorentalstore.core.utils.HttpApiHandler.execute;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ChargesController implements ChargesApi {

    private final ChargesService chargesService;

    @Override
    public ApiResponse<ChargeDto> getCharge(@NotNull @PathVariable(CHARGE_ID_VARIABLE) UUID chargeId) {
        log.info("Getting charge [chargeId: {}].", chargeId);
        ApiResponse<ChargeDto> result = execute(() -> chargesService.getCharge(chargeId));
        log.info("Retrieved charge [chargeId: {}]: {}.", chargeId, result);
        return result;
    }

    @Override
    public ApiResponse<RequestChargePaymentResultDto> requestChargePayment(@Valid @NotNull @RequestBody RequestChargePaymentCommand command) {
        log.info("Requesting charge payment [command: {}].", command);
        ApiResponse<RequestChargePaymentResultDto> result = execute(() -> chargesService.requestChargePayment(command));
        log.info("Requested charge payment [command: {}]: {}.", command, result);
        return result;
    }

    @Override
    public ApiResponse<RequestChargePaymentResultDto> modifyCharge(@NotNull @PathVariable(CHARGE_ID_VARIABLE) UUID chargeId, @Valid @NotNull @RequestBody ModifyChargeCommand command) {
        log.info("Modifying charge [chargeId: {}, command: {}].", chargeId, command);
        ApiResponse<RequestChargePaymentResultDto> result = execute(() -> chargesService.modifyCharge(chargeId, command));
        log.info("Modified charge payment [command: {}]: {}.", command, result);
        return result;
    }

}