package pl.budka.videorentalstore.payments.charges.exception;

import pl.budka.videorentalstore.api.payments.commons.model.PaymentsErrorCode;
import pl.budka.videorentalstore.payments.commons.exception.PaymentsApplicationException;

import java.util.UUID;

public class ChargeNotFoundException extends PaymentsApplicationException {

    private ChargeNotFoundException(String message) {
        super(message, PaymentsErrorCode.CHARGE_NOT_FOUND);
    }

    public static ChargeNotFoundException forId(UUID chargeId) {
        return new ChargeNotFoundException(String.format("Charge with id '%s' doesn't exist.", chargeId));
    }

}