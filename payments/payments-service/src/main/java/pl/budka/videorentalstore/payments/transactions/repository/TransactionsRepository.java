package pl.budka.videorentalstore.payments.transactions.repository;

import org.springframework.data.repository.Repository;
import pl.budka.videorentalstore.payments.transactions.model.Transaction;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TransactionsRepository extends Repository<Transaction, UUID> {

    Optional<Transaction> findOne(UUID transactionId);

    Transaction save(Transaction transaction);

    List<Transaction> findAll();

    void deleteAll();

}