package pl.budka.videorentalstore.payments.charges.service;

import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto;
import pl.budka.videorentalstore.api.payments.charges.model.ModifyChargeCommand;
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand;
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentResultDto;
import pl.budka.videorentalstore.payments.charges.exception.ChargeAlreadyFinishedException;
import pl.budka.videorentalstore.payments.charges.exception.ChargeNotFoundException;
import pl.budka.videorentalstore.payments.charges.exception.ChargePaymentInProgressException;
import pl.budka.videorentalstore.payments.charges.exception.ChargeRefundNotSupportedException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface ChargesService {

    ChargeDto getCharge(@NotNull UUID chargeId) throws ChargeNotFoundException;

    RequestChargePaymentResultDto requestChargePayment(@Valid @NotNull RequestChargePaymentCommand command);

    void finishCharge(@NotNull UUID chargeId) throws ChargeNotFoundException, ChargeAlreadyFinishedException;

    RequestChargePaymentResultDto modifyCharge(@NotNull UUID chargeId, @Valid @NotNull ModifyChargeCommand command) throws ChargeNotFoundException, ChargePaymentInProgressException, ChargeRefundNotSupportedException;

}