package pl.budka.videorentalstore.payments.charges.repository;

import org.springframework.data.repository.Repository;
import pl.budka.videorentalstore.payments.charges.model.Charge;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ChargesRepository extends Repository<Charge, UUID> {

    Optional<Charge> findOne(UUID chargeId);

    Charge save(Charge charge);

    List<Charge> findAll();

    void deleteAll();

}