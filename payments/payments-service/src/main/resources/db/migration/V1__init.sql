CREATE TABLE customer (
    id UUID PRIMARY KEY,
    firstname TEXT NOT NULL,
    surname TEXT NOT NULL,
    email TEXT NOT NULL
);

CREATE TABLE charge (
    id UUID PRIMARY KEY,
    status TEXT NOT NULL,
    currency TEXT NOT NULL,
    created TIMESTAMP NOT NULL,
    customer_id UUID NOT NULL REFERENCES customer(id)
);

CREATE TABLE price_position (
    id UUID PRIMARY KEY,
    name TEXT NOT NULL,
    quantity INTEGER NOT NULL,
    unit_price NUMERIC(19,2) NOT NULL,
    charge_id UUID NOT NULL REFERENCES charge(id)
);

CREATE TABLE "transaction" (
    id UUID PRIMARY KEY,
    status TEXT NOT NULL,
    created TIMESTAMP NOT NULL,
    completed TIMESTAMP,
    amount NUMERIC(19,2) NOT NULL,
    charge_id UUID NOT NULL REFERENCES charge(id)
);

