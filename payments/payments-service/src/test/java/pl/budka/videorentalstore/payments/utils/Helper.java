package pl.budka.videorentalstore.payments.utils;

import lombok.AllArgsConstructor;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.budka.videorentalstore.payments.charges.model.Charge;
import pl.budka.videorentalstore.payments.charges.repository.ChargesRepository;

import java.util.Optional;
import java.util.UUID;

@Component
@AllArgsConstructor
public class Helper {

    private final ChargesRepository chargesRepository;

    @Transactional(readOnly = true)
    public Optional<Charge> getCharge(UUID chargeId) {
        Optional<Charge> charge = chargesRepository.findOne(chargeId);
        charge.ifPresent(it -> Hibernate.initialize(it.getPricePositions()));
        charge.ifPresent(it -> Hibernate.initialize(it.getTransactions()));
        return charge;
    }


}