package pl.budka.videorentalstore.payments.transactions.streams

import com.neovisionaries.i18n.CurrencyCode
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatus
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatusChangedEvent
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatus
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatusChangedEvent
import pl.budka.videorentalstore.payments.AbstractPaymentsApplicationSpecIT
import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentCommand
import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentResultDto
import pl.budka.videorentalstore.payments.transactions.model.Transaction

class TransactionStatusChangedEventListenerSpecIT extends AbstractPaymentsApplicationSpecIT {

    def "should finish associated charge as COMPLETED when received status changed event of first transaction for that charge successfully completed"() {
        given:
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> RequestPaymentResultDto.builder()
                    .paymentLink("http://example.com/path")
                    .build()
            UUID transactionId = requestChargePayment(RequestChargePaymentCommand.builder()
                    .customer(ChargeDto.Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build())
                    .pricePositions([pricePosition("The Dark Knight", 30.00, 1)])
                    .currency(CurrencyCode.SEK)
                    .build()
            ).result.charge.transactions[0].id
            receiveTransactionPaymentNotification(transactionId)
            TransactionStatusChangedEvent event = collectEvents(transactionStatusChangedEventOutputChannel.output(), TransactionStatusChangedEvent)[0]
            cleanQueues()
        expect:
            with(transactionsRepository.findOne(transactionId).get() as Transaction) {
                assert getStatus() == TransactionStatus.COMPLETED
                assert getCharge().status == ChargeStatus.PENDING
            }
        when:
            sendTransactionStatusChangedEvent(event)
        then:
            0 * _

            noExceptionThrown()

            with(transactionsRepository.findOne(transactionId).get() as Transaction) {
                getStatus() == TransactionStatus.COMPLETED
                getCharge().status == ChargeStatus.COMPLETED
            }
            List<ChargeStatusChangedEvent> events = collectEvents(chargeStatusChangedEventOutputChannel.output(), ChargeStatusChangedEvent)
            events.size() == 1
            with(events[0]) {
                getTime()
                getChargeId() == transactionsRepository.findOne(transactionId).get().charge.id
                getTargetStatus() == ChargeStatus.COMPLETED
            }

    }

    def "should finish associated charge as FAILED when received status changed event of first transaction for that charge that failed"() {
        given:
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> RequestPaymentResultDto.builder()
                    .paymentLink("http://example.com/path")
                    .build()
            UUID transactionId = requestChargePayment(RequestChargePaymentCommand.builder()
                    .customer(ChargeDto.Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build())
                    .pricePositions([pricePosition("The Dark Knight", 30.00, 1)])
                    .currency(CurrencyCode.SEK)
                    .build()
            ).result.charge.transactions[0].id
            receiveTransactionPaymentNotification(transactionId, true)
            TransactionStatusChangedEvent event = collectEvents(transactionStatusChangedEventOutputChannel.output(), TransactionStatusChangedEvent)[0]
            cleanQueues()
        expect:
            with(transactionsRepository.findOne(transactionId).get() as Transaction) {
                assert getStatus() == TransactionStatus.FAILED
                assert getCharge().status == ChargeStatus.PENDING
            }
        when:
            sendTransactionStatusChangedEvent(event)
        then:
            0 * _

            noExceptionThrown()

            with(transactionsRepository.findOne(transactionId).get() as Transaction) {
                getStatus() == TransactionStatus.FAILED
                getCharge().status == ChargeStatus.FAILED
            }
            List<ChargeStatusChangedEvent> events = collectEvents(chargeStatusChangedEventOutputChannel.output(), ChargeStatusChangedEvent)
            events.size() == 1
            with(events[0]) {
                getTime()
                getChargeId() == transactionsRepository.findOne(transactionId).get().charge.id
                getTargetStatus() == ChargeStatus.FAILED
            }

    }

    def "should not finish charge when received status changed event of unknown transaction"() {
        expect:
            transactionsRepository.findAll().isEmpty()
        when:
            sendTransactionStatusChangedEvent(TransactionStatusChangedEvent.builder()
                    .targetStatus(TransactionStatus.COMPLETED)
                    .transactionId(UUID.randomUUID())
                    .build()
            )
        then:
            0 * _
            noExceptionThrown()

            collectEvents(chargeStatusChangedEventOutputChannel.output(), ChargeStatusChangedEvent).isEmpty()

    }

    def "should not finish charge when received status changed event of transaction associated with already finished charge"() {
        given:
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> RequestPaymentResultDto.builder()
                    .paymentLink("http://example.com/path")
                    .build()
            UUID transactionId = requestChargePayment(RequestChargePaymentCommand.builder()
                    .customer(ChargeDto.Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build())
                    .pricePositions([pricePosition("The Dark Knight", 30.00, 1)])
                    .currency(CurrencyCode.SEK)
                    .build()
            ).result.charge.transactions[0].id
            receiveTransactionPaymentNotification(transactionId)
            TransactionStatusChangedEvent event = collectEvents(transactionStatusChangedEventOutputChannel.output(), TransactionStatusChangedEvent)[0]
            sendTransactionStatusChangedEvent(event)
            cleanQueues()
        expect:
            with(transactionsRepository.findOne(transactionId).get() as Transaction) {
                assert getStatus() == TransactionStatus.COMPLETED
                assert getCharge().status == ChargeStatus.COMPLETED
            }
        when:
            sendTransactionStatusChangedEvent(event)
        then:
            0 * _
            noExceptionThrown()

            with(transactionsRepository.findOne(transactionId).get() as Transaction) {
                getStatus() == TransactionStatus.COMPLETED
                getCharge().status == ChargeStatus.COMPLETED
            }

            collectEvents(chargeStatusChangedEventOutputChannel.output(), ChargeStatusChangedEvent).isEmpty()
    }

}