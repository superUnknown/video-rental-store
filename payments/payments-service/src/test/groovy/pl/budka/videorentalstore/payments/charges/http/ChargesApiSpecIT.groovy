package pl.budka.videorentalstore.payments.charges.http

import com.neovisionaries.i18n.CurrencyCode
import org.springframework.http.HttpStatus
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatus
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatusChangedEvent
import pl.budka.videorentalstore.api.payments.charges.model.ModifyChargeCommand
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentResultDto
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatus
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatusChangedEvent
import pl.budka.videorentalstore.core.model.ApiResponse
import pl.budka.videorentalstore.core.utils.Time
import pl.budka.videorentalstore.payments.AbstractPaymentsApplicationSpecIT
import pl.budka.videorentalstore.payments.charges.model.Charge
import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentCommand
import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentResultDto

import java.time.Instant

class ChargesApiSpecIT extends AbstractPaymentsApplicationSpecIT {

    private static final String NOW = "2019-06-12T15:30:00.000Z"

    def setup() {
        Time.setClock(Instant.parse(NOW))
    }

    def "should create charge payment and return payment link when payment is requested successfully in external payment gateway"() {
        given:
            RequestChargePaymentCommand command = RequestChargePaymentCommand.builder()
                    .customer(ChargeDto.Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build())
                    .pricePositions([pricePosition("The Dark Knight", 30.00, 1), pricePosition("Black Panther", 15.37, 2)])
                    .currency(CurrencyCode.SEK)
                    .build()
        when:
            ApiResponse<RequestChargePaymentResultDto> response = requestChargePayment(command)
        then:
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> { RequestPaymentCommand cmd ->
                assert cmd.transactionId
                assert cmd.currency == command.currency
                assert cmd.customer == command.customer
                assert cmd.pricePositions.collect { pricePosition(it.name, it.unitPrice, it.quantity) } == command.pricePositions
                return RequestPaymentResultDto.builder().paymentLink("http://example.com/path").build()
            }
            0 * _

            response.isOk()
            with(response.result) {
                getPaymentLink() == "http://example.com/path"
                getCharge().id
                getCharge().status == ChargeStatus.PENDING
                getCharge().currency == CurrencyCode.SEK
                getCharge().created == Instant.parse(NOW)
                getCharge().customer.firstname == command.customer.firstname
                getCharge().customer.surname == command.customer.surname
                getCharge().customer.email == command.customer.email
                getCharge().pricePositions.size() == 2 // TODO better asserts
                getCharge().transactions.size() == 1
                getCharge().transactions[0].id
                getCharge().transactions[0].status == TransactionStatus.CREATED
                getCharge().transactions[0].created == Instant.parse(NOW)
                !getCharge().transactions[0].completed
                getCharge().transactions[0].amount == 60.74
            }

            chargesRepository.findAll().size() == 1
            with(helper.getCharge(response.result.charge.id).get() as Charge) {
                getStatus() == response.result.charge.status
                getCurrency() == response.result.charge.currency
                getCreated() == response.result.charge.created
//            TODO ... &  better asserts
                getPricePositions().size() == 2
                getTransactions().size() == 1
            }
    }

    def "should not create charge payment when payment request in external payment gateway failed"() {
        given:
            RequestChargePaymentCommand command = RequestChargePaymentCommand.builder()
                    .customer(ChargeDto.Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build())
                    .pricePositions([pricePosition("The Dark Knight", 30.00, 1), pricePosition("Black Panther", 15.37, 2)])
                    .currency(CurrencyCode.SEK)
                    .build()
        when:
            ApiResponse<RequestChargePaymentResultDto> response = requestChargePayment(command)
        then:
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> {
                throw new RuntimeException("test payment exception")
            }
            0 * _

            !response.isOk()
            response.error.code == HttpStatus.INTERNAL_SERVER_ERROR.name()
            !response.result

            chargesRepository.findAll().isEmpty()
    }

    def "should modify charge, request payment for additional amount and return payment link when charge payment is finished"() {
        given:
            ChargeDto charge = createAndPayCharge()
            ModifyChargeCommand command = ModifyChargeCommand.builder()
                    .pricePositions([pricePosition("The Dark Knight", 35.00, 1), pricePosition("Black Panther", 20.00, 1)])
                    .build()
        expect:
            chargesRepository.findOne(charge.id).get().status == ChargeStatus.COMPLETED
        when:
            ApiResponse<RequestChargePaymentResultDto> response = modifyCharge(charge.id, command)
        then:
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> { RequestPaymentCommand cmd ->
                assert cmd.transactionId
                assert cmd.currency == charge.currency
                assert cmd.customer == charge.customer
                assert cmd.pricePositions.collect {
                    pricePosition(it.name, it.unitPrice, it.quantity)
                } == command.pricePositions + pricePosition("ALREADY PAID OR SOME OTHER NAME", -45.00, 1)
                return RequestPaymentResultDto.builder().paymentLink("http://example.com/path").build()
            }
            0 * _

            response.isOk()
            with(response.result) {
                getPaymentLink() == "http://example.com/path"
                getCharge().id
                getCharge().status == ChargeStatus.PENDING
                getCharge().currency == CurrencyCode.SEK
                getCharge().created == Instant.parse(NOW)
                getCharge().customer == charge.customer
                getCharge().pricePositions.size() == 2 // TODO better asserts
                getCharge().transactions.size() == 2

                getCharge().transactions[0].id
                getCharge().transactions[0].status == TransactionStatus.COMPLETED
                getCharge().transactions[0].created
                getCharge().transactions[0].completed
                getCharge().transactions[0].amount == 45.00

                getCharge().transactions[1].id
                getCharge().transactions[1].status == TransactionStatus.CREATED
                getCharge().transactions[1].created
                !getCharge().transactions[1].completed
                getCharge().transactions[1].amount == 10.00
            }

            chargesRepository.findAll().size() == 1
            with(helper.getCharge(response.result.charge.id).get() as Charge) {
                getStatus() == response.result.charge.status
                getCurrency() == response.result.charge.currency
                getCreated() == response.result.charge.created
//            TODO ... &  better asserts
                getPricePositions().size() == 2
                getTransactions().size() == 2
            }

            List<ChargeStatusChangedEvent> events = collectEvents(chargeStatusChangedEventOutputChannel.output(), ChargeStatusChangedEvent)
            events.size() == 1
            with(events[0]) {
                getTime()
                getChargeId() == charge.id
                getTargetStatus() == ChargeStatus.PENDING
            }

    }

    def "should modify charge, request payment for additional amount and return payment link when previous payment after charge modification failed"() {
        given:
            ChargeDto charge = createAndPayCharge()
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> RequestPaymentResultDto.builder().paymentLink("http://example.com/path").build()
            charge = modifyCharge(charge.id, ModifyChargeCommand.builder()
                    .pricePositions([pricePosition("The Dark Knight", 45.00, 1), pricePosition("Black Panther", 20.00, 1)])
                    .build()
            ).result.charge
            receiveTransactionPaymentNotification(charge.transactions[1].id, true)
            sendTransactionStatusChangedEvent(collectEvents(transactionStatusChangedEventOutputChannel.output(), TransactionStatusChangedEvent)[0])
            ModifyChargeCommand command = ModifyChargeCommand.builder()
                    .pricePositions([pricePosition("The Dark Knight", 44.00, 1), pricePosition("Black Panther", 25.00, 1)])
                    .build()
            cleanQueues()
        expect:
            with(helper.getCharge(charge.id).get() as Charge) {
                assert getStatus() == ChargeStatus.FAILED
                assert getTransactions().sort { it.created }[0].status == TransactionStatus.COMPLETED
                assert getTransactions().sort { it.created }[1].status == TransactionStatus.FAILED
            }
        when:
            ApiResponse<RequestChargePaymentResultDto> response = modifyCharge(charge.id, command)
        then:
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> { RequestPaymentCommand cmd ->
                assert cmd.transactionId
                assert cmd.currency == charge.currency
                assert cmd.customer == charge.customer
                assert cmd.pricePositions.collect {
                    pricePosition(it.name, it.unitPrice, it.quantity)
                } == command.pricePositions + pricePosition("ALREADY PAID OR SOME OTHER NAME", -45.00, 1)
                return RequestPaymentResultDto.builder().paymentLink("http://example.com/path").build()
            }
            0 * _

            response.isOk()
            with(response.result) {
                getPaymentLink() == "http://example.com/path"
                getCharge().id
                getCharge().status == ChargeStatus.PENDING
                getCharge().currency == CurrencyCode.SEK
                getCharge().created == Instant.parse(NOW)
                getCharge().customer == charge.customer
                getCharge().pricePositions.size() == 2 // TODO better asserts
                getCharge().transactions.size() == 3

                getCharge().transactions[0].id
                getCharge().transactions[0].status == TransactionStatus.COMPLETED
                getCharge().transactions[0].created
                getCharge().transactions[0].completed
                getCharge().transactions[0].amount == 45.00

                getCharge().transactions[1].id
                getCharge().transactions[1].status == TransactionStatus.FAILED
                getCharge().transactions[1].created
                getCharge().transactions[1].completed
                getCharge().transactions[1].amount == 20.00

                getCharge().transactions[2].id
                getCharge().transactions[2].status == TransactionStatus.CREATED
                getCharge().transactions[2].created
                !getCharge().transactions[2].completed
                getCharge().transactions[2].amount == 24.00
            }

            chargesRepository.findAll().size() == 1
            with(helper.getCharge(response.result.charge.id).get() as Charge) {
                getStatus() == response.result.charge.status
                getCurrency() == response.result.charge.currency
                getCreated() == response.result.charge.created
//            TODO ... &  better asserts
                getPricePositions().size() == 2
                getTransactions().size() == 3
            }

            List<ChargeStatusChangedEvent> events = collectEvents(chargeStatusChangedEventOutputChannel.output(), ChargeStatusChangedEvent)
            events.size() == 1
            with(events[0]) {
                getTime()
                getChargeId() == charge.id
                getTargetStatus() == ChargeStatus.PENDING
            }

    }

    def "should not modify charge when payment request for additional amount in external payment gateway failed"() {
        given:
            ChargeDto charge = createAndPayCharge()
            ModifyChargeCommand command = ModifyChargeCommand.builder()
                    .pricePositions([pricePosition("The Dark Knight", 35.00, 1), pricePosition("Black Panther", 20.00, 1)])
                    .build()
        expect:
            chargesRepository.findOne(charge.id).get().status == ChargeStatus.COMPLETED
        when:
            ApiResponse<RequestChargePaymentResultDto> response = modifyCharge(charge.id, command)
        then:
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> {
                throw new RuntimeException("test payment exception")
            }
            0 * _

            !response.isOk()
            response.error.code == HttpStatus.INTERNAL_SERVER_ERROR.name()
            !response.result

            chargesRepository.findAll().size() == 1
            with(helper.getCharge(charge.id).get() as Charge) {
                getStatus() == ChargeStatus.COMPLETED
//            TODO ... &  better asserts
                getPricePositions().size() == 2
                getTransactions().size() == 1
            }

            collectEvents(chargeStatusChangedEventOutputChannel.output(), ChargeStatusChangedEvent).isEmpty()

    }

    def "should not modify charge when its payment is in progress"() {
//        TODO
    }

    def "should not modify charge when its new price positions list total amount is smaller than actual"() {
//        TODO
    }

    private ChargeDto createAndPayCharge() {
        1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> RequestPaymentResultDto.builder()
                .paymentLink("http://example.com/path")
                .build()
        ChargeDto charge = requestChargePayment(RequestChargePaymentCommand.builder()
                .customer(ChargeDto.Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build())
                .pricePositions([pricePosition("The Dark Knight", 30.00, 1), pricePosition("Black Panther", 15.00, 1)])
                .currency(CurrencyCode.SEK)
                .build()
        ).result.charge
        receiveTransactionPaymentNotification(charge.transactions[0].id)
        sendTransactionStatusChangedEvent(collectEvents(transactionStatusChangedEventOutputChannel.output(), TransactionStatusChangedEvent)[0])
        cleanQueues()
        return charge
    }

}