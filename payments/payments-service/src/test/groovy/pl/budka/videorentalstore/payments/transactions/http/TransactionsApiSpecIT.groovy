package pl.budka.videorentalstore.payments.transactions.http

import com.neovisionaries.i18n.CurrencyCode
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand
import pl.budka.videorentalstore.api.payments.commons.model.PaymentsErrorCode
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatus
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatusChangedEvent
import pl.budka.videorentalstore.core.model.ApiResponse
import pl.budka.videorentalstore.core.utils.Time
import pl.budka.videorentalstore.payments.AbstractPaymentsApplicationSpecIT
import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentCommand
import pl.budka.videorentalstore.payments.paymentgateway.dto.RequestPaymentResultDto
import pl.budka.videorentalstore.payments.transactions.model.Transaction

import java.time.Instant

class TransactionsApiSpecIT extends AbstractPaymentsApplicationSpecIT {

    private static final String NOW = "2019-06-12T15:30:00.000Z"

    def setup() {
        Time.setClock(Instant.parse(NOW))
    }

    def "should finish transaction as COMPLETED when received transaction payment notification and payment succeed"() {
        given:
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> RequestPaymentResultDto.builder()
                    .paymentLink("http://example.com/path")
                    .build()
            UUID transactionId = requestChargePayment(RequestChargePaymentCommand.builder()
                    .customer(ChargeDto.Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build())
                    .pricePositions([pricePosition("The Dark Knight", 30.00, 1)])
                    .currency(CurrencyCode.SEK)
                    .build()
            ).result.charge.transactions[0].id
        expect:
            transactionsRepository.findOne(transactionId).get().status == TransactionStatus.CREATED
        when:
            ApiResponse<Void> response = receiveTransactionPaymentNotification(transactionId)
        then:
            0 * _
            response.isOk()
            !response.result

            with(transactionsRepository.findOne(transactionId).get() as Transaction) {
                getStatus() == TransactionStatus.COMPLETED
                getCompleted() == Time.now()
            }

            List<TransactionStatusChangedEvent> events = collectEvents(transactionStatusChangedEventOutputChannel.output(), TransactionStatusChangedEvent)
            events.size() == 1
            with(events[0]) {
                getTime() == Time.now()
                getTransactionId() == transactionId
                getTargetStatus() == TransactionStatus.COMPLETED
            }

    }

    def "should finish transaction as FAILED when received transaction payment notification and payment failed"() {
        given:
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> RequestPaymentResultDto.builder()
                    .paymentLink("http://example.com/path")
                    .build()
            UUID transactionId = requestChargePayment(RequestChargePaymentCommand.builder()
                    .customer(ChargeDto.Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build())
                    .pricePositions([pricePosition("The Dark Knight", 30.00, 1)])
                    .currency(CurrencyCode.SEK)
                    .build()
            ).result.charge.transactions[0].id
        expect:
            transactionsRepository.findOne(transactionId).get().status == TransactionStatus.CREATED
        when:
            ApiResponse<Void> response = receiveTransactionPaymentNotification(transactionId, true)
        then:
            0 * _
            response.isOk()
            !response.result

            with(transactionsRepository.findOne(transactionId).get() as Transaction) {
                getStatus() == TransactionStatus.FAILED
                getCompleted() == Time.now()
            }

            List<TransactionStatusChangedEvent> events = collectEvents(transactionStatusChangedEventOutputChannel.output(), TransactionStatusChangedEvent)
            events.size() == 1
            with(events[0]) {
                getTime() == Time.now()
                getTransactionId() == transactionId
                getTargetStatus() == TransactionStatus.FAILED
            }

    }

    def "should not finish transaction when received payment notification already finished transaction"() {
        given:
            1 * paymentGatewayApi.requestPayment(_ as RequestPaymentCommand) >> RequestPaymentResultDto.builder()
                    .paymentLink("http://example.com/path")
                    .build()
            UUID transactionId = requestChargePayment(RequestChargePaymentCommand.builder()
                    .customer(ChargeDto.Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build())
                    .pricePositions([pricePosition("The Dark Knight", 30.00, 1)])
                    .currency(CurrencyCode.SEK)
                    .build()
            ).result.charge.transactions[0].id
            receiveTransactionPaymentNotification(transactionId, failed)
            cleanQueues()
        expect:
            transactionsRepository.findOne(transactionId).get().status == (failed ? TransactionStatus.FAILED : TransactionStatus.COMPLETED)
        when:
            ApiResponse<Void> response = receiveTransactionPaymentNotification(transactionId, failed)
        then:
            0 * _

            !response.isOk()
            response.error.code == PaymentsErrorCode.TRANSACTION_ALREADY_FINISHED.name()
            !response.result

            collectEvents(transactionStatusChangedEventOutputChannel.output(), TransactionStatusChangedEvent).isEmpty()
        where:
            failed << [true, false]

    }

    def "should not finish transaction when received payment notification of unknown transaction"() {
        expect:
            transactionsRepository.findAll().isEmpty()
        when:
            ApiResponse<Void> response = receiveTransactionPaymentNotification(UUID.randomUUID())
        then:
            0 * _

            !response.isOk()
            response.error.code == PaymentsErrorCode.TRANSACTION_NOT_FOUND.name()
            !response.result

            transactionsRepository.findAll().isEmpty()

            collectEvents(transactionStatusChangedEventOutputChannel.output(), TransactionStatusChangedEvent).isEmpty()

    }

}