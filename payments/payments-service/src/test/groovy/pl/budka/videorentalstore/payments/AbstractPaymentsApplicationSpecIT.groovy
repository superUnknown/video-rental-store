package pl.budka.videorentalstore.payments

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.cloud.stream.test.binder.MessageCollector
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.MessageBuilder
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import pl.budka.videorentalstore.api.payments.charges.http.ChargesApi
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto
import pl.budka.videorentalstore.api.payments.charges.model.ModifyChargeCommand
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentResultDto
import pl.budka.videorentalstore.api.payments.transactions.http.TransactionsApi
import pl.budka.videorentalstore.api.payments.transactions.model.TransactionStatusChangedEvent
import pl.budka.videorentalstore.api.payments.transactions.streams.TransactionStatusChangedEventListener
import pl.budka.videorentalstore.core.model.ApiResponse
import pl.budka.videorentalstore.payments.charges.repository.ChargesRepository
import pl.budka.videorentalstore.payments.charges.streams.ChargeStatusChangedEventPublisher
import pl.budka.videorentalstore.payments.paymentgateway.http.PaymentGatewayApi
import pl.budka.videorentalstore.payments.transactions.repository.TransactionsRepository
import pl.budka.videorentalstore.payments.transactions.streams.TransactionStatusChangedEventPublisher
import pl.budka.videorentalstore.payments.utils.Helper
import spock.lang.Specification
import spock.mock.DetachedMockFactory

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE
import static org.springframework.messaging.MessageHeaders.CONTENT_TYPE
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put

@AutoConfigureMockMvc
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
abstract class AbstractPaymentsApplicationSpecIT extends Specification {

    @Autowired
    protected TransactionStatusChangedEventPublisher.Channel transactionStatusChangedEventOutputChannel

    @Autowired
    protected TransactionStatusChangedEventListener.Channel transactionStatusChangedEventInputChannel

    @Autowired
    protected ChargeStatusChangedEventPublisher.Channel chargeStatusChangedEventOutputChannel

    @Autowired
    protected TransactionsRepository transactionsRepository

    @Autowired
    protected PaymentGatewayApi paymentGatewayApi

    @Autowired
    protected ChargesRepository chargesRepository

    @Autowired
    protected MessageCollector messageCollector

    @Autowired
    protected ObjectMapper mapper

    @Autowired
    protected MockMvc mockMvc

    @Autowired
    protected Helper helper

    def setup() {
        clean()
    }

    def cleanup() {
        clean()
    }

    void clean() {
        chargesRepository.deleteAll()
        cleanQueues()
    }

    void cleanQueues() {
        messageCollector.forChannel(transactionStatusChangedEventOutputChannel.output()).clear()
        messageCollector.forChannel(chargeStatusChangedEventOutputChannel.output()).clear()
    }

    @TestConfiguration
    static class Configuration {

        private DetachedMockFactory factory = new DetachedMockFactory()

        @Bean
        @Primary
        PaymentGatewayApi paymentGatewayApi() {
            return factory.Mock(PaymentGatewayApi)
        }

    }

    protected ApiResponse<RequestChargePaymentResultDto> requestChargePayment(RequestChargePaymentCommand command) {
        MockHttpServletResponse response = mockMvc.perform(post("${ChargesApi.REQUEST_CHARGE_PAYMENT_PATH}")
                .content(mapper.writeValueAsString(command))
                .contentType(APPLICATION_JSON_UTF8_VALUE)
        ).andReturn().getResponse()
        return mapper.readValue(response.getContentAsString(), new TypeReference<ApiResponse<RequestChargePaymentResultDto>>() {})
    }

    protected ApiResponse<RequestChargePaymentResultDto> modifyCharge(UUID chargeId, ModifyChargeCommand command) {
        MockHttpServletResponse response = mockMvc.perform(put("${ChargesApi.MODIFY_CHARGE_PATH}", chargeId)
                .content(mapper.writeValueAsString(command))
                .contentType(APPLICATION_JSON_UTF8_VALUE)
        ).andReturn().getResponse()
        return mapper.readValue(response.getContentAsString(), new TypeReference<ApiResponse<RequestChargePaymentResultDto>>() {})
    }

    protected ApiResponse<Void> receiveTransactionPaymentNotification(UUID transactionId, boolean failed = false) {
        MockHttpServletResponse response = mockMvc.perform(get("${TransactionsApi.RECEIVE_TRANSACTION_PAYMENT_NOTIFICATION_PATH}", transactionId)
                .param(TransactionsApi.FAILED_VARIABLE, "${failed}")
        ).andReturn().getResponse()
        return mapper.readValue(response.getContentAsString(), new TypeReference<ApiResponse<Void>>() {})
    }

    protected static ChargeDto.PricePosition pricePosition(String name, BigDecimal unitPrice, Integer quantity) {
        return ChargeDto.PricePosition.builder()
                .unitPrice(unitPrice)
                .quantity(quantity)
                .name(name)
                .build()
    }

    protected void sendTransactionStatusChangedEvent(TransactionStatusChangedEvent event) {
        transactionStatusChangedEventInputChannel.input().send(assembleMessage(event))
    }

    private Message<String> assembleMessage(Object event) {
        return MessageBuilder.withPayload(mapper.writeValueAsString(event))
                .setHeader(CONTENT_TYPE, APPLICATION_JSON_UTF8_VALUE)
                .build()
    }

    protected <T> List<T> collectEvents(MessageChannel channel, Class<T> clazz) {
        List<T> events = []
        Message<String> event
        while ((event = messageCollector.forChannel(channel).poll() as Message<String>) != null) {
            events.add(mapper.readValue(event.payload, clazz))
        }
        return events
    }

}