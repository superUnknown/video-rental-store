package pl.budka.videorentalstore.api.rentals.rentals.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateRentalCommand {

    @NotNull
    private UUID accountId;

    @NotNull
    private Instant endingUntil;

    @NotEmpty
    private Set<UUID> movieIds;

}