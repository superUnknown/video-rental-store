package pl.budka.videorentalstore.api.rentals.rentals.model;

public enum RentalStatus {
    REQUESTED,
    ACTIVE,
    REJECTED,
    COMPLETED
}