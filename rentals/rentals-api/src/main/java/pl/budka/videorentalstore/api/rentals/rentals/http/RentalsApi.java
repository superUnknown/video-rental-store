package pl.budka.videorentalstore.api.rentals.rentals.http;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalCommand;
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalResultDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalCommand;
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalResultDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalCostEstimationDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalDetailedStatus;
import pl.budka.videorentalstore.core.model.ApiResponse;
import pl.budka.videorentalstore.core.model.Result;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@FeignClient(RentalsApi.NAME)
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface RentalsApi {

    String NAME = "rentals-api";

    String RENTAL_ID_VARIABLE = "rentalId";

    String MOVIE_ID_VARIABLE = "movieId";

    String ENDING_UNTIL_VARIABLE = "endingUntil";

    String CREATE_RENTAL_PATH = "/rentals";

    String GET_RENTAL_DETAILED_STATUS_PATH = "/rentals/{" + RENTAL_ID_VARIABLE + "}/detailed-status";

    String FINISH_RENTAL_PATH = "/rentals/finish";

    String ESTIMATE_RENTAL_COST_PATH = "/rentals/cost-estimation";

    @GetMapping(GET_RENTAL_DETAILED_STATUS_PATH)
    ApiResponse<Result<RentalDetailedStatus>> getRentalDetailedStatus(@NotNull @PathVariable(RENTAL_ID_VARIABLE) UUID rentalId);

    @PostMapping(path = CREATE_RENTAL_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ApiResponse<CreateRentalResultDto> createRental(@Valid @NotNull @RequestBody CreateRentalCommand command);

    @PutMapping(path = FINISH_RENTAL_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ApiResponse<FinishRentalResultDto> finishRental(@Valid @NotNull @RequestBody FinishRentalCommand command);

    @GetMapping(ESTIMATE_RENTAL_COST_PATH)
    ApiResponse<RentalCostEstimationDto> estimateRentalCost(
            @NotNull @RequestParam(ENDING_UNTIL_VARIABLE) Instant endingUntil,
            @NotNull @RequestParam(MOVIE_ID_VARIABLE) Set<UUID> movieIds
    );

}