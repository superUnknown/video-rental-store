package pl.budka.videorentalstore.api.rentals.movies.model;

public enum MovieStatus {
    AVAILABLE,
    RENTED
}