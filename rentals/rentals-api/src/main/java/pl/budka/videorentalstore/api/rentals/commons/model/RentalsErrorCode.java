package pl.budka.videorentalstore.api.rentals.commons.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@RequiredArgsConstructor
public enum RentalsErrorCode {
    ACCOUNT_ALREADY_RENTING(HttpStatus.CONFLICT),
    MOVIE_NOT_FOUND(HttpStatus.NOT_FOUND),
    MOVIE_NOT_AVAILABLE(HttpStatus.NOT_FOUND),
    RENTAL_NOT_FOUND(HttpStatus.NOT_FOUND),
    RENTAL_DETAILED_STATUS_NOT_RESOLVED(HttpStatus.CONFLICT),
    RENTAL_ILLEGAL_STATUS__REQUESTED(HttpStatus.METHOD_NOT_ALLOWED),
    RENTAL_ILLEGAL_STATUS__ACTIVE(HttpStatus.METHOD_NOT_ALLOWED),
    RENTAL_ILLEGAL_STATUS__REJECTED(HttpStatus.METHOD_NOT_ALLOWED),
    RENTAL_ILLEGAL_STATUS__COMPLETED(HttpStatus.METHOD_NOT_ALLOWED),
    RENTAL_ILLEGAL_STATUS(HttpStatus.METHOD_NOT_ALLOWED),
    BONUS_POINTS_ALREADY_GRANTED(HttpStatus.CONFLICT);

    private final HttpStatus status;

}