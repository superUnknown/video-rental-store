package pl.budka.videorentalstore.api.rentals.rentals.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonSubTypes({
        @JsonSubTypes.Type(value = FinishRentalResultDto.AdditionalPaymentRequired.class, name = "ADDITIONAL_PAYMENT_REQUIRED"),
        @JsonSubTypes.Type(value = FinishRentalResultDto.Succeed.class, name = "SUCCEED")
})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "status")
public abstract class FinishRentalResultDto {

    @Valid
    @NotNull
    private RentalDto rental;

    public static FinishRentalResultDto.AdditionalPaymentRequired additionalPaymentRequired(RentalDto rental, String paymentLink) {
        return FinishRentalResultDto.AdditionalPaymentRequired.builder().rental(rental).paymentLink(paymentLink).build();
    }

    public static FinishRentalResultDto.Succeed succeed(RentalDto rental) {
        return FinishRentalResultDto.Succeed.builder().rental(rental).build();
    }

    public abstract Status getStatus();

    public enum Status {
        ADDITIONAL_PAYMENT_REQUIRED,
        SUCCEED
    }

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static class AdditionalPaymentRequired extends FinishRentalResultDto {

        private final Status status = Status.ADDITIONAL_PAYMENT_REQUIRED;

        @NotBlank
        private String paymentLink;

        @Builder
        public AdditionalPaymentRequired(@JsonProperty("rental") RentalDto rental, @JsonProperty("paymentLink") String paymentLink) {
            super(rental);
            this.paymentLink = paymentLink;
        }

    }

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static class Succeed extends FinishRentalResultDto {

        private final Status status = Status.SUCCEED;

        @Builder
        public Succeed(@JsonProperty("rental") RentalDto rental) {
            super(rental);
        }

    }

}