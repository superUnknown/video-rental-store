package pl.budka.videorentalstore.api.rentals.movies.model;

public enum MovieType {
    NEW_RELEASE,
    REGULAR,
    OLD
}