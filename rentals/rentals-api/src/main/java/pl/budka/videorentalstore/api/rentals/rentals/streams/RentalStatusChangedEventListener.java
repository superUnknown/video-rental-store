package pl.budka.videorentalstore.api.rentals.rentals.streams;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageChannel;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatusChangedEvent;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface RentalStatusChangedEventListener {

    @StreamListener(RentalStatusChangedEvent.QUEUE)
    void onEvent(@Valid @NotNull RentalStatusChangedEvent event);

    interface Channel {

        @Input(RentalStatusChangedEvent.QUEUE)
        MessageChannel input();

    }

}