package pl.budka.videorentalstore.api.rentals.rentals.model;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RentalCostEstimationDto {

    @NotNull
    @DecimalMin("0.01")
    private BigDecimal totalPrice;

    @NotNull
    private CurrencyCode currency;

    @Valid
    @NotEmpty
    private List<Movie> movies;

    public static RentalCostEstimationDto of(List<Movie> movies, CurrencyCode currency) {
        return RentalCostEstimationDto.builder()
                .totalPrice(movies.stream().map(Movie::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add))
                .currency(currency)
                .movies(movies)
                .build();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Movie {

        @NotNull
        private UUID id;

        @NotBlank
        private String name;

        @NotNull
        @DecimalMin("0.01")
        private BigDecimal price;

    }

}