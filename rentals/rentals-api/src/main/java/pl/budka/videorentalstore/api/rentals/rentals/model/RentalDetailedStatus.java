package pl.budka.videorentalstore.api.rentals.rentals.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatus;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@RequiredArgsConstructor
public enum RentalDetailedStatus {
    REQUESTED(RentalStatus.REQUESTED, ChargeStatus.PENDING),
    ACTIVE__PAID(RentalStatus.ACTIVE, ChargeStatus.COMPLETED),
    ACTIVE__PENDING_CORRECTION_PAYMENT(RentalStatus.ACTIVE, ChargeStatus.PENDING),
    ACTIVE__FAILED_CORRECTION_PAYMENT(RentalStatus.ACTIVE, ChargeStatus.FAILED),
    REJECTED(RentalStatus.REJECTED, ChargeStatus.FAILED),
    COMPLETED(RentalStatus.COMPLETED, ChargeStatus.COMPLETED);

    private static final Map<Pair<RentalStatus, ChargeStatus>, RentalDetailedStatus> MAPPINGS = Stream.of(RentalDetailedStatus.values())
            .collect(Collectors.toMap(it -> Pair.of(it.getRentalStatus(), it.getChargeStatus()), Function.identity()));

    private final RentalStatus rentalStatus;

    private final ChargeStatus chargeStatus;

    public static Optional<RentalDetailedStatus> of(RentalStatus rentalStatus, ChargeStatus chargeStatus) {
        return Optional.ofNullable(MAPPINGS.get(Pair.of(rentalStatus, chargeStatus)));
    }

}