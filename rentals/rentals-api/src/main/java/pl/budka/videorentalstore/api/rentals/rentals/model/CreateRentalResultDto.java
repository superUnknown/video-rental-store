package pl.budka.videorentalstore.api.rentals.rentals.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateRentalResultDto {

    @NotBlank
    private String paymentLink;

    @Valid
    @NotNull
    private RentalDto rental;

}