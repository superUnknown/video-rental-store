package pl.budka.videorentalstore.api.rentals.rentals.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.budka.videorentalstore.core.utils.Time;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RentalStatusChangedEvent {

    public static final String QUEUE = "rental-status-changed";

    @NotNull
    private final Instant time = Time.now();

    @NotNull
    private UUID rentalId;

    @NotNull
    private RentalStatus targetStatus;

}