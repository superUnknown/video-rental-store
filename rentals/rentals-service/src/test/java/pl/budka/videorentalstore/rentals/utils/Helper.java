package pl.budka.videorentalstore.rentals.utils;

import lombok.AllArgsConstructor;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.budka.videorentalstore.rentals.rentals.model.Rental;
import pl.budka.videorentalstore.rentals.rentals.repository.RentalsRepository;

import java.util.Optional;
import java.util.UUID;

@Component
@AllArgsConstructor
public class Helper {

    private final RentalsRepository rentalsRepository;

    @Transactional(readOnly = true)
    public Optional<Rental> getRental(UUID rentalId) {
        Optional<Rental> rental = rentalsRepository.findOne(rentalId);
        rental.ifPresent(it -> Hibernate.initialize(it.getMovies()));
        return rental;
    }


}