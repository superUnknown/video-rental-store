package pl.budka.videorentalstore.rentals

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.cloud.stream.test.binder.MessageCollector
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.MessageBuilder
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import pl.budka.videorentalstore.api.payments.charges.http.ChargesApi
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatusChangedEvent
import pl.budka.videorentalstore.api.payments.charges.streams.ChargeStatusChangedEventListener
import pl.budka.videorentalstore.api.rentals.movies.model.MovieStatus
import pl.budka.videorentalstore.api.rentals.movies.model.MovieType
import pl.budka.videorentalstore.api.rentals.rentals.http.RentalsApi
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalCommand
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalResultDto
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalCommand
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalResultDto
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatusChangedEvent
import pl.budka.videorentalstore.api.rentals.rentals.streams.RentalStatusChangedEventListener
import pl.budka.videorentalstore.api.users.accounts.http.AccountsApi
import pl.budka.videorentalstore.api.users.accounts.model.AccountDto
import pl.budka.videorentalstore.core.model.ApiResponse
import pl.budka.videorentalstore.rentals.bonuspoints.repository.BonusPointsRepository
import pl.budka.videorentalstore.rentals.movies.model.Movie
import pl.budka.videorentalstore.rentals.movies.repository.MoviesRepository
import pl.budka.videorentalstore.rentals.rentals.repository.RentalsRepository
import pl.budka.videorentalstore.rentals.rentals.streams.RentalStatusChangedEventPublisher
import pl.budka.videorentalstore.rentals.utils.Helper
import spock.lang.Specification
import spock.mock.DetachedMockFactory

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE
import static org.springframework.messaging.MessageHeaders.CONTENT_TYPE
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put

@AutoConfigureMockMvc
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
abstract class AbstractRentalsApplicationSpecIT extends Specification {

    @Autowired
    protected RentalStatusChangedEventPublisher.Channel rentalStatusChangedEventOutputChannel

    @Autowired
    protected RentalStatusChangedEventListener.Channel rentalStatusChangedEventInputChannel

    @Autowired
    protected ChargeStatusChangedEventListener.Channel chargeStatusChangedEventInputChannel

    @Autowired
    protected BonusPointsRepository bonusPointsRepository

    @Autowired
    protected RentalsRepository rentalsRepository

    @Autowired
    protected MoviesRepository moviesRepository

    @Autowired
    protected MessageCollector messageCollector

    @Autowired
    protected AccountsApi accountsApi

    @Autowired
    protected ChargesApi chargesApi

    @Autowired
    protected ObjectMapper mapper

    @Autowired
    protected MockMvc mockMvc

    @Autowired
    protected Helper helper

    def setup() {
        clean()
    }

    def cleanup() {
        clean()
    }

    void clean() {
        bonusPointsRepository.deleteAll()
        rentalsRepository.deleteAll()
        moviesRepository.deleteAll()
        cleanQueues()
    }

    void cleanQueues() {
        messageCollector.forChannel(rentalStatusChangedEventOutputChannel.output()).clear()
    }

    @TestConfiguration
    static class Configuration {

        private DetachedMockFactory factory = new DetachedMockFactory()

        @Bean
        @Primary
        AccountsApi accountsApi() {
            return factory.Mock(AccountsApi)
        }

        @Bean
        @Primary
        ChargesApi chargesApi() {
            return factory.Mock(ChargesApi)
        }

    }

    protected ApiResponse<CreateRentalResultDto> createRental(CreateRentalCommand command) {
        MockHttpServletResponse response = mockMvc.perform(post("${RentalsApi.CREATE_RENTAL_PATH}")
                .content(mapper.writeValueAsString(command))
                .contentType(APPLICATION_JSON_UTF8_VALUE)
        ).andReturn().getResponse()
        return mapper.readValue(response.getContentAsString(), new TypeReference<ApiResponse<CreateRentalResultDto>>() {})
    }

    protected ApiResponse<FinishRentalResultDto> finishRental(FinishRentalCommand command) {
        MockHttpServletResponse response = mockMvc.perform(put("${RentalsApi.FINISH_RENTAL_PATH}")
                .content(mapper.writeValueAsString(command))
                .contentType(APPLICATION_JSON_UTF8_VALUE)
        ).andReturn().getResponse()
        return mapper.readValue(response.getContentAsString(), new TypeReference<ApiResponse<FinishRentalResultDto>>() {})
    }

    protected Movie movie(String name, MovieType type, MovieStatus status = MovieStatus.AVAILABLE) {
        return moviesRepository.save(Movie.builder()
                .id(UUID.randomUUID())
                .status(status)
                .type(type)
                .name(name)
                .build()
        )
    }

    protected static AccountDto assembleAccount(UUID id = UUID.randomUUID(), String email = "dev@null.com") {
        return AccountDto.builder()
                .firstname("John")
                .surname("Smith")
                .email(email)
                .id(id)
                .build()
    }

    protected void sendChargeStatusChangedEvent(ChargeStatusChangedEvent event) {
        chargeStatusChangedEventInputChannel.input().send(assembleMessage(event))
    }

    protected void sendRentalStatusChangedEvent(RentalStatusChangedEvent event) {
        rentalStatusChangedEventInputChannel.input().send(assembleMessage(event))
    }

    private Message<String> assembleMessage(Object event) {
        return MessageBuilder.withPayload(mapper.writeValueAsString(event))
                .setHeader(CONTENT_TYPE, APPLICATION_JSON_UTF8_VALUE)
                .build()
    }

    protected <T> List<T> collectEvents(MessageChannel channel, Class<T> clazz) {
        List<T> events = []
        Message<String> event
        while ((event = messageCollector.forChannel(channel).poll() as Message<String>) != null) {
            events.add(mapper.readValue(event.payload, clazz))
        }
        return events
    }

}