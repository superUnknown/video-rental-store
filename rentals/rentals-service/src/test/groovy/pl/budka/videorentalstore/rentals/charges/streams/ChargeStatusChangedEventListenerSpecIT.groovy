package pl.budka.videorentalstore.rentals.charges.streams


import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatus
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatusChangedEvent
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentResultDto
import pl.budka.videorentalstore.api.rentals.movies.model.MovieStatus
import pl.budka.videorentalstore.api.rentals.movies.model.MovieType
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalCommand
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalDto
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatus
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatusChangedEvent
import pl.budka.videorentalstore.api.users.accounts.model.AccountDto
import pl.budka.videorentalstore.core.model.ApiResponse
import pl.budka.videorentalstore.core.utils.Time
import pl.budka.videorentalstore.rentals.AbstractRentalsApplicationSpecIT

import java.time.Duration

class ChargeStatusChangedEventListenerSpecIT extends AbstractRentalsApplicationSpecIT {

    def "should activate newly requested rental when received status changed event of associated charge that successfully completed"() {
        given:
            RentalDto rental = createRental()
        expect:
            rentalsRepository.findOne(rental.id).get().status == RentalStatus.REQUESTED
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }
        when:
            sendChargeStatusChangedEvent(ChargeStatusChangedEvent.builder()
                    .targetStatus(ChargeStatus.COMPLETED)
                    .chargeId(rental.chargeId)
                    .build()
            )
        then:
            0 * _
            noExceptionThrown()

            rentalsRepository.findOne(rental.id).get().status == RentalStatus.ACTIVE
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }

            List<RentalStatusChangedEvent> events = collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent)
            events.size() == 1
            with(events[0]) {
                getTime()
                getRentalId() == rental.id
                getTargetStatus() == RentalStatus.ACTIVE
            }

    }

    def "should reject newly requested rental and release rented movies when received status changed event of associated charge that failed"() {
        given:
            RentalDto rental = createRental()
        expect:
            rentalsRepository.findOne(rental.id).get().status == RentalStatus.REQUESTED
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }
        when:
            sendChargeStatusChangedEvent(ChargeStatusChangedEvent.builder()
                    .targetStatus(ChargeStatus.FAILED)
                    .chargeId(rental.chargeId)
                    .build()
            )
        then:
            0 * _
            noExceptionThrown()

            rentalsRepository.findOne(rental.id).get().status == RentalStatus.REJECTED
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.AVAILABLE
            }

            List<RentalStatusChangedEvent> events = collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent)
            events.size() == 1
            with(events[0]) {
                getTime()
                getRentalId() == rental.id
                getTargetStatus() == RentalStatus.REJECTED
            }

    }

    def "should finish active rental when received status changed event of associated charge that successfully completed and that rental is not expired"() {
//        TODO
    }

    def "should finish active rental when received status changed event of associated charge that successfully completed even if that rental is already expired"() {
//        TODO
    }

    def "should do nothing when received status changed event of charge that has status unimportant for newly requested rental"() {
        given:
            RentalDto rental = createRental()
        expect:
            rentalsRepository.findOne(rental.id).get().status == RentalStatus.REQUESTED
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }
        when:
            sendChargeStatusChangedEvent(ChargeStatusChangedEvent.builder()
                    .targetStatus(chargeStatus)
                    .chargeId(rental.chargeId)
                    .build()
            )
        then:
            0 * _
            noExceptionThrown()

            rentalsRepository.findOne(rental.id).get().status == RentalStatus.REQUESTED
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()

        where:
            chargeStatus << ChargeStatus.values().findAll { ![ChargeStatus.COMPLETED, ChargeStatus.FAILED].contains(it) }
    }

    def "should do nothing when received status changed event of charge that is not associated with any rental"() {
        expect:
            rentalsRepository.findAll().isEmpty()
        when:
            sendChargeStatusChangedEvent(ChargeStatusChangedEvent.builder()
                    .targetStatus(ChargeStatus.COMPLETED)
                    .chargeId(UUID.randomUUID())
                    .build()
            )
        then:
            0 * _
            noExceptionThrown()

            rentalsRepository.findAll().isEmpty()

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()

    }

    private RentalDto createRental() {
        AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
        1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
        1 * chargesApi.requestChargePayment(_ as RequestChargePaymentCommand) >> ApiResponse.ok(RequestChargePaymentResultDto.builder()
                .charge(ChargeDto.builder().id(UUID.randomUUID()).build())
                .paymentLink("http://example.com/path")
                .build()
        )
        return createRental(CreateRentalCommand.builder()
                .movieIds([movie("Black Panther", MovieType.NEW_RELEASE), movie("The Dark Knight", MovieType.REGULAR)].collect { it.id }.toSet())
                .endingUntil(Time.now() + Duration.ofDays(7))
                .accountId(account.id)
                .build()
        ).result.rental
    }

}