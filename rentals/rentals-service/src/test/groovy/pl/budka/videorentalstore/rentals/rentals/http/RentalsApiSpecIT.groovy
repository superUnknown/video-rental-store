package pl.budka.videorentalstore.rentals.rentals.http

import com.neovisionaries.i18n.CurrencyCode
import org.springframework.http.HttpStatus
import org.springframework.test.context.TestPropertySource
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatus
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatusChangedEvent
import pl.budka.videorentalstore.api.payments.charges.model.ModifyChargeCommand
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentResultDto
import pl.budka.videorentalstore.api.rentals.commons.model.RentalsErrorCode
import pl.budka.videorentalstore.api.rentals.movies.model.MovieStatus
import pl.budka.videorentalstore.api.rentals.movies.model.MovieType
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalCommand
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalResultDto
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalCommand
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalResultDto
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalDto
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatus
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatusChangedEvent
import pl.budka.videorentalstore.api.users.accounts.model.AccountDto
import pl.budka.videorentalstore.api.users.commons.model.UsersErrorCode
import pl.budka.videorentalstore.core.model.ApiResponse
import pl.budka.videorentalstore.core.utils.Time
import pl.budka.videorentalstore.rentals.AbstractRentalsApplicationSpecIT
import pl.budka.videorentalstore.rentals.rentals.model.Rental

import java.time.Duration
import java.time.Instant

import static pl.budka.videorentalstore.api.payments.charges.model.ChargeDto.Customer
import static pl.budka.videorentalstore.api.payments.charges.model.ChargeDto.PricePosition

@TestPropertySource(properties = [
        "video-rental-store.rentals.movie-prices.premium-price=40.00",
        "video-rental-store.rentals.movie-prices.basic-price=30.00",
        "video-rental-store.rentals.bonus-points.premium-points-quantity=2",
        "video-rental-store.rentals.bonus-points.basic-points-quantity=1"
])
class RentalsApiSpecIT extends AbstractRentalsApplicationSpecIT {

    private static final String NOW = "2019-06-12T15:30:00.000Z"

    def setup() {
        Time.setClock(Instant.parse(NOW))
    }

    def "should create rental for many movies for one day and return payment link when account exists, all movies are available and charge payment was successfully requested"() {
        given:
            UUID chargeId = UUID.randomUUID()
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            def movies = [
                    1: movie("Black Panther", MovieType.NEW_RELEASE),
                    2: movie("The Dark Knight", MovieType.REGULAR),
                    3: movie("The Shawshank Redemption", MovieType.OLD)
            ]
        expect:
            rentalsRepository.findAll().isEmpty()
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE
        when:
            ApiResponse<CreateRentalResultDto> response = createRental(CreateRentalCommand.builder()
                    .movieIds(movies.values().collect { it.id }.toSet())
                    .endingUntil(Time.now() + Duration.ofHours(3))
                    .accountId(account.id)
                    .build())
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            1 * chargesApi.requestChargePayment(_ as RequestChargePaymentCommand) >> { RequestChargePaymentCommand cmd ->
                assert cmd.customer == Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build()
                assert cmd.currency == CurrencyCode.SEK
                assert cmd.pricePositions.size() == 3
                assert cmd.pricePositions.find { it.name == "The Dark Knight" } == PricePosition.builder()
                        .name("The Dark Knight")
                        .unitPrice(30.00)
                        .quantity(1)
                        .build()
                assert cmd.pricePositions.find { it.name == "Black Panther" } == PricePosition.builder()
                        .name("Black Panther")
                        .unitPrice(40.00)
                        .quantity(1)
                        .build()
                assert cmd.pricePositions.find { it.name == "The Shawshank Redemption" } == PricePosition.builder()
                        .name("The Shawshank Redemption")
                        .unitPrice(30.00)
                        .quantity(1)
                        .build()

                return ApiResponse.ok(RequestChargePaymentResultDto.builder()
                        .charge(ChargeDto.builder().id(chargeId).build())
                        .paymentLink("http://example.com/path")
                        .build()
                )
            }
            0 * _

            response.isOk()
            with(response.result) {
                getPaymentLink() == "http://example.com/path"
                getRental().id
                getRental().accountId == account.id
                getRental().chargeId == chargeId
                getRental().status == RentalStatus.REQUESTED
                getRental().started == Instant.parse(NOW)
                getRental().endingUntil == Instant.parse("2019-06-12T23:59:59.999999999Z")
                !getRental().completed
                getRental().movieIds.sort() == movies.values().collect { it.id }.sort()
            }

            rentalsRepository.findAll().size() == 1
            with(helper.getRental(response.result.rental.id).get() as Rental) {
                getAccountId() == response.result.rental.accountId
                getChargeId() == response.result.rental.chargeId
                getStatus() == response.result.rental.status
                getStarted() == response.result.rental.started
                getEndingUntil() == response.result.rental.endingUntil
                getCompleted() == response.result.rental.completed
                getPremiumPriceApplied() == 40.00
                getBasicPriceApplied() == 30.00
                getPremiumBonusPointsApplied() == 2
                getBasicBonusPointsApplied() == 1
                getMovies().collect { it.movie.id }.sort() == response.result.rental.movieIds.sort()
                getMovies().collect { it.movieType }.sort() == movies.values().collect { it.type }.sort()

            }

            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.RENTED
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.RENTED
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.RENTED

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should create rental for many movies for several days and return payment link when account exists, all movies are available and charge payment was successfully requested"() {
        given:
            UUID chargeId = UUID.randomUUID()
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            def movies = [
                    1: movie("Black Panther", MovieType.NEW_RELEASE),
                    2: movie("The Dark Knight", MovieType.REGULAR),
                    3: movie("The Shawshank Redemption", MovieType.OLD)
            ]
        expect:
            rentalsRepository.findAll().isEmpty()
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE
        when:
            ApiResponse<CreateRentalResultDto> response = createRental(CreateRentalCommand.builder()
                    .movieIds(movies.values().collect { it.id }.toSet())
                    .endingUntil(Time.now() + Duration.ofDays(7))
                    .accountId(account.id)
                    .build())
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            1 * chargesApi.requestChargePayment(_ as RequestChargePaymentCommand) >> { RequestChargePaymentCommand cmd ->
                assert cmd.customer == Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build()
                assert cmd.currency == CurrencyCode.SEK
                assert cmd.pricePositions.size() == 3
                assert cmd.pricePositions.find { it.name == "The Dark Knight" } == PricePosition.builder()
                        .name("The Dark Knight")
                        .unitPrice(150.00)
                        .quantity(1)
                        .build()
                assert cmd.pricePositions.find { it.name == "Black Panther" } == PricePosition.builder()
                        .name("Black Panther")
                        .unitPrice(280.00)
                        .quantity(1)
                        .build()
                assert cmd.pricePositions.find { it.name == "The Shawshank Redemption" } == PricePosition.builder()
                        .name("The Shawshank Redemption")
                        .unitPrice(90.00)
                        .quantity(1)
                        .build()

                return ApiResponse.ok(RequestChargePaymentResultDto.builder()
                        .charge(ChargeDto.builder().id(chargeId).build())
                        .paymentLink("http://example.com/path")
                        .build()
                )
            }
            0 * _

            response.isOk()
            with(response.result) {
                getPaymentLink() == "http://example.com/path"
                getRental().id
                getRental().accountId == account.id
                getRental().chargeId == chargeId
                getRental().status == RentalStatus.REQUESTED
                getRental().started == Instant.parse(NOW)
                getRental().endingUntil == Instant.parse("2019-06-19T23:59:59.999999999Z")
                !getRental().completed
                getRental().movieIds.sort() == movies.values().collect { it.id }.sort()
            }

            rentalsRepository.findAll().size() == 1
            with(helper.getRental(response.result.rental.id).get() as Rental) {
                getAccountId() == response.result.rental.accountId
                getChargeId() == response.result.rental.chargeId
                getStatus() == response.result.rental.status
                getStarted() == response.result.rental.started
                getEndingUntil() == response.result.rental.endingUntil
                getCompleted() == response.result.rental.completed
                getPremiumPriceApplied() == 40.00
                getBasicPriceApplied() == 30.00
                getPremiumBonusPointsApplied() == 2
                getBasicBonusPointsApplied() == 1
                getMovies().collect { it.movie.id }.sort() == response.result.rental.movieIds.sort()
                getMovies().collect { it.movieType }.sort() == movies.values().collect { it.type }.sort()

            }

            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.RENTED
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.RENTED
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.RENTED

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should create rental for one movie for several days and return payment link when account exists, all movies are available and charge payment was successfully requested"() {
        given:
            UUID chargeId = UUID.randomUUID()
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            def movies = [
                    1: movie("Black Panther", MovieType.NEW_RELEASE),
                    2: movie("The Dark Knight", MovieType.REGULAR),
                    3: movie("The Shawshank Redemption", MovieType.OLD)
            ]
        expect:
            rentalsRepository.findAll().isEmpty()
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE
        when:
            ApiResponse<CreateRentalResultDto> response = createRental(CreateRentalCommand.builder()
                    .movieIds([movies[2].id].toSet())
                    .endingUntil(Time.now() + Duration.ofDays(3))
                    .accountId(account.id)
                    .build())
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            1 * chargesApi.requestChargePayment(_ as RequestChargePaymentCommand) >> { RequestChargePaymentCommand cmd ->
                assert cmd.customer == Customer.builder().email("dev@null.com").firstname("John").surname("Smith").build()
                assert cmd.currency == CurrencyCode.SEK
                assert cmd.pricePositions.size() == 1
                assert cmd.pricePositions.find { it.name == "The Dark Knight" } == PricePosition.builder()
                        .name("The Dark Knight")
                        .unitPrice(30.00)
                        .quantity(1)
                        .build()

                return ApiResponse.ok(RequestChargePaymentResultDto.builder()
                        .charge(ChargeDto.builder().id(chargeId).build())
                        .paymentLink("http://example.com/path")
                        .build()
                )
            }
            0 * _

            response.isOk()
            with(response.result) {
                getPaymentLink() == "http://example.com/path"
                getRental().id
                getRental().accountId == account.id
                getRental().chargeId == chargeId
                getRental().status == RentalStatus.REQUESTED
                getRental().started == Instant.parse(NOW)
                getRental().endingUntil == Instant.parse("2019-06-15T23:59:59.999999999Z")
                !getRental().completed
                getRental().movieIds == [movies[2].id].toSet()
            }

            rentalsRepository.findAll().size() == 1
            with(helper.getRental(response.result.rental.id).get() as Rental) {
                getAccountId() == response.result.rental.accountId
                getChargeId() == response.result.rental.chargeId
                getStatus() == response.result.rental.status
                getStarted() == response.result.rental.started
                getEndingUntil() == response.result.rental.endingUntil
                getCompleted() == response.result.rental.completed
                getPremiumPriceApplied() == 40.00
                getBasicPriceApplied() == 30.00
                getPremiumBonusPointsApplied() == 2
                getBasicBonusPointsApplied() == 1
                getMovies().collect { it.movie.id }.sort() == response.result.rental.movieIds.sort()
                getMovies().collect { it.movieType } == [movies[2].type]
            }

            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.RENTED
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should create rental for movies and return payment link when account has other REJECTED rental"() {
//        TODO
    }

    def "should create rental for movies and return payment link when account has other COMPLETED rental"() {
//        TODO
    }

    def "should not create rental for movies when charge payment request failed due to unexpected error"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            def movies = [
                    1: movie("Black Panther", MovieType.NEW_RELEASE),
                    2: movie("The Dark Knight", MovieType.REGULAR),
                    3: movie("The Shawshank Redemption", MovieType.OLD)
            ]
        expect:
            rentalsRepository.findAll().isEmpty()
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE
        when:
            ApiResponse<CreateRentalResultDto> response = createRental(CreateRentalCommand.builder()
                    .movieIds([movies[2].id, movies[3].id].toSet())
                    .endingUntil(Time.now() + Duration.ofDays(3))
                    .accountId(account.id)
                    .build())
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            1 * chargesApi.requestChargePayment(_ as RequestChargePaymentCommand) >> {
                throw new RuntimeException("test timeout exception")
            }
            0 * _

            !response.isOk()
            response.error.code == HttpStatus.INTERNAL_SERVER_ERROR.name()
            !response.result

            rentalsRepository.findAll().isEmpty()
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should not create rental for movies when charge payment request failed due to business error"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            def movies = [
                    1: movie("Black Panther", MovieType.NEW_RELEASE),
                    2: movie("The Dark Knight", MovieType.REGULAR),
                    3: movie("The Shawshank Redemption", MovieType.OLD)
            ]
        expect:
            rentalsRepository.findAll().isEmpty()
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE
        when:
            ApiResponse<CreateRentalResultDto> response = createRental(CreateRentalCommand.builder()
                    .movieIds([movies[2].id, movies[3].id].toSet())
                    .endingUntil(Time.now() + Duration.ofDays(3))
                    .accountId(account.id)
                    .build())
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            1 * chargesApi.requestChargePayment(_ as RequestChargePaymentCommand) >> ApiResponse.error(400, ApiResponse.Error.builder()
                    .code(HttpStatus.BAD_REQUEST.name())
                    .message("test constraint exception")
                    .build()
            )
            0 * _

            !response.isOk()
            response.error.code == HttpStatus.BAD_REQUEST.name()
            !response.result

            rentalsRepository.findAll().isEmpty()
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should not create rental for movies when one of some selected movies are unavailable"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            def movies = [
                    1: movie("Black Panther", MovieType.NEW_RELEASE),
                    2: movie("The Dark Knight", MovieType.REGULAR),
                    3: movie("The Shawshank Redemption", MovieType.OLD)
            ]

            1 * accountsApi.getAccount(_ as UUID) >> { UUID accountId -> ApiResponse.ok(assembleAccount(accountId, "otherUser@null.com")) }
            1 * chargesApi.requestChargePayment(_ as RequestChargePaymentCommand) >> ApiResponse.ok(RequestChargePaymentResultDto.builder()
                    .charge(ChargeDto.builder().id(UUID.randomUUID()).build())
                    .paymentLink("http://example.com/path")
                    .build()
            )
            createRental(CreateRentalCommand.builder()
                    .movieIds([movies[2].id, movies[3].id].toSet())
                    .endingUntil(Time.now() + Duration.ofDays(3))
                    .accountId(UUID.randomUUID())
                    .build()
            )
        expect:
            rentalsRepository.findAll().size() == 1
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.RENTED
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.RENTED
        when:
            ApiResponse<CreateRentalResultDto> response = createRental(CreateRentalCommand.builder()
                    .movieIds([movies[1].id, movies[2].id].toSet())
                    .endingUntil(Time.now() + Duration.ofDays(3))
                    .accountId(account.id)
                    .build()
            )
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            0 * _

            !response.isOk()
            response.error.code == RentalsErrorCode.MOVIE_NOT_AVAILABLE.name()
            !response.result

            rentalsRepository.findAll().size() == 1
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.RENTED
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.RENTED

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should not create rental for movies when one of some selected movies are doesn't exist"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            def movies = [
                    1: movie("Black Panther", MovieType.NEW_RELEASE),
                    2: movie("The Dark Knight", MovieType.REGULAR),
                    3: movie("The Shawshank Redemption", MovieType.OLD)
            ]
        expect:
            rentalsRepository.findAll().isEmpty()
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE
        when:
            ApiResponse<CreateRentalResultDto> response = createRental(CreateRentalCommand.builder()
                    .movieIds([movies[1].id, movies[2].id, UUID.randomUUID()].toSet())
                    .endingUntil(Time.now() + Duration.ofDays(3))
                    .accountId(account.id)
                    .build()
            )
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            0 * _

            !response.isOk()
            response.error.code == RentalsErrorCode.MOVIE_NOT_FOUND.name()
            !response.result

            rentalsRepository.findAll().isEmpty()
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should not create rental for movies when given account has already REQUESTED rental"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            def movies = [
                    1: movie("Black Panther", MovieType.NEW_RELEASE),
                    2: movie("The Dark Knight", MovieType.REGULAR),
                    3: movie("The Shawshank Redemption", MovieType.OLD)
            ]

            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            1 * chargesApi.requestChargePayment(_ as RequestChargePaymentCommand) >> ApiResponse.ok(RequestChargePaymentResultDto.builder()
                    .charge(ChargeDto.builder().id(UUID.randomUUID()).build())
                    .paymentLink("http://example.com/path")
                    .build()
            )
            createRental(CreateRentalCommand.builder()
                    .movieIds([movies[2].id, movies[3].id].toSet())
                    .endingUntil(Time.now() + Duration.ofDays(3))
                    .accountId(account.id)
                    .build()
            )

        expect:
            rentalsRepository.findAll().size() == 1
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.RENTED
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.RENTED
        when:
            ApiResponse<CreateRentalResultDto> response = createRental(CreateRentalCommand.builder()
                    .movieIds([movies[1].id, movies[2].id].toSet())
                    .endingUntil(Time.now() + Duration.ofDays(3))
                    .accountId(account.id)
                    .build()
            )
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            0 * _

            !response.isOk()
            response.error.code == RentalsErrorCode.ACCOUNT_ALREADY_RENTING.name()
            !response.result

            rentalsRepository.findAll().size() == 1
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.RENTED
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.RENTED

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should not create rental for movies when given account has already ACTIVE rental"() {
//        TODO
    }

    def "should not create rental for movies when account doesn't exist"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            def movies = [
                    1: movie("Black Panther", MovieType.NEW_RELEASE),
                    2: movie("The Dark Knight", MovieType.REGULAR),
                    3: movie("The Shawshank Redemption", MovieType.OLD)
            ]
        expect:
            rentalsRepository.findAll().isEmpty()
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE
        when:
            ApiResponse<CreateRentalResultDto> response = createRental(CreateRentalCommand.builder()
                    .movieIds([movies[1].id, movies[2].id].toSet())
                    .endingUntil(Time.now() + Duration.ofDays(3))
                    .accountId(account.id)
                    .build()
            )
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.error(404, ApiResponse.Error.builder()
                    .code(UsersErrorCode.ACCOUNT_NOT_FOUND.name())
                    .message("test account exception")
                    .build()
            )
            0 * _

            !response.isOk()
            response.error.code == UsersErrorCode.ACCOUNT_NOT_FOUND.name()
            !response.result

            rentalsRepository.findAll().isEmpty()
            moviesRepository.findOne(movies[1].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[2].id).get().status == MovieStatus.AVAILABLE
            moviesRepository.findOne(movies[3].id).get().status == MovieStatus.AVAILABLE

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should not create rental for movies when request constraints validation failed"() {
//        TODO
    }

    def "should finish active rental when is not expired"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            RentalDto rental = createRental(account, Time.now() + Duration.ofDays(7))
            sendChargeStatusChangedEvent(ChargeStatusChangedEvent.builder()
                    .targetStatus(ChargeStatus.COMPLETED)
                    .chargeId(rental.chargeId)
                    .build()
            )
            cleanQueues()
        expect:
            with(rentalsRepository.findOne(rental.id).get() as Rental) {
                assert getStatus() == RentalStatus.ACTIVE
                assert getEndingUntil().isAfter(Time.now())
            }
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }
        when:
            ApiResponse<FinishRentalResultDto> response = finishRental(FinishRentalCommand.builder()
                    .accountId(account.id)
                    .build()
            )
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            0 * _

            response.isOk()
            with(response.result as FinishRentalResultDto.Succeed) {
                getStatus() == FinishRentalResultDto.Status.SUCCEED
                getRental().id
                getRental().accountId == rental.accountId
                getRental().chargeId == rental.chargeId
                getRental().status == RentalStatus.COMPLETED
                getRental().started == rental.started
                getRental().endingUntil == rental.endingUntil
                getRental().completed == Time.now()
            }

            rentalsRepository.findAll().size() == 1
            with(helper.getRental(response.result.rental.id).get() as Rental) {
                getAccountId() == response.result.rental.accountId
                getChargeId() == response.result.rental.chargeId
                getStatus() == response.result.rental.status
                getStarted() == response.result.rental.started
                getEndingUntil() == response.result.rental.endingUntil
                getCompleted() == response.result.rental.completed
                getPremiumPriceApplied() == 40.00
                getBasicPriceApplied() == 30.00
                getPremiumBonusPointsApplied() == 2
                getBasicBonusPointsApplied() == 1
            }
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.AVAILABLE
            }

            List<RentalStatusChangedEvent> events = collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent)
            events.size() == 1
            with(events[0]) {
                getTime()
                getRentalId() == rental.id
                getTargetStatus() == RentalStatus.COMPLETED
            }

    }

    def "should not finish active rental when has just expired"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            RentalDto rental = createRental(account, Time.now() + Duration.ofDays(7))
            sendChargeStatusChangedEvent(ChargeStatusChangedEvent.builder()
                    .targetStatus(ChargeStatus.COMPLETED)
                    .chargeId(rental.chargeId)
                    .build()
            )
            cleanQueues()
            Time.setClock(rental.endingUntil + Duration.ofNanos(1))
        expect:
            with(rentalsRepository.findOne(rental.id).get() as Rental) {
                assert getStatus() == RentalStatus.ACTIVE
                assert getEndingUntil().isBefore(Time.now())
            }
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }
        when:
            ApiResponse<FinishRentalResultDto> response = finishRental(FinishRentalCommand.builder()
                    .accountId(account.id)
                    .build()
            )
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            1 * chargesApi.modifyCharge(_ as UUID, _ as ModifyChargeCommand) >> { UUID chargeId, ModifyChargeCommand cmd ->
                assert chargeId == rental.chargeId
                assert cmd.pricePositions.size() == 3
                assert cmd.pricePositions.find { it.name == "The Dark Knight" } == PricePosition.builder()
                        .name("The Dark Knight")
                        .unitPrice(180.00)
                        .quantity(1)
                        .build()
                assert cmd.pricePositions.find { it.name == "Black Panther" } == PricePosition.builder()
                        .name("Black Panther")
                        .unitPrice(320.00)
                        .quantity(1)
                        .build()
                assert cmd.pricePositions.find { it.name == "The Shawshank Redemption" } == PricePosition.builder()
                        .name("The Shawshank Redemption")
                        .unitPrice(120.00)
                        .quantity(1)
                        .build()
                return ApiResponse.ok(RequestChargePaymentResultDto.builder()
                        .charge(ChargeDto.builder().id(chargeId).build())
                        .paymentLink("http://example.com/path")
                        .build()
                )
            }
            0 * _

            response.isOk()
            with(response.result as FinishRentalResultDto.AdditionalPaymentRequired) {
                getStatus() == FinishRentalResultDto.Status.ADDITIONAL_PAYMENT_REQUIRED
                getPaymentLink() == "http://example.com/path"
                getRental().id
                getRental().accountId == rental.accountId
                getRental().chargeId == rental.chargeId
                getRental().status == RentalStatus.ACTIVE
                getRental().started == rental.started
                getRental().endingUntil == rental.endingUntil
                !getRental().completed
            }

            rentalsRepository.findAll().size() == 1
            with(helper.getRental(response.result.rental.id).get() as Rental) {
                getAccountId() == response.result.rental.accountId
                getChargeId() == response.result.rental.chargeId
                getStatus() == response.result.rental.status
                getStarted() == response.result.rental.started
                getEndingUntil() == response.result.rental.endingUntil
                getCompleted() == response.result.rental.completed
                getPremiumPriceApplied() == 40.00
                getBasicPriceApplied() == 30.00
                getPremiumBonusPointsApplied() == 2
                getBasicBonusPointsApplied() == 1
            }
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()

    }

    def "should not finish active rental when is expired since few days"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            RentalDto rental = createRental(account, Time.now() + Duration.ofDays(7))
            sendChargeStatusChangedEvent(ChargeStatusChangedEvent.builder()
                    .targetStatus(ChargeStatus.COMPLETED)
                    .chargeId(rental.chargeId)
                    .build()
            )
            cleanQueues()
            Time.setClock(Time.now() + Duration.ofDays(11))
        expect:
            with(rentalsRepository.findOne(rental.id).get() as Rental) {
                assert getStatus() == RentalStatus.ACTIVE
                assert getEndingUntil().isBefore(Time.now())
            }
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }
        when:
            ApiResponse<FinishRentalResultDto> response = finishRental(FinishRentalCommand.builder()
                    .accountId(account.id)
                    .build()
            )
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            1 * chargesApi.modifyCharge(_ as UUID, _ as ModifyChargeCommand) >> { UUID chargeId, ModifyChargeCommand cmd ->
                assert chargeId == rental.chargeId
                assert cmd.pricePositions.size() == 3
                assert cmd.pricePositions.find { it.name == "The Dark Knight" } == PricePosition.builder()
                        .name("The Dark Knight")
                        .unitPrice(270.00)
                        .quantity(1)
                        .build()
                assert cmd.pricePositions.find { it.name == "Black Panther" } == PricePosition.builder()
                        .name("Black Panther")
                        .unitPrice(440.00)
                        .quantity(1)
                        .build()
                assert cmd.pricePositions.find { it.name == "The Shawshank Redemption" } == PricePosition.builder()
                        .name("The Shawshank Redemption")
                        .unitPrice(210.00)
                        .quantity(1)
                        .build()
                return ApiResponse.ok(RequestChargePaymentResultDto.builder()
                        .charge(ChargeDto.builder().id(chargeId).build())
                        .paymentLink("http://example.com/path")
                        .build()
                )
            }
            0 * _

            response.isOk()
            with(response.result as FinishRentalResultDto.AdditionalPaymentRequired) {
                getStatus() == FinishRentalResultDto.Status.ADDITIONAL_PAYMENT_REQUIRED
                getPaymentLink() == "http://example.com/path"
                getRental().id
                getRental().accountId == rental.accountId
                getRental().chargeId == rental.chargeId
                getRental().status == RentalStatus.ACTIVE
                getRental().started == rental.started
                getRental().endingUntil == rental.endingUntil
                !getRental().completed
            }

            rentalsRepository.findAll().size() == 1
            with(helper.getRental(response.result.rental.id).get() as Rental) {
                getAccountId() == response.result.rental.accountId
                getChargeId() == response.result.rental.chargeId
                getStatus() == response.result.rental.status
                getStarted() == response.result.rental.started
                getEndingUntil() == response.result.rental.endingUntil
                getCompleted() == response.result.rental.completed
                getPremiumPriceApplied() == 40.00
                getBasicPriceApplied() == 30.00
                getPremiumBonusPointsApplied() == 2
                getBasicBonusPointsApplied() == 1
            }
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()

    }

    def "should not finish rental when is REQUESTED"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            RentalDto rental = createRental(account, Time.now() + Duration.ofDays(7))
        expect:
            rentalsRepository.findOne(rental.id).get().status == RentalStatus.REQUESTED
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }
        when:
            ApiResponse<FinishRentalResultDto> response = finishRental(FinishRentalCommand.builder()
                    .accountId(account.id)
                    .build()
            )
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            0 * _

            !response.isOk()
            response.error.code == RentalsErrorCode.RENTAL_NOT_FOUND.name()
            !response.result

            rentalsRepository.findAll().size() == 1
            rentalsRepository.findOne(rental.id).get().status == RentalStatus.REQUESTED
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.RENTED
            }

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should not finish rental when is REJECTED"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            RentalDto rental = createRental(account, Time.now() + Duration.ofDays(7))
            sendChargeStatusChangedEvent(ChargeStatusChangedEvent.builder()
                    .targetStatus(ChargeStatus.FAILED)
                    .chargeId(rental.chargeId)
                    .build()
            )
            cleanQueues()
        expect:
            rentalsRepository.findOne(rental.id).get().status == RentalStatus.REJECTED
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.AVAILABLE
            }
        when:
            ApiResponse<FinishRentalResultDto> response = finishRental(FinishRentalCommand.builder()
                    .accountId(account.id)
                    .build()
            )
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            0 * _

            !response.isOk()
            response.error.code == RentalsErrorCode.RENTAL_NOT_FOUND.name()
            !response.result

            rentalsRepository.findAll().size() == 1
            rentalsRepository.findOne(rental.id).get().status == RentalStatus.REJECTED
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.AVAILABLE
            }

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should not finish rental when is COMPLETED"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            RentalDto rental = createRental(account, Time.now() + Duration.ofDays(7))
            sendChargeStatusChangedEvent(ChargeStatusChangedEvent.builder()
                    .targetStatus(ChargeStatus.COMPLETED)
                    .chargeId(rental.chargeId)
                    .build()
            )
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            finishRental(FinishRentalCommand.builder()
                    .accountId(account.id)
                    .build()
            )
            cleanQueues()
        expect:
            rentalsRepository.findOne(rental.id).get().status == RentalStatus.COMPLETED
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.AVAILABLE
            }
        when:
            ApiResponse<FinishRentalResultDto> response = finishRental(FinishRentalCommand.builder()
                    .accountId(account.id)
                    .build()
            )
        then:
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            0 * _

            !response.isOk()
            response.error.code == RentalsErrorCode.RENTAL_NOT_FOUND.name()
            !response.result

            rentalsRepository.findAll().size() == 1
            rentalsRepository.findOne(rental.id).get().status == RentalStatus.COMPLETED
            moviesRepository.findAll().each {
                assert it.status == MovieStatus.AVAILABLE
            }

            collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent).isEmpty()
    }

    def "should estimate rental cost when account exists and all given movies are available"() {
//        TODO
    }

    def "should not estimate rental cost when some of given movies are unavailable"() {
//        TODO
    }

    def "should not estimate rental cost when some of given movies doesn't exist"() {
//        TODO
    }

    def "should not estimate rental cost when given account doesn't exist"() {
//        TODO
    }

    private RentalDto createRental(AccountDto account, Instant endingUntil) {
        1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
        1 * chargesApi.requestChargePayment(_ as RequestChargePaymentCommand) >> ApiResponse.ok(RequestChargePaymentResultDto.builder()
                .charge(ChargeDto.builder().id(UUID.randomUUID()).build())
                .paymentLink("http://example.com/path")
                .build()
        )
        return createRental(CreateRentalCommand.builder()
                .movieIds([movie("Black Panther", MovieType.NEW_RELEASE), movie("The Dark Knight", MovieType.REGULAR), movie("The Shawshank Redemption", MovieType.OLD)].collect {
            it.id
        }.toSet())
                .endingUntil(endingUntil)
                .accountId(account.id)
                .build()
        ).result.rental
    }

}