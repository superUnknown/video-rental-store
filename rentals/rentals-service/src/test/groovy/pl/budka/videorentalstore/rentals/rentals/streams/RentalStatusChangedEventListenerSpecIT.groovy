package pl.budka.videorentalstore.rentals.rentals.streams

import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatus
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatusChangedEvent
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentResultDto
import pl.budka.videorentalstore.api.rentals.movies.model.MovieType
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalCommand
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalCommand
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalDto
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatus
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatusChangedEvent
import pl.budka.videorentalstore.api.users.accounts.model.AccountDto
import pl.budka.videorentalstore.core.model.ApiResponse
import pl.budka.videorentalstore.core.utils.Time
import pl.budka.videorentalstore.rentals.AbstractRentalsApplicationSpecIT

import java.time.Duration

class RentalStatusChangedEventListenerSpecIT extends AbstractRentalsApplicationSpecIT {

    def "should grant bonus points for rental when received status changed event of rental that successfully completed"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            def movies = [
                    1: movie("Black Panther", MovieType.NEW_RELEASE),
                    2: movie("The Dark Knight", MovieType.REGULAR),
                    3: movie("The Shawshank Redemption", MovieType.OLD)
            ]
            RentalDto rental = createRental(account, movies.values().collect { it.id }.toSet())
            sendChargeStatusChangedEvent(ChargeStatusChangedEvent.builder()
                    .targetStatus(ChargeStatus.COMPLETED)
                    .chargeId(rental.chargeId)
                    .build()
            )
            cleanQueues()
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            finishRental(FinishRentalCommand.builder()
                    .accountId(account.id)
                    .build()
            )
            RentalStatusChangedEvent event = collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent)[0]
        expect:
            rentalsRepository.findOne(rental.id).get().status == RentalStatus.COMPLETED
            bonusPointsRepository.findAll().isEmpty()
        when:
            sendRentalStatusChangedEvent(event)
        then:
            0 * _
            noExceptionThrown()

            rentalsRepository.findOne(rental.id).get().status == RentalStatus.COMPLETED

            bonusPointsRepository.findAll().each { assert it.rental.id == rental.id }
            bonusPointsRepository.findAll().collectEntries { [(it.movie.id): it.value] }.sort() == [
                    (movies[1].id): 2,
                    (movies[2].id): 1,
                    (movies[3].id): 1
            ]
    }

    def "should not grant bonus points for rental when received status changed event of rental that successfully completed but bonus points are already granted"() {
        given:
            AccountDto account = assembleAccount(UUID.randomUUID(), "dev@null.com")
            def movies = [
                    1: movie("Black Panther", MovieType.NEW_RELEASE),
                    2: movie("The Dark Knight", MovieType.REGULAR),
                    3: movie("The Shawshank Redemption", MovieType.OLD)
            ]
            RentalDto rental = createRental(account, movies.values().collect { it.id }.toSet())
            sendChargeStatusChangedEvent(ChargeStatusChangedEvent.builder()
                    .targetStatus(ChargeStatus.COMPLETED)
                    .chargeId(rental.chargeId)
                    .build()
            )
            cleanQueues()
            1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
            finishRental(FinishRentalCommand.builder()
                    .accountId(account.id)
                    .build()
            )
            RentalStatusChangedEvent event = collectEvents(rentalStatusChangedEventOutputChannel.output(), RentalStatusChangedEvent)[0]
            sendRentalStatusChangedEvent(event)
        expect:
            rentalsRepository.findOne(rental.id).get().status == RentalStatus.COMPLETED
            bonusPointsRepository.findAll().collectEntries { [(it.movie.id): it.value] }.sort() == [
                    (movies[1].id): 2,
                    (movies[2].id): 1,
                    (movies[3].id): 1
            ]
        when:
            sendRentalStatusChangedEvent(event)
        then:
            0 * _
            noExceptionThrown()

            rentalsRepository.findOne(rental.id).get().status == RentalStatus.COMPLETED

            bonusPointsRepository.findAll().each { assert it.rental.id == rental.id }
            bonusPointsRepository.findAll().collectEntries { [(it.movie.id): it.value] }.sort() == [
                    (movies[1].id): 2,
                    (movies[2].id): 1,
                    (movies[3].id): 1
            ]
    }

    def "should ignore rental status changed event for other status than COMPLETED"() {
        when:
            sendRentalStatusChangedEvent(RentalStatusChangedEvent.builder()
                    .rentalId(UUID.randomUUID())
                    .targetStatus(rentalStatus)
                    .build()
            )
        then:
            0 * _
            noExceptionThrown()

            rentalsRepository.findAll().isEmpty()
            bonusPointsRepository.findAll().isEmpty()
        where:
            rentalStatus << RentalStatus.values().findAll { it != RentalStatus.COMPLETED }
    }

    private RentalDto createRental(AccountDto account, Collection<UUID> movieIds) {
        1 * accountsApi.getAccount(account.id) >> ApiResponse.ok(account)
        1 * chargesApi.requestChargePayment(_ as RequestChargePaymentCommand) >> ApiResponse.ok(RequestChargePaymentResultDto.builder()
                .charge(ChargeDto.builder().id(UUID.randomUUID()).build())
                .paymentLink("http://example.com/path")
                .build()
        )
        return createRental(CreateRentalCommand.builder()
                .endingUntil(Time.now() + Duration.ofDays(7))
                .movieIds(movieIds.toSet())
                .accountId(account.id)
                .build()
        ).result.rental
    }

}