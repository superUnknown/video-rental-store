package pl.budka.videorentalstore.rentals.movies.exception;

import pl.budka.videorentalstore.api.rentals.commons.model.RentalsErrorCode;
import pl.budka.videorentalstore.rentals.commons.exception.RentalsApplicationException;

import java.util.UUID;

public class MovieNotAvailableException extends RentalsApplicationException {

    private MovieNotAvailableException(String message) {
        super(message, RentalsErrorCode.MOVIE_NOT_AVAILABLE);
    }

    public static MovieNotAvailableException forId(UUID movieId) {
        return new MovieNotAvailableException(String.format("Movie with id '%s' is not available.", movieId));
    }

    public static MovieNotAvailableException oneOrMore() {
        return new MovieNotAvailableException("One or more of given movies are not available.");
    }

}