package pl.budka.videorentalstore.rentals.rentals.streams;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatusChangedEvent;

public interface RentalStatusChangedEventPublisher {

    void publish(RentalStatusChangedEvent event);

    interface Channel {

        @Output(RentalStatusChangedEvent.QUEUE + "-output")
        MessageChannel output();

    }

}
