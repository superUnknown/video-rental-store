package pl.budka.videorentalstore.rentals.rentals.service;

import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalCommand;
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalResultDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalCommand;
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalResultDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalCostEstimationDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalDetailedStatus;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalDto;
import pl.budka.videorentalstore.rentals.movies.exception.AccountAlreadyRentingException;
import pl.budka.videorentalstore.rentals.movies.exception.MovieNotAvailableException;
import pl.budka.videorentalstore.rentals.movies.exception.MovieNotFoundException;
import pl.budka.videorentalstore.rentals.rentals.dto.EstimateRentalCostCommand;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalDetailedStatusNotResolvedException;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalIllegalStatusException;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalNotFoundException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface RentalsService {

    RentalDetailedStatus getRentalDetailedStatus(@NotNull UUID rentalId) throws RentalNotFoundException, RentalDetailedStatusNotResolvedException;

    RentalDto getRentalByCharge(@NotNull UUID chargeId) throws RentalNotFoundException;

    CreateRentalResultDto createRental(@Valid @NotNull CreateRentalCommand command) throws MovieNotFoundException, MovieNotAvailableException, AccountAlreadyRentingException;

    void activate(@NotNull UUID rentalId) throws RentalNotFoundException, RentalIllegalStatusException;

    void reject(@NotNull UUID rentalId) throws RentalNotFoundException, RentalIllegalStatusException;

    FinishRentalResultDto finish(@Valid @NotNull FinishRentalCommand command, boolean force) throws RentalNotFoundException;

    RentalCostEstimationDto estimateRentalCost(@Valid @NotNull EstimateRentalCostCommand command) throws MovieNotFoundException, MovieNotAvailableException;

}