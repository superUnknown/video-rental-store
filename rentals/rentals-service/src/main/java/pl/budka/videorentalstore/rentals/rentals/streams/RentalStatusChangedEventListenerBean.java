package pl.budka.videorentalstore.rentals.rentals.streams;

import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatus;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatusChangedEvent;
import pl.budka.videorentalstore.api.rentals.rentals.streams.RentalStatusChangedEventListener;
import pl.budka.videorentalstore.rentals.bonuspoints.exception.BonusPointsAlreadyGrantedException;
import pl.budka.videorentalstore.rentals.bonuspoints.service.BonusPointsService;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalIllegalStatusException;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalNotFoundException;

import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@Validated
@EnableBinding(RentalStatusChangedEventListener.Channel.class)
public class RentalStatusChangedEventListenerBean implements RentalStatusChangedEventListener {

    private final Map<RentalStatus, Delegate> factory;

    public RentalStatusChangedEventListenerBean(BonusPointsService bonusPointsService) {
        this.factory = ImmutableMap.of(
                RentalStatus.COMPLETED, new Delegate.Completed(bonusPointsService)
        );
    }

    @Override
    public void onEvent(RentalStatusChangedEvent event) {
        log.info("onRentalStatusChangedEvent [event: {}]", event);
        Optional<Delegate> handler = Optional.ofNullable(factory.get(event.getTargetStatus()));
        if (!handler.isPresent()) {
            log.trace("Ignoring RentalStatusChangedEvent; there is no handler defined.");
            return;
        }
        handler.get().handle(event.getRentalId());
    }

    private interface Delegate {

        void handle(@NotNull UUID rentalId);

        @RequiredArgsConstructor
        class Completed implements Delegate {

            private final BonusPointsService bonusPointsService;

            @Override
            public void handle(UUID rentalId) {
                try {
                    bonusPointsService.grantBonusPointsForRental(rentalId);
                } catch (BonusPointsAlreadyGrantedException e) {
                    log.warn("Failed to grant bonus points for rental ({}).", e.getMessage());
                } catch (RentalNotFoundException | RentalIllegalStatusException e) {
                    throw new RuntimeException("Unexpected error occurred when granting bonus points for rental.", e);
                }
            }
        }

    }

}