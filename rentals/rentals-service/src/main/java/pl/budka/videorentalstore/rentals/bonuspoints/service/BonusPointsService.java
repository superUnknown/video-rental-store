package pl.budka.videorentalstore.rentals.bonuspoints.service;

import pl.budka.videorentalstore.rentals.bonuspoints.exception.BonusPointsAlreadyGrantedException;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalIllegalStatusException;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalNotFoundException;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface BonusPointsService {

    void grantBonusPointsForRental(@NotNull UUID rentalId) throws RentalNotFoundException, RentalIllegalStatusException, BonusPointsAlreadyGrantedException;

}