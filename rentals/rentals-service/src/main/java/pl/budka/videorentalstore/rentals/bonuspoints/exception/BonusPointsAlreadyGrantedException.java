package pl.budka.videorentalstore.rentals.bonuspoints.exception;

import pl.budka.videorentalstore.api.rentals.commons.model.RentalsErrorCode;
import pl.budka.videorentalstore.rentals.commons.exception.RentalsApplicationException;

import java.util.UUID;

public class BonusPointsAlreadyGrantedException extends RentalsApplicationException {

    private BonusPointsAlreadyGrantedException(String message) {
        super(message, RentalsErrorCode.BONUS_POINTS_ALREADY_GRANTED);
    }

    public static BonusPointsAlreadyGrantedException forRental(UUID rentalId) {
        return new BonusPointsAlreadyGrantedException(String.format("Bonus points for rental with id '%s' are already granted.", rentalId));
    }

}