package pl.budka.videorentalstore.rentals.movies.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;
import pl.budka.videorentalstore.api.rentals.movies.model.MovieStatus;
import pl.budka.videorentalstore.api.rentals.movies.model.MovieType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Movie {

    @Id
    private UUID id;

    @NotBlank
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    private MovieType type;

    @NotNull
    @Enumerated(EnumType.STRING)
    private MovieStatus status;

    @Transient
    public boolean isAvailable() {
        return this.status == MovieStatus.AVAILABLE;
    }

    @Transient
    public void markAsRented() {
        this.status = MovieStatus.RENTED;
    }

    @Transient
    public void markAsAvailable() {
        this.status = MovieStatus.AVAILABLE;
    }

}