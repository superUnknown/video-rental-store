package pl.budka.videorentalstore.rentals.commons.exception;

import pl.budka.videorentalstore.api.rentals.commons.model.RentalsErrorCode;
import pl.budka.videorentalstore.core.exceptions.ApplicationException;

public abstract class RentalsApplicationException extends ApplicationException {

    protected RentalsApplicationException(String message, RentalsErrorCode code) {
        this(message, code, null);
    }

    protected RentalsApplicationException(String message, RentalsErrorCode code, Exception cause) {
        super(message, code.name(), code.getStatus().value(), cause);
    }

}