package pl.budka.videorentalstore.rentals.rentals.price.utils;

import com.google.common.collect.ImmutableMap;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import pl.budka.videorentalstore.api.rentals.movies.model.MovieType;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

@Slf4j
public class MoviePriceCalculator {

    private final Map<MovieType, Delegate> factory;

    @Builder
    public MoviePriceCalculator(BigDecimal premiumMoviePrice, BigDecimal basicMoviePrice, long standardDurationInDays, long extraDurationInDays) {
        this.factory = ImmutableMap.of(
                MovieType.NEW_RELEASE, new Delegate.NewReleases(premiumMoviePrice, basicMoviePrice, standardDurationInDays, extraDurationInDays),
                MovieType.REGULAR, new Delegate.RegularMovies(premiumMoviePrice, basicMoviePrice, standardDurationInDays, extraDurationInDays),
                MovieType.OLD, new Delegate.OldMovies(premiumMoviePrice, basicMoviePrice, standardDurationInDays, extraDurationInDays)
        );
    }

    public BigDecimal calculate(MovieType movieType) {
        return Optional.ofNullable(factory.get(movieType))
                .map(Delegate::calculate)
                .orElseThrow(() -> new RuntimeException(String.format("There is no price calculator for movie of type '%s'.", movieType)));
    }

    @Getter
    @ToString
    @RequiredArgsConstructor
    private static abstract class Delegate {

        private final BigDecimal premiumMoviePrice;

        private final BigDecimal basicMoviePrice;

        private final long standardDurationInDays;

        private final long extraDurationInDays;

        protected abstract BigDecimal doCalculate();

        private BigDecimal calculate() {
            log.trace("Calculating movie price [calculator: {}, metadata: {}].", this.getClass().getSimpleName(), this);
            BigDecimal result = doCalculate();
            log.trace("Calculated movie price [calculator: {}, metadata: {}]: {}.", this.getClass().getSimpleName(), this);
            return result;
        }

        BigDecimal calculate(BigDecimal unitPrice, long numberOfInitialDays) {
            BigDecimal standardPrice = unitPrice.add(unitPrice.multiply(BigDecimal.valueOf(getStandardDurationInDays() - Math.min(numberOfInitialDays, getStandardDurationInDays()))));
            BigDecimal extraDaysPrice = unitPrice.multiply(BigDecimal.valueOf(getExtraDurationInDays()));
            return standardPrice.add(extraDaysPrice);
        }

        static class NewReleases extends Delegate {

            NewReleases(BigDecimal premiumMoviePrice, BigDecimal basicMoviePrice, long standardDurationInDays, long extraDurationInDays) {
                super(premiumMoviePrice, basicMoviePrice, standardDurationInDays, extraDurationInDays);
            }

            @Override
            public BigDecimal doCalculate() {
                return getPremiumMoviePrice().multiply(BigDecimal.valueOf(getStandardDurationInDays() + getExtraDurationInDays()));
            }

        }

        static class RegularMovies extends Delegate {

            RegularMovies(BigDecimal premiumMoviePrice, BigDecimal basicMoviePrice, long standardDurationInDays, long extraDurationInDays) {
                super(premiumMoviePrice, basicMoviePrice, standardDurationInDays, extraDurationInDays);
            }

            @Override
            public BigDecimal doCalculate() {
                return calculate(getBasicMoviePrice(), 3);
            }

        }

        static class OldMovies extends Delegate {

            OldMovies(BigDecimal premiumMoviePrice, BigDecimal basicMoviePrice, long standardDurationInDays, long extraDurationInDays) {
                super(premiumMoviePrice, basicMoviePrice, standardDurationInDays, extraDurationInDays);
            }

            @Override
            public BigDecimal doCalculate() {
                return calculate(getBasicMoviePrice(), 5);
            }

        }

    }

}