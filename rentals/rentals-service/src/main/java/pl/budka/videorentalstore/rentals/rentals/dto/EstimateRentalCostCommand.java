package pl.budka.videorentalstore.rentals.rentals.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EstimateRentalCostCommand {

    @NotNull
    private Instant endingUntil;

    @NotNull
    private Set<UUID> movieIds;

}