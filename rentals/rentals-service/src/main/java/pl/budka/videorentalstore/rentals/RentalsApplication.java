package pl.budka.videorentalstore.rentals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import pl.budka.videorentalstore.core.exceptions.HttpExceptionHandler;
import pl.budka.videorentalstore.core.streams.StreamEventPublisherSupport;

@SpringBootApplication
@Import({HttpExceptionHandler.class, StreamEventPublisherSupport.class})
public class RentalsApplication {

    public static void main(String[] args) {
        SpringApplication.run(RentalsApplication.class, args);
    }

}