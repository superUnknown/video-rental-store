package pl.budka.videorentalstore.rentals.rentals.service;

import com.google.common.collect.ImmutableSet;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.api.payments.charges.http.ChargesApi;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto.Customer;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeDto.PricePosition;
import pl.budka.videorentalstore.api.payments.charges.model.ModifyChargeCommand;
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentCommand;
import pl.budka.videorentalstore.api.payments.charges.model.RequestChargePaymentResultDto;
import pl.budka.videorentalstore.api.rentals.movies.model.MovieType;
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalCommand;
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalResultDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalCommand;
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalResultDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalCostEstimationDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalDetailedStatus;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatus;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatusChangedEvent;
import pl.budka.videorentalstore.api.users.accounts.http.AccountsApi;
import pl.budka.videorentalstore.api.users.accounts.model.AccountDto;
import pl.budka.videorentalstore.core.utils.Time;
import pl.budka.videorentalstore.rentals.configuration.RentalsServiceConfiguration;
import pl.budka.videorentalstore.rentals.movies.exception.AccountAlreadyRentingException;
import pl.budka.videorentalstore.rentals.movies.exception.MovieNotAvailableException;
import pl.budka.videorentalstore.rentals.movies.exception.MovieNotFoundException;
import pl.budka.videorentalstore.rentals.movies.model.Movie;
import pl.budka.videorentalstore.rentals.movies.repository.MoviesRepository;
import pl.budka.videorentalstore.rentals.rentals.dto.EstimateRentalCostCommand;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalDetailedStatusNotResolvedException;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalIllegalStatusException;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalNotFoundException;
import pl.budka.videorentalstore.rentals.rentals.model.Rental;
import pl.budka.videorentalstore.rentals.rentals.price.utils.MoviePriceCalculator;
import pl.budka.videorentalstore.rentals.rentals.repository.RentalsRepository;
import pl.budka.videorentalstore.rentals.rentals.streams.RentalStatusChangedEventPublisher;
import pl.budka.videorentalstore.rentals.utils.Mapper;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static pl.budka.videorentalstore.core.utils.HttpApiHandler.call;

@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class RentalsServiceBean implements RentalsService {

    private final RentalStatusChangedEventPublisher rentalStatusChangedEventPublisher;

    private final RentalsServiceConfiguration.Properties properties;

    private final RentalsRepository rentalsRepository;

    private final MoviesRepository moviesRepository;

    private final AccountsApi accountsApi;

    private final ChargesApi chargesApi;

    @Override
    @Transactional(readOnly = true)
    public RentalDetailedStatus getRentalDetailedStatus(UUID rentalId) throws RentalNotFoundException, RentalDetailedStatusNotResolvedException {
        log.trace("Getting rental detailed status [rentalId: {}].", rentalId);
        Rental rental = rentalsRepository.findOne(rentalId).orElseThrow(() -> RentalNotFoundException.forId(rentalId));
        ChargeDto charge = call(() -> chargesApi.getCharge(rental.getChargeId()));
        RentalDetailedStatus result = RentalDetailedStatus.of(rental.getStatus(), charge.getStatus()).orElseThrow(() -> RentalDetailedStatusNotResolvedException
                .of(rental.getId(), rental.getStatus(), charge.getId(), charge.getStatus()) // should never be thrown
        );
        log.trace("Retrieved rental detailed status [rentalId: {}]: {}.", rentalId, result);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public RentalDto getRentalByCharge(UUID chargeId) throws RentalNotFoundException {
        log.trace("Getting rental by charge [chargeId: {}].", chargeId);
        RentalDto result = Mapper.map(rentalsRepository.findByChargeId(chargeId)
                .orElseThrow(() -> RentalNotFoundException.forCharge(chargeId)), RentalDto.class
        );
        log.trace("Retrieved rental by charge [chargeId: {}]: {}.", chargeId, result);
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public CreateRentalResultDto createRental(CreateRentalCommand command) throws MovieNotFoundException, MovieNotAvailableException, AccountAlreadyRentingException {
        log.debug("Creating rental [command: {}].", command);
        Assert.isTrue(command.getEndingUntil().isAfter(Time.now()), "Property endingUntil must be in the future.");

        AccountDto account = call(() -> accountsApi.getAccount(command.getAccountId()));
        if (isAccountAlreadyRenting(account.getId())) {
            throw AccountAlreadyRentingException.withId(account.getId());
        }

        Rental rental = Rental.builder()
                .premiumBonusPointsApplied(properties.getBonusPoints().getPremiumPointsQuantity())
                .basicBonusPointsApplied(properties.getBonusPoints().getBasicPointsQuantity())
                .premiumPriceApplied(properties.getMoviePrices().getPremiumPrice())
                .basicPriceApplied(properties.getMoviePrices().getBasicPrice())
                .endingUntil(Time.atEndOfDay(command.getEndingUntil()))
                .movies(findAvailableMovies(command.getMovieIds()))
                .accountId(account.getId())
                .build();

        RequestChargePaymentResultDto payment = call(() -> chargesApi.requestChargePayment(RequestChargePaymentCommand.builder()
                .currency(properties.getMoviePrices().getBaseCurrency())
                .pricePositions(assemblePricePositions(rental))
                .customer(Mapper.map(account, Customer.class))
                .build()
        ));
        rental.setChargeId(payment.getCharge().getId());

        rental.getMovies().stream().map(Rental.RentedMovie::getMovie).peek(Movie::markAsRented).forEach(moviesRepository::save);

        CreateRentalResultDto result = CreateRentalResultDto.builder()
                .rental(Mapper.map(rentalsRepository.save(rental), RentalDto.class))
                .paymentLink(payment.getPaymentLink())
                .build();
        log.debug("Created rental [command: {}]: {}.", command, result);
        return result;
    }

    private boolean isAccountAlreadyRenting(UUID accountId) {
        return !rentalsRepository.findAllByAccountIdAndStatusIn(accountId, ImmutableSet.of(RentalStatus.REQUESTED, RentalStatus.ACTIVE)).isEmpty();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void activate(UUID rentalId) throws RentalNotFoundException, RentalIllegalStatusException {
        log.debug("Activating rental [rentalId: {}].", rentalId);
        Rental rental = rentalsRepository.findOne(rentalId).orElseThrow(() -> RentalNotFoundException.forId(rentalId));
        if (!rental.isRequested()) {
            throw RentalIllegalStatusException.forId(rentalId, rental.getStatus());
        }
        changeStatus(rental, RentalStatus.ACTIVE);
        rentalsRepository.save(rental);
        log.debug("Activated rental [rentalId: {}].", rentalId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void reject(UUID rentalId) throws RentalNotFoundException, RentalIllegalStatusException {
        log.debug("Rejecting rental [rentalId: {}].", rentalId);
        Rental rental = rentalsRepository.findOne(rentalId).orElseThrow(() -> RentalNotFoundException.forId(rentalId));
        if (!rental.isRequested()) {
            throw RentalIllegalStatusException.forId(rentalId, rental.getStatus());
        }
        changeStatus(rental, RentalStatus.REJECTED);
        releaseRentedMovies(rental);
        rentalsRepository.save(rental);
        log.debug("Rejected rental [rentalId: {}].", rentalId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public FinishRentalResultDto finish(FinishRentalCommand command, boolean force) throws RentalNotFoundException {
        log.debug("Finishing rental [force: {}, command: {}].", force, command);
        AccountDto account = call(() -> accountsApi.getAccount(command.getAccountId()));
        Rental rental = rentalsRepository.findAllByAccountIdAndStatusIn(account.getId(), Collections.singleton(RentalStatus.ACTIVE)).stream().findAny()
                .orElseThrow(() -> RentalNotFoundException.activeForAccount(account.getId()));
        log.debug("Found active rental for account; trying to finish [rentalId: {}].", rental.getId());
        if (!force && rental.isExpired()) {
            log.debug("Rental is expired; requesting additional late return penalty payment.");
            FinishRentalResultDto result = requestAdditionalPayment(rental);
            log.debug("Failed to finish rental (additional late return penalty payment is required) [command: {}]: {}.", command, result);
            return result;
        }
        changeStatus(rental, RentalStatus.COMPLETED);
        rental.setCompleted(Time.now());
        releaseRentedMovies(rental);
        FinishRentalResultDto result = FinishRentalResultDto.succeed(Mapper.map(rentalsRepository.save(rental), RentalDto.class));
        log.debug("Finished rental [force: {}. command: {}]: {}.", force, command, result);
        return result;
    }

    private FinishRentalResultDto.AdditionalPaymentRequired requestAdditionalPayment(Rental rental) {
        RequestChargePaymentResultDto payment = call(() -> chargesApi.modifyCharge(rental.getChargeId(), ModifyChargeCommand.builder()
                .pricePositions(assemblePricePositions(rental))
                .build()
        ));
        return FinishRentalResultDto.additionalPaymentRequired(Mapper.map(rental, RentalDto.class), payment.getPaymentLink());
    }

    private static List<PricePosition> assemblePricePositions(Rental rental) {
        return new ArrayList<>(calculateMoviePrices(
                rental.getMovies(),
                rental.getStarted(),
                rental.getEndingUntil(),
                rental.getPremiumPriceApplied(),
                rental.getBasicPriceApplied(),
                it -> it.getMovie().getId(),
                Rental.RentedMovie::getMovieType,
                it -> it.getMovie().getName()
        ).values());
    }

    private void releaseRentedMovies(Rental rental) {
        rental.getMovies().stream().map(Rental.RentedMovie::getMovie).peek(Movie::markAsAvailable).forEach(moviesRepository::save);
    }

    private void changeStatus(Rental rental, RentalStatus targetStatus) {
        log.trace("Changing rental status [rentalId: {}, status: {}, targetStatus: {}].", rental.getId(), rental.getStatus(), targetStatus);
        rental.setStatus(targetStatus);
        rentalStatusChangedEventPublisher.publish(RentalStatusChangedEvent.builder()
                .targetStatus(rental.getStatus())
                .rentalId(rental.getId())
                .build()
        );
    }

    @Override
    @Transactional(readOnly = true)
    public RentalCostEstimationDto estimateRentalCost(EstimateRentalCostCommand command) throws MovieNotFoundException, MovieNotAvailableException {
        log.debug("Estimating rental cost [command: {}].", command);
        Assert.isTrue(command.getEndingUntil().isAfter(Time.now()), "Property endingUntil must be in the future.");
        List<RentalCostEstimationDto.Movie> movies = estimateRentalCost(findAvailableMovies(command.getMovieIds()), command.getEndingUntil());
        RentalCostEstimationDto result = RentalCostEstimationDto.of(movies, properties.getMoviePrices().getBaseCurrency());
        log.debug("Estimated rental cost [command: {}]: {}.", command, result);
        return result;
    }

    private List<RentalCostEstimationDto.Movie> estimateRentalCost(List<Movie> movies, Instant endingUntil) {
        Map<UUID, PricePosition> moviePrices = calculateMoviePrices(
                movies,
                Time.now(),
                endingUntil,
                properties.getMoviePrices().getPremiumPrice(),
                properties.getMoviePrices().getBasicPrice(),
                Movie::getId,
                Movie::getType,
                Movie::getName
        );
        return moviePrices.entrySet().stream().map(it -> RentalCostEstimationDto.Movie.builder()
                .price(it.getValue().getUnitPrice())
                .name(it.getValue().getName())
                .id(it.getKey())
                .build()
        ).collect(Collectors.toList());
    }

    private List<Movie> findAvailableMovies(Set<UUID> movieIds) throws MovieNotFoundException, MovieNotAvailableException {
        List<Movie> movies = moviesRepository.findByIdIn(movieIds);
        if (movies.size() != movieIds.size()) {
            throw MovieNotFoundException.oneOrMore();
        }
        if (!movies.stream().allMatch(Movie::isAvailable)) {
            throw MovieNotAvailableException.oneOrMore();
        }
        return movies;
    }

    private static <S> Map<UUID, PricePosition> calculateMoviePrices(Collection<S> movies, Instant rentalStart, Instant rentalFinish, BigDecimal premiumPrice, BigDecimal basicPrice, Function<S, UUID> movieIdExtractor, Function<S, MovieType> movieTypeExtractor, Function<S, String> movieNameExtractor) {
        MoviePriceCalculator calculator = MoviePriceCalculator.builder()
                .standardDurationInDays(Math.max(Duration.between(rentalStart.truncatedTo(ChronoUnit.DAYS), rentalFinish.truncatedTo(ChronoUnit.DAYS)).toDays(), 1))
                .extraDurationInDays(Math.min(Duration.between(Time.now().truncatedTo(ChronoUnit.DAYS), rentalFinish.truncatedTo(ChronoUnit.DAYS)).toDays(), 0) * -1)
                .premiumMoviePrice(premiumPrice)
                .basicMoviePrice(basicPrice)
                .build();
        return movies.stream().collect(Collectors.toMap(movieIdExtractor, it -> PricePosition.builder()
                .unitPrice(calculator.calculate(movieTypeExtractor.apply(it)))
                .name(movieNameExtractor.apply(it))
                .quantity(1)
                .build()
        ));
    }

}