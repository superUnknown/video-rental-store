package pl.budka.videorentalstore.rentals.bonuspoints.utils;

import com.google.common.collect.ImmutableMap;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import pl.budka.videorentalstore.api.rentals.movies.model.MovieType;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

@Slf4j
public class BonusPointsCalculator {

    private final Map<MovieType, Supplier<Integer>> factory;

    @Builder
    public BonusPointsCalculator(Integer premiumBonusPoints, Integer basicBonusPoints) {
        this.factory = ImmutableMap.of(
                MovieType.NEW_RELEASE, () -> premiumBonusPoints,
                MovieType.REGULAR, () -> basicBonusPoints,
                MovieType.OLD, () -> basicBonusPoints
        );
    }

    public Integer calculate(MovieType movieType) {
        return Optional.ofNullable(factory.get(movieType)).map(Supplier::get).orElseThrow(
                () -> new RuntimeException(String.format("There is no bonus points calculator for movie of type '%s'.", movieType))
        );
    }

}