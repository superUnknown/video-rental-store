package pl.budka.videorentalstore.rentals.movies.exception;

import pl.budka.videorentalstore.api.rentals.commons.model.RentalsErrorCode;
import pl.budka.videorentalstore.rentals.commons.exception.RentalsApplicationException;

import java.util.UUID;

public class AccountAlreadyRentingException extends RentalsApplicationException {

    private AccountAlreadyRentingException(String message) {
        super(message, RentalsErrorCode.ACCOUNT_ALREADY_RENTING);
    }

    public static AccountAlreadyRentingException withId(UUID accountId) {
        return new AccountAlreadyRentingException(String.format("Account with id '%s' is already renting.", accountId));
    }

}