package pl.budka.videorentalstore.rentals.rentals.http;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.budka.videorentalstore.api.rentals.rentals.http.RentalsApi;
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalCommand;
import pl.budka.videorentalstore.api.rentals.rentals.model.CreateRentalResultDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalCommand;
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalResultDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalCostEstimationDto;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalDetailedStatus;
import pl.budka.videorentalstore.core.model.ApiResponse;
import pl.budka.videorentalstore.core.model.Result;
import pl.budka.videorentalstore.rentals.rentals.dto.EstimateRentalCostCommand;
import pl.budka.videorentalstore.rentals.rentals.service.RentalsService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;

import static pl.budka.videorentalstore.core.utils.HttpApiHandler.execute;

@Slf4j
@RestController
@RequiredArgsConstructor
public class RentalsController implements RentalsApi {

    private final RentalsService rentalsService;

    @Override
    public ApiResponse<Result<RentalDetailedStatus>> getRentalDetailedStatus(@NotNull @PathVariable(RENTAL_ID_VARIABLE) UUID rentalId) {
        log.info("Getting rental detailed status [rentalId: {}].", rentalId);
        ApiResponse<Result<RentalDetailedStatus>> result = execute(() -> Result.of(rentalsService.getRentalDetailedStatus(rentalId)));
        log.info("Retrieved rental detailed status [rentalId: {}]: {}.", rentalId, result);
        return result;
    }

    @Override
    public ApiResponse<CreateRentalResultDto> createRental(@Valid @NotNull @RequestBody CreateRentalCommand command) {
        log.info("Creating rental [command: {}].", command);
        ApiResponse<CreateRentalResultDto> result = execute(() -> rentalsService.createRental(command));
        log.info("Created rental [command: {}]: {}.", command, result);
        return result;
    }

    @Override
    public ApiResponse<FinishRentalResultDto> finishRental(@Valid @NotNull @RequestBody FinishRentalCommand command) {
        log.info("Finishing rental [command: {}].", command);
        ApiResponse<FinishRentalResultDto> result = execute(() -> rentalsService.finish(command, false));
        log.info("Finished rental [command: {}]: {}.", command, result);
        return result;
    }

    @Override
    public ApiResponse<RentalCostEstimationDto> estimateRentalCost(@NotNull @RequestParam(ENDING_UNTIL_VARIABLE) Instant endingUntil, @NotNull @RequestParam(MOVIE_ID_VARIABLE) Set<UUID> movieIds) {
        log.info("Estimating rental cost [endingUntil: {}, movieIds: {}].", endingUntil, movieIds);
        ApiResponse<RentalCostEstimationDto> result = execute(() -> rentalsService.estimateRentalCost(EstimateRentalCostCommand.builder()
                .endingUntil(endingUntil)
                .movieIds(movieIds)
                .build()
        ));
        log.info("Estimated rental cost [endingUntil: {}, movieIds: {}]: {}.", endingUntil, movieIds, result);
        return result;
    }

}