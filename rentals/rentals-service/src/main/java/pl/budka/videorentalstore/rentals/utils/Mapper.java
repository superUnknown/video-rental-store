package pl.budka.videorentalstore.rentals.utils;

import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalDto;
import pl.budka.videorentalstore.rentals.rentals.model.Rental;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class Mapper {

    private static final ModelMapper INSTANCE;

    private static final PropertyMap<Rental, RentalDto> RENTAL_TO_DTO_MAPPING = new PropertyMap<Rental, RentalDto>() {
        @Override
        protected void configure() {
            using(new AbstractConverter<Collection<Rental.RentedMovie>, Set<UUID>>() {
                @Override
                protected Set<UUID> convert(Collection<Rental.RentedMovie> source) {
                    return source.stream().map(it -> it.getMovie().getId()).collect(Collectors.toSet());
                }
            }).map(source.getMovies(), destination.getMovieIds());
            map().setCompleted(source.getCompleted());
        }
    };

    static {
        INSTANCE = new ModelMapper();
        INSTANCE.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        INSTANCE.addMappings(RENTAL_TO_DTO_MAPPING);
    }

    public static <D> D map(Object src, Class<D> clazz) {
        return INSTANCE.map(src, clazz);
    }

    public static <D> D map(Object src, D dest) {
        INSTANCE.map(src, dest);
        return dest;
    }

}