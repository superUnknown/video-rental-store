package pl.budka.videorentalstore.rentals.configuration;

import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import pl.budka.videorentalstore.api.payments.charges.http.ChargesApi;
import pl.budka.videorentalstore.api.users.accounts.http.AccountsApi;
import pl.budka.videorentalstore.core.configuration.FeignClientsConfiguration;

@Configuration
@Profile("!test")
@Import({FeignClientsConfiguration.class})
@EnableFeignClients(clients = {AccountsApi.class, ChargesApi.class})
public class ExternalHttpApiConfiguration {

}