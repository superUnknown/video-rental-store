package pl.budka.videorentalstore.rentals.rentals.streams;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatusChangedEvent;
import pl.budka.videorentalstore.core.streams.StreamEventPublisherSupport;

@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@EnableBinding(RentalStatusChangedEventPublisher.Channel.class)
public class RentalStatusChangedEventPublisherBean implements RentalStatusChangedEventPublisher {

    private final StreamEventPublisherSupport publisher;

    private final Channel channel;

    @Override
    public void publish(RentalStatusChangedEvent event) {
        publisher.enqueue(channel.output(), event);
    }

}