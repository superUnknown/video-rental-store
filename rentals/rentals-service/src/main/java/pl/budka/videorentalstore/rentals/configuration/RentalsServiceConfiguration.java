package pl.budka.videorentalstore.rentals.configuration;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Configuration
@EnableConfigurationProperties(RentalsServiceConfiguration.Properties.class)
public class RentalsServiceConfiguration {

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ConfigurationProperties("video-rental-store.rentals")
    public static class Properties {

        @Valid
        @NotNull
        private MoviePrices moviePrices;

        @Valid
        @NotNull
        private BonusPoints bonusPoints;

        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        public static class MoviePrices {

            @NotNull
            private CurrencyCode baseCurrency;

            @NotNull
            @DecimalMin("0.01")
            private BigDecimal premiumPrice;

            @NotNull
            @DecimalMin("0.01")
            private BigDecimal basicPrice;

        }

        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        public static class BonusPoints {

            @Min(0)
            @NotNull
            private Integer premiumPointsQuantity;

            @Min(0)
            @NotNull
            private Integer basicPointsQuantity;

        }

    }

}