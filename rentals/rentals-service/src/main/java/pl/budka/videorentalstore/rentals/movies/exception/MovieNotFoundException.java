package pl.budka.videorentalstore.rentals.movies.exception;

import pl.budka.videorentalstore.api.rentals.commons.model.RentalsErrorCode;
import pl.budka.videorentalstore.rentals.commons.exception.RentalsApplicationException;

import java.util.UUID;

public class MovieNotFoundException extends RentalsApplicationException {

    private MovieNotFoundException(String message) {
        super(message, RentalsErrorCode.MOVIE_NOT_FOUND);
    }

    public static MovieNotFoundException forId(UUID movieId) {
        return new MovieNotFoundException(String.format("Movie with id '%s' doesn't exist.", movieId));
    }

    public static MovieNotFoundException oneOrMore() {
        return new MovieNotFoundException("One or more of given movies doesn't exist.");
    }

}