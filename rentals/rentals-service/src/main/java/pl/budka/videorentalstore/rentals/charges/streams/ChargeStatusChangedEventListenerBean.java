package pl.budka.videorentalstore.rentals.charges.streams;

import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatus;
import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatusChangedEvent;
import pl.budka.videorentalstore.api.payments.charges.streams.ChargeStatusChangedEventListener;
import pl.budka.videorentalstore.api.rentals.rentals.model.FinishRentalCommand;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalDto;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalIllegalStatusException;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalNotFoundException;
import pl.budka.videorentalstore.rentals.rentals.service.RentalsService;

import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
@Validated
@EnableBinding(ChargeStatusChangedEventListener.Channel.class)
public class ChargeStatusChangedEventListenerBean implements ChargeStatusChangedEventListener {

    private final Map<ChargeStatus, Delegate> factory;

    private final RentalsService rentalsService;

    public ChargeStatusChangedEventListenerBean(RentalsService rentalsService) {
        this.factory = ImmutableMap.of(
                ChargeStatus.COMPLETED, new Delegate.Completed(rentalsService),
                ChargeStatus.FAILED, new Delegate.Failed(rentalsService)
        );
        this.rentalsService = rentalsService;
    }

    @Override
    public void onEvent(ChargeStatusChangedEvent event) {
        log.info("onChargeStatusChangedEvent [event: {}]", event);
        Optional<Delegate> handler = Optional.ofNullable(factory.get(event.getTargetStatus()));
        if (!handler.isPresent()) {
            log.trace("Ignoring RentalStatusChangedEvent; there is no handler defined.");
            return;
        }
        try {
            handler.get().handle(rentalsService.getRentalByCharge(event.getChargeId()));
        } catch (RentalNotFoundException e) {
            log.warn("Ignoring RentalStatusChangedEvent ({}).", e.getMessage());
        }
    }

    private interface Delegate {

        void handle(RentalDto rental);

        @RequiredArgsConstructor
        class Completed implements Delegate {

            private final RentalsService rentalsService;

            @Override
            public void handle(RentalDto rental) {
                switch (rental.getStatus()) {
                    case REQUESTED:
                        try {
                            rentalsService.activate(rental.getId());
                        } catch (RentalNotFoundException | RentalIllegalStatusException e) {
                            throw new RuntimeException("Unexpected error occurred when activating rental.", e);
                        }
                        break;
                    case ACTIVE:
                        try {
                            rentalsService.finish(FinishRentalCommand.builder().accountId(rental.getAccountId()).build(), true);
                        } catch (RentalNotFoundException e) {
                            throw new RuntimeException("Unexpected error occurred when finishing rental.", e);
                        }
                    default:
                        log.trace("Associated rental with id '{}' is '{}'; no action needed.", rental.getId(), rental.getStatus());
                }
            }

        }

        @RequiredArgsConstructor
        class Failed implements Delegate {

            private final RentalsService rentalsService;

            @Override
            public void handle(RentalDto rental) {
                switch (rental.getStatus()) {
                    case REQUESTED:
                        try {
                            rentalsService.reject(rental.getId());
                        } catch (RentalNotFoundException | RentalIllegalStatusException e) {
                            throw new RuntimeException("Unexpected error occurred when rejecting rental.", e);
                        }
                        break;
                    default:
                        log.trace("Associated rental with id '{}' is '{}'; no action needed.", rental.getId(), rental.getStatus());
                }
            }

        }

    }

}