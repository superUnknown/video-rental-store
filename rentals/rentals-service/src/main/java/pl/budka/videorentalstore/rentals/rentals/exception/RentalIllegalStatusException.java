package pl.budka.videorentalstore.rentals.rentals.exception;

import com.google.common.collect.ImmutableMap;
import lombok.Builder;
import pl.budka.videorentalstore.api.rentals.commons.model.RentalsErrorCode;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatus;
import pl.budka.videorentalstore.rentals.commons.exception.RentalsApplicationException;

import java.util.Map;
import java.util.UUID;

public class RentalIllegalStatusException extends RentalsApplicationException {

    private static final Map<RentalStatus, RentalsErrorCode> MAPPINGS = ImmutableMap.of(
            RentalStatus.REQUESTED, RentalsErrorCode.RENTAL_ILLEGAL_STATUS__REQUESTED,
            RentalStatus.ACTIVE, RentalsErrorCode.RENTAL_ILLEGAL_STATUS__ACTIVE,
            RentalStatus.REJECTED, RentalsErrorCode.RENTAL_ILLEGAL_STATUS__REJECTED,
            RentalStatus.COMPLETED, RentalsErrorCode.RENTAL_ILLEGAL_STATUS__COMPLETED
    );

    @Builder
    private RentalIllegalStatusException(String message, RentalsErrorCode code) {
        super(message, code);
    }

    public static RentalIllegalStatusException forId(UUID rentalId, RentalStatus status) {
        return RentalIllegalStatusException.builder()
                .message(String.format("Rental with id '%s' has illegal status '%s' to process this operation.", rentalId, status))
                .code(MAPPINGS.getOrDefault(status, RentalsErrorCode.RENTAL_ILLEGAL_STATUS))
                .build();
    }

}