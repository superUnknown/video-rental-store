package pl.budka.videorentalstore.rentals.movies.repository;

import org.springframework.data.repository.Repository;
import pl.budka.videorentalstore.rentals.movies.model.Movie;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface MoviesRepository extends Repository<Movie, UUID> {

    Optional<Movie> findOne(UUID movieId);

    List<Movie> findByIdIn(Set<UUID> movieIds);

    Movie save(Movie movie);

    List<Movie> findAll();

    void deleteAll();

}