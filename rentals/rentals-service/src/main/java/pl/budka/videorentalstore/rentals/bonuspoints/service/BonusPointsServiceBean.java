package pl.budka.videorentalstore.rentals.bonuspoints.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import pl.budka.videorentalstore.rentals.bonuspoints.exception.BonusPointsAlreadyGrantedException;
import pl.budka.videorentalstore.rentals.bonuspoints.model.BonusPoints;
import pl.budka.videorentalstore.rentals.bonuspoints.repository.BonusPointsRepository;
import pl.budka.videorentalstore.rentals.bonuspoints.utils.BonusPointsCalculator;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalIllegalStatusException;
import pl.budka.videorentalstore.rentals.rentals.exception.RentalNotFoundException;
import pl.budka.videorentalstore.rentals.rentals.model.Rental;
import pl.budka.videorentalstore.rentals.rentals.repository.RentalsRepository;

import java.util.UUID;
import java.util.function.Function;

@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class BonusPointsServiceBean implements BonusPointsService {

    private final BonusPointsRepository bonusPointsRepository;

    private final RentalsRepository rentalsRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void grantBonusPointsForRental(UUID rentalId) throws RentalNotFoundException, RentalIllegalStatusException, BonusPointsAlreadyGrantedException {
        log.debug("Granting bonus points for rental [rentalId: {}].", rentalId);
        Rental rental = rentalsRepository.findOne(rentalId).orElseThrow(() -> RentalNotFoundException.forId(rentalId));
        if (!rental.isCompleted()) {
            throw RentalIllegalStatusException.forId(rental.getId(), rental.getStatus());
        }
        if (bonusPointsRepository.existsByRental(rental)) {
            throw BonusPointsAlreadyGrantedException.forRental(rental.getId());
        }
        BonusPointsCalculator calculator = BonusPointsCalculator.builder()
                .premiumBonusPoints(rental.getPremiumBonusPointsApplied())
                .basicBonusPoints(rental.getBasicBonusPointsApplied())
                .build();
        Integer grantedBonusPoints = rental.getMovies().stream()
                .map(calculate(calculator))
                .filter(it -> it.getValue() > 0)
                .map(bonusPointsRepository::save)
                .mapToInt(BonusPoints::getValue).sum();
        log.debug("Granted bonus points for rental [rentalId: {}]: {} in total.", rentalId, grantedBonusPoints);
    }

    private static Function<Rental.RentedMovie, BonusPoints> calculate(BonusPointsCalculator calculator) {
        return it -> BonusPoints.builder()
                .value(calculator.calculate(it.getMovieType()))
                .rental(it.getRental())
                .movie(it.getMovie())
                .build();
    }

}