package pl.budka.videorentalstore.rentals.rentals.exception;

import pl.budka.videorentalstore.api.rentals.commons.model.RentalsErrorCode;
import pl.budka.videorentalstore.rentals.commons.exception.RentalsApplicationException;

import java.util.UUID;

public class RentalNotFoundException extends RentalsApplicationException {

    private RentalNotFoundException(String message) {
        super(message, RentalsErrorCode.RENTAL_NOT_FOUND);
    }

    public static RentalNotFoundException forId(UUID rentalId) {
        return new RentalNotFoundException(String.format("Rental with id '%s' doesn't exist.", rentalId));
    }

    public static RentalNotFoundException forCharge(UUID chargeId) {
        return new RentalNotFoundException(String.format("Rental for charge with id '%s' doesn't exist.", chargeId));
    }

    public static RentalNotFoundException activeForAccount(UUID accountId) {
        return new RentalNotFoundException(String.format("Active rental for account with id '%s' doesn't exist.", accountId));
    }

}