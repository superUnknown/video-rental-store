package pl.budka.videorentalstore.rentals.bonuspoints.repository;

import org.springframework.data.repository.Repository;
import pl.budka.videorentalstore.rentals.bonuspoints.model.BonusPoints;
import pl.budka.videorentalstore.rentals.rentals.model.Rental;

import java.util.List;
import java.util.UUID;

public interface BonusPointsRepository extends Repository<BonusPoints, UUID> {

    BonusPoints save(BonusPoints bonusPoints);

    List<BonusPoints> findAll();

    void deleteAll();

    boolean existsByRental(Rental rental);

}