package pl.budka.videorentalstore.rentals.rentals.repository;

import org.springframework.data.repository.Repository;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatus;
import pl.budka.videorentalstore.rentals.rentals.model.Rental;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface RentalsRepository extends Repository<Rental, UUID> {

    Optional<Rental> findOne(UUID rentalId);

    List<Rental> findAllByAccountIdAndStatusIn(UUID accountId, Set<RentalStatus> statuses);

    Optional<Rental> findByChargeId(UUID chargeId);

    Rental save(Rental rental);

    List<Rental> findAll();

    void deleteAll();

}