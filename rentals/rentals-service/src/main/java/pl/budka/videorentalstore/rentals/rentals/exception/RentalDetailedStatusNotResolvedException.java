package pl.budka.videorentalstore.rentals.rentals.exception;

import pl.budka.videorentalstore.api.payments.charges.model.ChargeStatus;
import pl.budka.videorentalstore.api.rentals.commons.model.RentalsErrorCode;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatus;
import pl.budka.videorentalstore.rentals.commons.exception.RentalsApplicationException;

import java.util.UUID;

public class RentalDetailedStatusNotResolvedException extends RentalsApplicationException {

    private RentalDetailedStatusNotResolvedException(String message) {
        super(message, RentalsErrorCode.RENTAL_DETAILED_STATUS_NOT_RESOLVED);
    }

    public static RentalDetailedStatusNotResolvedException of(UUID rentalId, RentalStatus rentalStatus, UUID chargeId, ChargeStatus chargeStatus) {
        return new RentalDetailedStatusNotResolvedException(String.format("Rental detailed status couldn't be resolved [rentalId: %s, rentalStatus: %s, chargeId: %s, chargeStatus: %s].", rentalId, rentalStatus, chargeId, chargeStatus));
    }

}