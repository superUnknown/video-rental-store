package pl.budka.videorentalstore.rentals.rentals.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;
import pl.budka.videorentalstore.api.rentals.movies.model.MovieType;
import pl.budka.videorentalstore.api.rentals.rentals.model.RentalStatus;
import pl.budka.videorentalstore.core.utils.Time;
import pl.budka.videorentalstore.rentals.movies.model.Movie;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Rental {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    private UUID accountId;

    @NotNull
    private UUID chargeId;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RentalStatus status = RentalStatus.REQUESTED;

    @NotNull
    private Instant started = Time.now();

    @NotNull
    private Instant endingUntil;

    private Instant completed;

    @NotNull
    @DecimalMin("0.01")
    private BigDecimal premiumPriceApplied;

    @NotNull
    @DecimalMin("0.01")
    private BigDecimal basicPriceApplied;

    @Min(0)
    @NotNull
    private Integer premiumBonusPointsApplied;

    @Min(0)
    @NotNull
    private Integer basicBonusPointsApplied;

    @NotEmpty
    @OneToMany(mappedBy = "rental", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RentedMovie> movies;

    @Transient
    public boolean isRequested() {
        return status == RentalStatus.REQUESTED;
    }

    @Transient
    public boolean isCompleted() {
        return status == RentalStatus.COMPLETED;
    }

    @Transient
    public boolean isExpired() {
        return endingUntil.isBefore(Time.now());
    }

    @Builder
    public Rental(UUID accountId, Instant endingUntil, BigDecimal premiumPriceApplied, BigDecimal basicPriceApplied, Integer premiumBonusPointsApplied, Integer basicBonusPointsApplied, List<Movie> movies) {
        this.accountId = accountId;
        this.endingUntil = endingUntil;
        this.premiumPriceApplied = premiumPriceApplied;
        this.basicPriceApplied = basicPriceApplied;
        this.premiumBonusPointsApplied = premiumBonusPointsApplied;
        this.basicBonusPointsApplied = basicBonusPointsApplied;
        this.movies = movies.stream()
                .map(it -> RentedMovie.builder().rental(this).movie(it).build())
                .collect(Collectors.toList());
    }

    @Getter
    @Setter
    @Entity
    @NoArgsConstructor
    @Table(name = "rented_movie", uniqueConstraints = @UniqueConstraint(columnNames = {"movie_id", "rental_id"}))
    public static class RentedMovie {

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID id;

        @NotNull
        @ManyToOne
        @JoinColumn(name = "movie_id")
        private Movie movie;

        @NotNull
        @ManyToOne
        @JoinColumn(name = "rental_id")
        private Rental rental;

        @NotNull
        @Enumerated(EnumType.STRING)
        private MovieType movieType;

        @Builder
        public RentedMovie(Movie movie, Rental rental) {
            this.movie = movie;
            this.rental = rental;
            this.movieType = movie.getType();
        }

    }

}