CREATE TABLE movie (
    id UUID PRIMARY KEY,
    name TEXT NOT NULL,
    type TEXT NOT NULL,
    status TEXT NOT NULL
);

CREATE TABLE rental (
    id UUID PRIMARY KEY,
    account_id UUID NOT NULL,
    charge_id UUID NOT NULL,
    status TEXT NOT NULL,
    started TIMESTAMP NOT NULL,
    ending_until TIMESTAMP NOT NULL,
    completed TIMESTAMP,
    premium_price_applied NUMERIC(19,2) NOT NULL,
    basic_price_applied NUMERIC(19,2) NOT NULL,
    premium_bonus_points_applied INTEGER NOT NULL,
    basic_bonus_points_applied INTEGER NOT NULL
);

CREATE TABLE rented_movie (
    id UUID PRIMARY KEY,
    movie_id UUID NOT NULL REFERENCES movie(id),
    rental_id UUID NOT NULL REFERENCES rental(id),
    movie_type TEXT NOT NULL,
    unique (movie_id, rental_id)
);

CREATE TABLE bonus_points (
    id UUID PRIMARY KEY,
    "value" INTEGER NOT NULL,
    movie_id UUID NOT NULL REFERENCES movie(id),
    rental_id UUID NOT NULL REFERENCES rental(id),
    unique (movie_id, rental_id)
);